## Scripture Bible (3 pages) (webapp)

- [ ] virtual app

- [ ] can type in their name (backend choose login)

- [ ] 6 questions (1 page)

```Typescript
1. 今日有無祈禱
2. 今日有無唱聖詩
3. ...
```

- [ ] needs to know who has submitted

- [ ] chatroom (professor,users) [socketIO] (1 page)

- [ ] admin page (1 page)

```Typescript
1. check返啲人嘅提交答案(分年/月/日)
2. 可以交新users
```

- [ ] angular/react => ionic

- [ ] design a pdf form to know the frequency of praying

- [ ] sms authentication (if can)

- [ ] hash telephone to generate icon

- [ ] native script

modifications

- [ ] receive notifications without signing the app

- [ ] weekly progress

- [ ] change to cantonese (i18n)

- [x] qrCode keeps redundant

- [x] add password for users

- [x] add page for admin creating users

- [x] change password, add new table to store default password

- [x] html, print element

- [x] add announcement chatroom

- [ ] change scripture api(may not be application because of utf16 decoding problem)

- [x] compress image size

- [x] fs.unlinkSync -> delete function, needs to try catch

- [x] add file detail table, modify the multer and controller

- [x] admin can see the marked and unmarked tasks

- [ ] use ionic to re-write the app

- [x] final choose to use aws s3 bucket with cloudfront to store files

- [x] modify remarks to only one, not each question has one remark