import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {catchError} from "rxjs/operators";

@Injectable({
    providedIn:"root",
})
export class SoundService{
    constructor(private http:HttpClient){}
	token = localStorage.getItem("token");

	getSound(soundUrl:string):Observable<Blob>{
		// console.log(imageUrl);
		return this.http.get(soundUrl,{headers:new HttpHeaders({"Authorization":`Bearer ${this.token}`})
		,responseType:"blob"}).pipe(
			catchError(this.handleError<any>("load image"))
		)
	}
	private handleError<T>(operation = "operation",result?:T){
		return(error:any):Observable<T> => {
			console.error(`${operation}:${error}`);
			return of(result as T);
		}
	}
}
