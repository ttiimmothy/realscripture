import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ComponentFixture,TestBed} from '@angular/core/testing';
import {FormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {Router} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {of} from "rxjs";
import {BibleService} from "../bible.service";
import {BottomBarComponent} from "../bottom-bar/bottom-bar.component";
import {LoginComponent} from "../login/login.component";
import {QuestionsService} from "../questions.service";
import {UserService} from "../user.service";
import {HomeComponent} from './home.component';

describe("HomeComponent",() => {
	let component:HomeComponent;
	let fixture:ComponentFixture<HomeComponent>;
	let userService:UserService;
	let bibleService:BibleService;
	let questionService:QuestionsService;
	let router:Router;

	beforeEach(async() => {
		await TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				RouterTestingModule.withRoutes(
					[{path:"",component:LoginComponent}]
				),
				FormsModule,
				MatIconModule
			],
			declarations:[
				HomeComponent,
				BottomBarComponent
			]
		}).compileComponents();
	})

	beforeEach(() => {
		fixture = TestBed.createComponent(HomeComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
		userService = TestBed.inject(UserService);
		bibleService = TestBed.inject(BibleService);
		questionService = TestBed.inject(QuestionsService);
		router = TestBed.inject(Router);
		localStorage.setItem("token","abc");
	})

	it("should create",() => {
		expect(component).toBeTruthy();
	})

	it("cannot get current user when localStorage token is empty",() => {
		localStorage.setItem("token","");
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy"}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("");
	})

	it("should get current user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy"}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("timothy");
	})

	it("cannot get current user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("");
	})

	it("should get bible",() => {
		spyOn(bibleService,"loadScripture").and.returnValue(of({success:true,data:{id:1,bible_id:1,content:"timo"}}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.goldenSentence).toEqual({id:1,bible_id:1,content:"timo"});
	})

	it("cannot get bible",() => {
		spyOn(bibleService,"loadScripture").and.returnValue(of({success:false,data:{id:1,bible_id:1,content:"timo"}}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.goldenSentence).toEqual({id:0,bible_id:0,content:""});
	})

	it("should get question list",() => {
		spyOn(questionService,"loadQuestion").and.returnValue(of({success:true,data:[
			{
				id:1,
				content:"timo",
				remarks:"mothy",
				created_at:"2021-08-24",
				update_at:"2021-08-24"
			}
		]}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.questions).toEqual([
			{
				id:1,
				content:"timo",
				remarks:"mothy",
				created_at:"2021-08-24",
				update_at:"2021-08-24"
			}
		])
		expect(component.question).toEqual({
			id:1,
			content:"timo",
			remarks:"mothy",
			created_at:"2021-08-24",
			update_at:"2021-08-24"
		})
	})

	it("cannot get question list",() => {
		spyOn(questionService,"loadQuestion").and.returnValue(of({success:false}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.questions).toEqual([])
	})

	it("should submit answer with input time is a number",() => {
		spyOn(questionService,"submit").and.returnValue(of({success:true}));
		component.usedTime = [
			{
				time:"25",
				remark:""
			}
		]
		component.questions = [
			{
				id:1,
				content:"timo",
				remarks:"mothy",
				created_at:"2021-08-24",
				update_at:"2021-08-24"
			}
		]
		component.submitAnswer([{time:"20",remark:""}],"timo");
		expect(component.usedTime).toEqual([
			{
				time:"",
				remark:""
			}
		])
	})

	it("cannot submit answer with input time is not a number",() => {
		spyOn(questionService,"submit").and.returnValue(of({success:true}));
		component.usedTime = [
			{
				time:"25",
				remark:""
			}
		]
		component.questions = [
			{
				id:1,
				content:"timo",
				remarks:"mothy",
				created_at:"2021-08-24",
				update_at:"2021-08-24"
			}
		]
		component.submitAnswer([{time:"a",remark:""}],"timo");
		expect(component.usedTime).toEqual([
			{
				time:"25",
				remark:""
			}
		])
	})

	it("cannot submit answer with input time is empty",() => {
		spyOn(questionService,"submit").and.returnValue(of({success:true}));
		component.usedTime = [
			{
				time:"25",
				remark:""
			}
		]
		component.questions = [
			{
				id:1,
				content:"timo",
				remarks:"mothy",
				created_at:"2021-08-24",
				update_at:"2021-08-24"
			}
		]
		component.submitAnswer([{time:"",remark:""}],"timo");
		expect(component.usedTime).toEqual([
			{
				time:"25",
				remark:""
			}
		])
	})

	it("cannot submit answer with input time is empty",() => {
		spyOn(questionService,"submit").and.returnValue(of({success:false}));
		component.usedTime = [
			{
				time:"25",
				remark:""
			}
		]
		component.questions = [
			{
				id:1,
				content:"timo",
				remarks:"mothy",
				created_at:"2021-08-24",
				update_at:"2021-08-24"
			}
		]
		component.submitAnswer([{time:"20",remark:""}],"timo");
		expect(component.usedTime).toEqual([
			{
				time:"25",
				remark:""
			}
		])
	})

	it("should logout",() => {
		let routerNew = spyOn(router,"navigate");
		component.logout();
		expect(localStorage.getItem("token")).toEqual(null);
		expect(routerNew).toHaveBeenCalledWith(["/"]);
	})
})