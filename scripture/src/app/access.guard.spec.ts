import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from '@angular/core/testing';
import {RouterTestingModule} from "@angular/router/testing";
import {AccessGuard} from './access.guard';
import {RouterStateSnapshot,Router,ActivatedRouteSnapshot} from '@angular/router';
import {UserService} from "./user.service";
import {HttpClient, HttpHandler} from "@angular/common/http";
import {of} from "rxjs";

function fakeRouterState(url:string):RouterStateSnapshot{
	return {
		url
	} as RouterStateSnapshot;
}

describe("AccessGuard",() => {
	let guard:AccessGuard;
	let route:ActivatedRouteSnapshot;
	const url = "/";
	let router:jasmine.SpyObj<Router>;
	let userService: UserService;
	let httpTestingController:HttpTestingController;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				RouterTestingModule
			]
		})
		// guard = TestBed.inject(AccessGuard);
		router = jasmine.createSpyObj<Router>("Router",["navigate"]);
		httpTestingController = TestBed.inject(HttpTestingController);
        userService = TestBed.inject(UserService);
		// guard = TestBed.inject(AccessGuard);
		guard = new AccessGuard(userService,router);
		route = {
			data:{
				requiresLogin:false,
				requiresUserLogin:false,
				requiresAdminLogin:false,
			}
		} as any as ActivatedRouteSnapshot;
	})

	it("should be created",() => {
		expect(guard).toBeTruthy();
	})

	it("should go to guard routes with requiresLogin",() => {
		route.data.requiresLogin = true;
		userService.token = "123";
		const canActivate = guard.canActivate(route,fakeRouterState(url));
		expect(canActivate).toBeTrue();
	})

	it("should not go to guard routes with requiresLogin without token",() => {
		route.data.requiresLogin = true;
		userService.token = "";
		const canActivate = guard.canActivate(route,fakeRouterState(url));
		expect(canActivate).toBeFalse();
		expect(router.navigate).toHaveBeenCalledWith(["/"]);
	})

	it("should go to guard routes with requiresUserLogin",() => {
		route.data.requiresUserLogin = true;
		userService.token = "123";
		spyOn(userService,'checkCurrentUsers').and.returnValue(of({success:true,data:{is_admin:false}}));
		const canActivate = guard.canActivate(route,fakeRouterState(url));
		expect(canActivate).toBeTrue();
	})

	it("should not go to guard routes with requiresUserLogin if user is admin",() => {
		route.data.requiresUserLogin = true;
		userService.token = "123";
		spyOn(userService,'checkCurrentUsers').and.returnValue(of({success:true,data:{is_admin:true}}));
		const canActivate = guard.canActivate(route,fakeRouterState(url));
		expect(canActivate).toBeFalse();
	})

	it("should not go to guard routes with requiresUserLogin if token is empty",() => {
		route.data.requiresUserLogin = true;
		userService.token = "";
		spyOn(userService,'checkCurrentUsers').and.returnValue(of({success:true,data:{is_admin:false}}));
		const canActivate = guard.canActivate(route,fakeRouterState(url));
		expect(canActivate).toBeFalse();
	})

	it("should go to guard routes with requiresAdminLogin",() => {
		route.data.requiresAdminLogin = true;
		userService.token = "123";
		spyOn(userService,'checkCurrentUsers').and.returnValue(of({success:true,data:{is_admin:true}}));
		const canActivate = guard.canActivate(route,fakeRouterState(url));
		expect(canActivate).toBeTrue();
	})

	it("should not go to guard routes with requiresAdminLogin without token",() => {
		route.data.requiresAdminLogin = true;
		userService.token = "";
		spyOn(userService,'checkCurrentUsers').and.returnValue(of({success:true,data:{is_admin:true}}));
		const canActivate = guard.canActivate(route,fakeRouterState(url));
		expect(canActivate).toBeFalse();
	})

	it("should not go to guard routes without stating the related route.data",() => {
		route.data.requiresAdminLogin = false;
		spyOn(userService,'checkCurrentUsers').and.returnValue(of({success:true,data:{is_admin:true}}));
		const canActivate = guard.canActivate(route,fakeRouterState(url));
		expect(canActivate).toBeFalse();
	})
})