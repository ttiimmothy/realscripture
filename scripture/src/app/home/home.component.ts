import {Component,OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {AccessGuard} from "../access.guard";
import {BibleService} from "../bible.service";
import {QuestionsService} from "../questions.service";
import {UserService} from "../user.service";
import {Question, Sentence, Time} from "./question";
@Component({
	selector:"app-home",
	templateUrl:"./home.component.html",
	styleUrls:["./home.component.scss"]
})
export class HomeComponent implements OnInit{
	constructor(private userService:UserService,private questionService:QuestionsService,private router:Router,
	private bibleService:BibleService){}
	ngOnInit():void{
		if(localStorage.getItem("token")){
			this.userService.checkCurrentUsers().subscribe((result) => {
				// console.log(result.data.is_admin)
				if(result && result.success){
					this.user = result.user;
					// this.userService.user = result.user;
					// this.userService.token = localStorage.getItem("token");
				}
			})
		}
		this.bibleService.loadScripture().subscribe((result) => {
			// console.log(result)
			if(result.success){
				this.goldenSentence = result.data;
			}
		})
		this.questionService.loadQuestion().subscribe((result) => {
			// console.log(result);
			if(result.success){
				this.questions = result.data;
				this.question = result.data[0];
				for(let question of this.questions){
					this.usedTime[question.id - this.questions[0].id] = {
						time:"",
						remark:""
					}
				}
			}
		})
		this.questionService.token = localStorage.getItem("token");
	}

	user = "";
	date = new Date().toISOString().substring(0,10);
	questions:Question[] = [];
	question:Question = {
		id:0,
		content:"",
		remarks:"",
		created_at:"",
		update_at:""
	}
	token = localStorage.getItem("token");
	usedTime:Time[] = [];
	answer:string = "";
	goldenSentence:Sentence = {
		id:0,
		bible_id:0,
		content:""
	}

	submitAnswer(time:Time[],answer:string){
		// console.log(time);
		// console.log(answer);
		for(let i = 0; i < time.length; i++){
			if(!time[i]["time"]){
				return;
			}
			if((parseInt(time[i]["time"]) + "") !== time[i]["time"]){
				return;
			}
		}
		time[time.length - 1]["remark"] = answer;
		// console.log(time);
		this.questionService.submit(time).subscribe((result) => {
			// console.log(result);
			if(result.success){
				this.answer = "";
				for(let i = 0; i < this.questions.length; i++){
					this.usedTime[i] = {
						time:"",
						remark:""
					}
				}
			}
		})
	}

	logout(){
		localStorage.removeItem("token");
		this.router.navigate(["/"]);
	}
}