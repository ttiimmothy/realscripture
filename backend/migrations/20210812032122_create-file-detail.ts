import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("file_detail",(table) => {
		table.increments();
		table.text("name");
		table.integer("size");
		table.string("type").notNullable();
		table.timestamps(false,true)
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("file_detail");
}