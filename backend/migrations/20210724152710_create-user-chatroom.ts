import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("user_chatroom",(table) => {
		table.increments();
		table.integer("user_id").unsigned();
		table.foreign("user_id").references("users.id");
		table.integer("chatroom_id").unsigned();
		table.foreign("chatroom_id").references("chatroom.id");
		table.boolean("is_admin").defaultTo(false);
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("user_chatroom");
}