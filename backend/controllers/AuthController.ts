import {Request,Response} from "express";
import {logger} from "../logger";

export class AuthController{
	constructor(){}

	checkCurrentUser = async(req:Request,res:Response) => {
		try{
			res.status(200).json({success:true,data:req.user,user:req.user?.username})
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`)
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}
}