import {Request, Response} from "express";
import {Knex} from "knex";
import {UserService} from "../services/UserService";
import {UserController} from "./UserController";
import jwt from "jwt-simple";
import {hashPassword} from "../hash";
import {logger} from "../logger";

describe("UserController",() => {
	let userController:UserController;
	let userService:UserService;
	let req:Request;
	let res:Response;
	let hashedPassword:string;

	beforeEach(async() => {
		userService = new UserService({} as Knex);
		hashedPassword = await hashPassword("ttiimmothhy");
		jest.spyOn(userService,"login").mockImplementation((phoneNumber:number) => Promise.resolve([{id:1,username:"timothy",
		phone:11122233,icon:"",is_admin:false,qr_code:"",password:hashedPassword,created_at:new Date("2021-07-30"),
		updated_at:new Date("2021-07-30")}]));

		jest.spyOn(userService,"adminLogin").mockImplementation((phoneNumber:number) => Promise.resolve([{id:1,
		username:"timothy",phone:11122233,icon:"",is_admin:true,qr_code:"",password:hashedPassword,created_at:
		new Date("2021-07-30"),updated_at:new Date("2021-07-30")}]));

		jest.spyOn(userService,"getUserPassword").mockImplementation(async(userId:number) => Promise.resolve([{id:1,
		username:"timothy",phone:11122233,icon:"",is_admin:true,qr_code:"",password:hashedPassword,created_at:
		new Date("2021-07-30"),updated_at:new Date("2021-07-30")}]));

		jest.spyOn(userService,"signUp").mockImplementation((username:string,phone:number,icon:string = "unknown.png",
		size:number = 0) => Promise.resolve());

		req = {
			body:{},
			params:{},
			query:{},
			user:{
				id:1,
				username:"timothy"
			}
		} as any as Request;

		res = {
			json:jest.fn((obj:any) => null),
			status:jest.fn((statusCode:number) => res)
		} as any as Response;

		userController = new UserController(userService)

		logger.error = jest.fn()
	})

	it("should login",async() => {
		req.body = {phoneNumber:"11122233",password:"ttiimmothhy"};
		await userController.login(req,res);
		const token = jwt.encode({
			id:1,
			phone:11122233
		},process.env.JWT_SECRET!)
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:token,user:"timothy"});
	})

	it("cannot login",async() => {
		jest.spyOn(userService,"login").mockImplementation((phoneNumber:number) => Promise.resolve([]));
		req.body = {phoneNumber:"11122234",password:"daniel"};
		await userController.login(req,res);
		expect(res.json).toBeCalledWith({success:false,message:"user does not exist"});
	})

	it("cannot login as user because of wrong password",async() => {
		// jest.spyOn(userService,"adminLogin").mockImplementation((phoneNumber:number) => Promise.resolve([]));
		req.body = {phoneNumber:"11122233",password:"daniel"};
		await userController.login(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:false,message:"password is not correct"});
	})

	it("should login as admin",async() => {
		req.body = {phoneNumber:"11122233",password:"ttiimmothhy"};
		await userController.adminLogin(req,res);
		const token = jwt.encode({
			id:1,
			phone:11122233
		},process.env.JWT_SECRET!)
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:token,user:"timothy"});
	})

	it("cannot login as admin because of wrong phone",async() => {
		jest.spyOn(userService,"adminLogin").mockImplementation((phoneNumber:number) => Promise.resolve([]));
		req.body = {phoneNumber:"11122234",password:"ttiimmothhy"};
		await userController.adminLogin(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:false,message:"this user is not admin"});
	})

	it("cannot login as admin because of wrong password",async() => {
		// jest.spyOn(userService,"adminLogin").mockImplementation((phoneNumber:number) => Promise.resolve([]));
		req.body = {phoneNumber:"11122233",password:"daniel"};
		await userController.adminLogin(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:false,message:"password is not correct"});
	})

	it("should sign up without icon",async() => {
		req.body = {username:"",phoneNumber:"11122233",icon:""};
		await userController.signUp(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true});
	})

	it("should sign up with icon",async() => {
		req.body = {username:"",phoneNumber:"11122233",icon:""};
		req.file = {
			filename:"daniel.png"
		} as any
		await userController.signUp(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true});
	})

	it("should print error when login as user get error",async () => {
		req.path = "/user/login";
		req.method = "post";
		req.body = {phoneNumber:"11122233",password:"timo"};
		res = {} as any
		try{
			await userController.login(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			// expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:post path:/user/login")
		}
	})

	it("should print error when login as admin get error",async () => {
		req.path = "/user/admin/login";
		req.method = "post";
		res = {} as any;
		// jest.spyOn(userService,"adminLogin").mockImplementation((phoneNumber:number) => Promise.reject());
		try{
			await userController.adminLogin(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			// expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:post path:/user/admin/login")
		}
	})

	it("should print error when sign up get error",async () => {
		req.path = "/user/newUser";
		req.method = "post";
		res = {} as any;
		try{
			await userController.signUp(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:post path:/user/newUser")
		}
	})
})
