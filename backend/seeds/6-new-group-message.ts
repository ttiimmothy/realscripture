import {Knex} from "knex";

export async function seed(knex:Knex):Promise<void>{
	// Deletes ALL existing entries
	let roomId = (await knex.select("*").from("chatroom").where("room_name","daniel"))[0].id;
	await knex("group_message").where("chatroom_id",roomId).del();
	let userId = (await knex.select("id").from("users").where("username","timothy"))[0].id;
	let receiveUserId = (await knex.select("id").from("users").where("username","timo"))[0].id;
	// Inserts seed entries
	await knex("group_message").insert([
		{send_user_id:userId,chatroom_id:roomId,content:"good morning"},
		{send_user_id:receiveUserId,chatroom_id:roomId,content:"good afternoon"},
	])
}