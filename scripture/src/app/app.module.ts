import {HttpClientModule} from "@angular/common/http";
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {LoginComponent} from "./login/login.component";
import {HomeComponent} from "./home/home.component";
import {AccessGuard} from "./access.guard";
import {BottomBarComponent} from './bottom-bar/bottom-bar.component';
import {DateAdapter,MatNativeDateModule,MAT_DATE_FORMATS,MAT_DATE_LOCALE} from '@angular/material/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
import {NoticeComponent} from './notice/notice.component';
import {ChatroomComponent} from './chatroom/chatroom.component';
import {ChatroomDetailComponent} from './chatroom-detail/chatroom-detail.component';
import {SocketIoConfig, SocketIoModule} from "ngx-socket-io";
import {FormsModule} from "@angular/forms";
import {CreateChatroomComponent} from './create-chatroom/create-chatroom.component';
import {AdminComponent} from './admin/admin.component';
import {AdminHomeComponent, myFormats} from './admin-home/admin-home.component';
import {MatSliderModule} from '@angular/material/slider';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from '@angular/material/input';
import {MomentDateAdapter,MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {UnmarkedTasksComponent} from './unmarked-tasks/unmarked-tasks.component';
import {AddUserComponent} from './add-user/add-user.component';
import {BibleDetailComponent} from './bible-detail/bible-detail.component';
import {SignUpComponent} from './sign-up/sign-up.component';
import {environment} from "src/environments/environment";
import {PrintFormComponent} from './print-form/print-form.component';
import {ActivateGuard} from "./activate.guard";

export const config:SocketIoConfig = {url:environment.BACKEND_PATH,options:{}};
@NgModule({
	declarations:[
		AppComponent,
		LoginComponent,
  		HomeComponent,
    	BottomBarComponent,
		NoticeComponent,
		ChatroomComponent,
  		ChatroomDetailComponent,
    	CreateChatroomComponent,
     	AdminComponent,
      	AdminHomeComponent,
		UnmarkedTasksComponent,
		AddUserComponent,
		BibleDetailComponent,
		SignUpComponent,
  		PrintFormComponent
	],
	imports:[
		BrowserModule,
		AppRoutingModule, // 轉頁
		HttpClientModule, // fetch
		MatNativeDateModule,
		BrowserAnimationsModule,
		MatIconModule,
		FormsModule,
		SocketIoModule.forRoot(config),
		MatNativeDateModule,
		MatSliderModule,
		MatDatepickerModule,
		MatFormFieldModule,
		MatInputModule,
	],
	providers:[
		AccessGuard,
		ActivateGuard,
		{
			provide:DateAdapter,
			useClass:MomentDateAdapter,
			// useValue:{useUtc:true},
			deps:[MAT_DATE_LOCALE,MAT_MOMENT_DATE_ADAPTER_OPTIONS]
		},
		{
			provide:MAT_DATE_FORMATS,useValue:myFormats
		}
	],
	bootstrap:[AppComponent]
})
export class AppModule{}
