export interface Sentence{
	id:number;
	bible_id:number;
	content:string;
}
export interface Answer{
	id:number;
	username:string;
	content:string;
	remarks:string
	question:string;
	is_marked:boolean;
}