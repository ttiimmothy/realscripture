import {Request, Response} from "express";
import {Knex} from "knex";
import {logger} from "../logger";
import {AuthService} from "../services/AuthService";
import {AuthController} from "./AuthController";

describe("AuthController",() => {
	let authController:AuthController;
	let authService:AuthService;
	let req:Request;
	let res:Response;

	beforeEach(() => {
		authService = new AuthService({} as Knex);
		jest.spyOn(authService,"checkCurrentUser").mockImplementation((phone:number) => Promise.resolve(
		[{id:1,username:"timothy",phone:12345678,icon:"unknown.png","is_admin":false,"qr_code":""}]));

		authController = new AuthController();

		req = {
			body:{},
			params:{},
			query:{},
			user:{
				id:1,
				username:"timothy"
			}
		} as any as Request;

		res = {
			json:jest.fn((obj:any) => null),
			status:jest.fn((statusCode:number) => res)
		} as any as Response;

		logger.error = jest.fn();
	})

	it("should check current users",() => {
		authController.checkCurrentUser(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:{id:1,username:"timothy"},user:"timothy"});
	})

	it("should print error when getting unmarked tasks get error",async () => {
		req.path = "/auth/currentUserCheck";
		req.method = "get";
		res = {} as any
		try{
			await authController.checkCurrentUser(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:get path:/auth/currentUserCheck")
		}
	})

	it("should get undefined in checking current user when req.user is undefined",async () => {
		req.user = undefined;
		await authController.checkCurrentUser(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:undefined});
	})
})