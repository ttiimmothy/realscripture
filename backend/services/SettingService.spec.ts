import {SettingService} from "./SettingService";
import Knex from "knex";
import {hashPassword} from "../hash";
import {logger} from "../logger";
const knexfile = require("../knexfile");
const knex = Knex(knexfile["testing"]);

describe("SettingService",() => {
	let settingService:SettingService;
	let hashedPassword:string;
	let userId:any;

	beforeEach(async() => {
		hashedPassword = await hashPassword("ttiimmothhy");
		userId = (await knex.insert({username:"timothy",phone:11122233,icon:"timo",qr_code:"",password:hashedPassword})
		.into("users").returning("id"))[0];
		await knex.insert({username:"admin",phone:11122333,icon:"timo",qr_code:"",is_admin:true,password:hashedPassword})
		.into("users");
		await knex.insert({password:hashedPassword}).into("default_password");
		settingService = new SettingService(knex);
		logger.error = jest.fn();
	})

	it("should change password",async() => {
		let newHashedPassword = await hashPassword("patrick");
		await settingService.changePassword(userId,newHashedPassword);
		let userInfo = await knex.select("*").from("users").where("id",userId);
		expect(userInfo).toMatchObject([{
			username:"timothy",
			phone:11122233,
			icon:"timo",
			qr_code:"",
			password:newHashedPassword
		}])
	})

	it("should not change password when req.user.id is undefined",async() => {
		let newHashedPassword = await hashPassword("patrick");
		await settingService.changePassword(undefined,newHashedPassword);
		let userInfo = await knex.select("*").from("users").where("id",userId);
		expect(userInfo).toMatchObject([{
			username:"timothy",
			phone:11122233,
			icon:"timo",
			qr_code:"",
			password:hashedPassword
		}])
	})

	it("should change default password",async() => {
		let newHashedPassword = await hashPassword("patrick");
		await settingService.changeDefaultPassword(true,newHashedPassword);
		let defaultPassword = await knex.select("*").from("default_password");
		expect(defaultPassword).toMatchObject([{
			password:newHashedPassword
		}])
	})

	it("should not change default password when the user is not admin",async() => {
		let newHashedPassword = await hashPassword("patrick");
		await settingService.changeDefaultPassword(false,newHashedPassword);
		let defaultPassword = await knex.select("*").from("default_password");
		expect(defaultPassword).toMatchObject([{
			password:hashedPassword
		}])
	})

	it("should print error and rollback in transaction when changing default password get error",async() => {
		try{
			await settingService.changeDefaultPassword(true,undefined as any as string);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith("knex transaction rollback")
		}
	})

	afterEach(async() => {
		await knex("users").del();
		await knex("default_password").del();
	})

	afterAll(async() => {
		await knex.destroy();
	})
})