import {Component,OnInit} from '@angular/core';
import {UserService} from "../user.service";

@Component({
	selector: 'app-bottom-bar',
	templateUrl: './bottom-bar.component.html',
	styleUrls: ['./bottom-bar.component.scss']
})
export class BottomBarComponent implements OnInit {
	constructor(private userService:UserService){}
	ngOnInit():void{
		this.userService.checkCurrentUsers().subscribe((result) => {
			// console.log(result);
			if(result.success && result.data.is_admin){
				this.isAdmin = true;
			}
			// console.log(this.isAdmin);
		})
	}
	isAdmin:boolean = false;
}
