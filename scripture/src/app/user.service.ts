import {Injectable} from '@angular/core';
import {Observable, of} from "rxjs";
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {catchError} from "rxjs/operators";
import {environment} from "src/environments/environment";
import {Object} from "./models";

@Injectable({
	providedIn:"root"
})
export class UserService{
	constructor(private http:HttpClient){}
	token = localStorage.getItem("token");
	user = "";
	httpOptions = {
		headers:new HttpHeaders({"Content-type":"application/json"})
	}

	login(phoneNumber:string,password:string):Observable<any>{
		return this.http.post<Object>(`${environment.BACKEND_PATH}/user/login`,JSON.stringify({phoneNumber,password}),this.httpOptions)
		.pipe(
			// tap(() => this.log("login success")),
			catchError(this.handleError<any>("login"))
		)
	}

	adminLogin(phoneNumber:string,password:string):Observable<any>{
		return this.http.post<Object>(`${environment.BACKEND_PATH}/user/admin/login`,JSON.stringify({phoneNumber,password}),
		this.httpOptions)
		.pipe(
			catchError(this.handleError<any>("admin login"))
		)
	}

	checkCurrentUsers():Observable<any>{
		return this.http.get<Object>(`${environment.BACKEND_PATH}/auth/currentUserCheck`,
		{headers:new HttpHeaders({"Authorization":`Bearer ${this.token}`})}).pipe(
			// tap(() => {this.log("user already logins")}),
			catchError(this.handleError<any>("current users"))
		)
	}

	signUp(file:File|null,phone:string,username:string):Observable<any>{
		let formData = new FormData();
		if(file){
			formData.append("icon",file);
		}
		formData.append("username",username);
		formData.append("phoneNumber",phone);
		return this.http.post(`${environment.BACKEND_PATH}/user/newUser`,formData).pipe(
			catchError(this.handleError<any>("admin login"))
		)
	}

	changePassword(password:string):Observable<any>{
		return this.http.post(`${environment.BACKEND_PATH}/setting/password`,JSON.stringify({password}),
		{headers:new HttpHeaders({"Authorization":`Bearer ${this.token}`,"Content-type":"application/json"})}).pipe(
			catchError(this.handleError<any>("change user password"))
		)
	}

	changeDefaultPassword(password:string):Observable<any>{
		return this.http.post(`${environment.BACKEND_PATH}/setting/defaultPassword`,JSON.stringify({password}),
		{headers:new HttpHeaders({"Authorization":`Bearer ${this.token}`,"Content-type":"application/json"})}).pipe(
			catchError(this.handleError<any>("change user password"))
		)
	}

	private handleError<T>(operation = "operation",result?:T){
		return (error:any):Observable<T> => {
			console.error(`${operation}:${error}`);
			return of(result as T);
		}
	}
}
