import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from '@angular/core';
import {Observable, of} from "rxjs";
import {catchError} from "rxjs/operators";
import {environment} from "src/environments/environment";

@Injectable({
    providedIn:'root',
})
export class AdminHomeService{
    constructor(private http:HttpClient){}

	token = localStorage.getItem("token");

	loadTasks():Observable<any>{
		return this.http.get(`${environment.BACKEND_PATH}/admin/tasks`,
		{headers:new HttpHeaders({"authorization":`Bearer ${this.token}`})}).pipe(
			catchError(this.handleError("load unmarked task"))
		)
	}

	loadMarkedTasks():Observable<any>{
		return this.http.get(`${environment.BACKEND_PATH}/admin/markedTasks`,
		{headers:new HttpHeaders({"authorization":`Bearer ${this.token}`})}).pipe(
			catchError(this.handleError("load marked task"))
		)
	}

	searchTasks(start:string,end:string):Observable<any>{
		return this.http.post(`${environment.BACKEND_PATH}/admin/tasksWithRange`,JSON.stringify({start,end}),
		{headers:new HttpHeaders({"authorization":`Bearer ${this.token}`,"Content-type":"application/json"})}).pipe(
			catchError(this.handleError("load unmarked task with time range"))
		)
	}

	searchMarkedTasks(start:string,end:string):Observable<any>{
		return this.http.post(`${environment.BACKEND_PATH}/admin/markedTasksWithRange`,JSON.stringify({start,end}),
		{headers:new HttpHeaders({"authorization":`Bearer ${this.token}`,"Content-type":"application/json"})}).pipe(
			catchError(this.handleError("load marked task with time range"))
		)
	}

	loadTaskFinishDate(id:number):Observable<any>{
		return this.http.get(`${environment.BACKEND_PATH}/admin/taskFinishDate/${id}`,
		{headers:new HttpHeaders({"authorization":`Bearer ${this.token}`})}).pipe(
			catchError(this.handleError("load task finish date"))
		)
	}

	loadUserAnswer(id:number):Observable<any>{
		return this.http.get(`${environment.BACKEND_PATH}/admin/dailyAnswer/${id}`,
		{headers:new HttpHeaders({"authorization":`Bearer ${this.token}`})}).pipe(
			catchError(this.handleError("load user answer"))
		)
	}

	finishMarking(id:number):Observable<any>{
		return this.http.get(`${environment.BACKEND_PATH}/admin/markingDone/${id}`,
		{headers:new HttpHeaders({"authorization":`Bearer ${this.token}`})}).pipe(
			catchError(this.handleError("finish marking answer"))
		)
	}

	private handleError<T>(operation = "operation",result?:T){
		return(error:any):Observable<T> => {
			console.error(`${operation}:${error}`);
			return of(result as T);
		}
	}
}
