import {Component, OnInit} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {ActivatedRoute, Router} from "@angular/router";
import {environment} from "src/environments/environment";
import {BibleService} from "../bible.service";
import {ChatUser} from "../chatroom-detail/chatMessage";
import {ChatroomService} from "../chatroom.service";
import {ImageService} from "../image.service";
import {UserService} from "../user.service";
import {NewUser,Sentence} from "./sentence";

@Component({
    selector: 'app-add-user',
    templateUrl: './add-user.component.html',
    styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit{
	/* test-code */
    constructor(private userService:UserService,private bibleService:BibleService,private imageService:ImageService,
	private chatroomService:ChatroomService,private route:ActivatedRoute,private router:Router,
	private sanitizer:DomSanitizer){}
	/* end-test-code */
    ngOnInit():void{
		if(localStorage.getItem("token")){
			this.userService.checkCurrentUsers().subscribe((result) => {
				// console.log(result.data);
				if(result.success){
					this.user = result.user;
				}
			})
		}
		this.bibleService.loadScripture().subscribe((result) => {
			if(result.success){
				this.goldenSentence = result.data;
			}
		})
		this.chatroomService.getChatroomUser(this.id).subscribe((result) => {
			if(result.success){
				this.alreadyInGroupUsers = result.data.map((user:ChatUser) => {return({
					id:user.id,
					username:user.username,
					icon:user.icon
				})})
				// console.log(this.alreadyInGroupUsers);
				for(let user of this.alreadyInGroupUsers){
					this.getImageFromService(`${this.imagePath}/${user.icon}`,user.id);
				}
			}
		})
	}
	user = "";
	goldenSentence:Sentence = {
		id:0,
		bible_id:0,
		content:""
	}
	usernames:NewUser[] = [];
	alreadyInGroupUsers:NewUser[] = [];
	choseUsernames:NewUser[] = [];
	searchKeyword = "";
	id = Number(this.route.snapshot.paramMap.get("id"));
	searchNewUser(username:string,existUser:NewUser[],alreadyInGroupUser:NewUser[]){
		if(username){
			this.chatroomService.searchNewPerson(username,existUser.map(user => user.username),
			alreadyInGroupUser.map(user => user.username)).subscribe((result) => {
				// console.log(result)
				if(result.success){
					this.usernames = result.data;
					for(let user of this.usernames){
						this.getImageFromService(`${this.imagePath}/${user.icon}`,user.id);
					}
				}
			})
		}else{
			this.usernames = [];
		}
	}

	addUser(id:number,username:string,icon:string){
		this.choseUsernames.push({id,username,icon});
		for(let user of this.choseUsernames){
			this.getImageFromService(`${this.imagePath}/${user.icon}`,user.id);
		}
		this.usernames = [];
		this.searchKeyword = "";
	}
	deleteUser(username:string){
		this.choseUsernames = this.choseUsernames.filter((user) => user.username !== username);
	}

	// imagePath = environment.BACKEND_PATH;
	imagePath = environment.IMAGE_PATH;
	imageToShow:(string|ArrayBuffer|null|any)[] = [];
	createImageFromBlob(image:Blob,index:number){
		let reader = new FileReader();
		reader.addEventListener("load",() => {
			this.imageToShow[index] = this.sanitizer.bypassSecurityTrustResourceUrl(reader.result as string);
		},false);
		if(image){
			reader.readAsDataURL(image);
		}
	}
	getImageFromService(imageUrl:string,index:number){
		// this.imageService.getImage(imageUrl).subscribe((data) => {
		this.imageService.getImageWithoutHeaders(imageUrl).subscribe((data) => {
			// console.log(data);
			this.createImageFromBlob(data,index);
		},(error) => {

		})
	}

	addUserToChatroom(users:NewUser[],id:number){
		this.chatroomService.addUserToChatroom(users.map((user) => user.username),id).subscribe((result) => {
			if(result.success){
				setTimeout(() => {
					this.router.navigate([`/chat/${this.id}`])
				},1000)
			}
		})
	}
}
