import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("answer",(table) => {
		table.increments();
		table.text("content");
		table.integer("user_id");
		table.text("remarks");
		table.integer("task_id").unsigned().notNullable();
		table.foreign("task_id").references("admin_task.id");
		table.timestamps(false,true)
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("answer");
}