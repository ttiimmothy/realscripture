import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("golden_sentence",(table) => {
		table.increments();
		table.integer("bible_id");
		table.text("content");
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("golden_sentence");
}