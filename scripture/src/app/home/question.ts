export interface Question{
	id:number;
	content:string;
	remarks:string;
	created_at:string;
	update_at:string;
}
export interface Sentence{
	id:number;
	bible_id:number;
	content:string;
}
export interface Time{
	time:string;
	remark:string;
}