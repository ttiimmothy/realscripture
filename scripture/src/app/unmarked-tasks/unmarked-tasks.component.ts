import {Component,OnInit} from '@angular/core';
import {ActivatedRoute,Router} from "@angular/router";
import {AdminHomeService} from "../admin-home.service";
import {BibleService} from "../bible.service";
// import {QuestionsService} from "../questions.service";
import {UserService} from "../user.service";
import {Answer,Sentence} from "./sentence";

@Component({
    selector: 'app-unmarked-tasks',
    templateUrl: './unmarked-tasks.component.html',
    styleUrls: ['./unmarked-tasks.component.scss'],
})
export class UnmarkedTasksComponent implements OnInit{
    constructor(private userService:UserService,private bibleService:BibleService,private route:ActivatedRoute,
	private adminHomeService:AdminHomeService,private router:Router){}
    ngOnInit():void{
		if(localStorage.getItem("token")){
			this.userService.checkCurrentUsers().subscribe((result) => {
				// console.log(result.data);
				if(result.success){
					this.user = result.user;
				}
			})
		}
		this.bibleService.loadScripture().subscribe((result) => {
			if(result.success){
				this.goldenSentence = result.data;
			}
		})
		this.adminHomeService.loadTaskFinishDate(this.id).subscribe((result) => {
			if(result.success){
				this.taskInfo = result.data;
			}
		})
		this.adminHomeService.loadUserAnswer(this.id).subscribe((result) => {
			if(result.success){
				this.answers = result.data;
				if(!result.data[0].is_marked){
					this.isMarked = false;
					for(let answer of this.answers){
						this.showAnswer[answer.id] = false;
					}
				}
			}
		})
	}

	user = "";
	goldenSentence:Sentence = {
		id:0,
		bible_id:0,
		content:""
	}
	id = Number(this.route.snapshot.paramMap.get("id"));
	taskInfo = {
		user_id:0,
		username:"",
		finish_date:""
	}
	answers:Answer[] = [];
	showAnswer:boolean[] = [];
	isMarked = true;
	makeVisible(id:number){
		this.showAnswer[id] = true;
	}
	makeHidden(id:number){
		this.showAnswer[id] = false;
	}
	finishMarking(){
		this.adminHomeService.finishMarking(this.id).subscribe((result) => {
			if(result.success){
				setTimeout(() => {
					this.router.navigate(["/home/admin"]);
				},1000)
			}
		})
	}
}