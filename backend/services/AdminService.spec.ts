import Knex from "knex";
import {logger} from "../logger";
import {AdminService} from "./AdminService";
const knexfile = require("../knexfile");
const knex = Knex(knexfile["testing"]);

describe("AdminService",() => {
	let adminService:AdminService;
	let tasks = [];
	let userId;

	beforeEach(async() => {
		userId = (await knex.insert({username:"timothy",phone:11222333,icon:"",qr_code:""}).into("users")
		.returning("id"))[0];
		// console.log(userId)
		tasks = [
			{
				user_id:userId,
				finish_date:new Date("2021-07-29"),
				// is_marked:false
			},
			{
				user_id:userId,
				finish_date:new Date("2021-07-30"),
				// is_marked:false
			},
			{
				user_id:userId,
				finish_date:new Date("2021-07-29"),
				is_marked:true
			},
			{
				user_id:userId,
				finish_date:new Date("2021-07-30"),
				is_marked:true
			}
		]
		await knex.insert(tasks).into("admin_task");
		adminService = new AdminService(knex);
		logger.error = jest.fn();
	})

	it("should load all unmarked tasks",async() => {
		let tasks = await adminService.loadUnmarkedTasks();
		expect(tasks).toMatchObject([
			{
				username:"timothy",
				finish_date:new Date("2021-07-29")
			},
			{
				username:"timothy",
				finish_date:new Date("2021-07-30")
			}
		])
	})

	it("should load all marked tasks",async() => {
		let tasks = await adminService.loadMarkedTasks();
		expect(tasks).toMatchObject([
			{
				username:"timothy",
				finish_date:new Date("2021-07-29")
			},
			{
				username:"timothy",
				finish_date:new Date("2021-07-30")
			}
		])
	})

	it("should load unmarked tasks within time period",async() => {
		let tasks = await adminService.loadUnmarkedTasksWithRange("2021-07-28","2021-07-29");
		expect(tasks).toMatchObject([
			{
				username:"timothy",
				finish_date:new Date("2021-07-29")
			}
		])
	})

	it("should load marked tasks within time period",async() => {
		let tasks = await adminService.loadMarkedTasksWithRange("2021-07-28","2021-07-29");
		expect(tasks).toMatchObject([
			{
				username:"timothy",
				finish_date:new Date("2021-07-29")
			}
		])
	})

	it("should get task finish date",async() => {
		let userId = (await knex.select("id").from("users").where("username","timothy"))[0].id
		let taskId = (await knex.select("*").from("admin_task").where("user_id",userId).orderBy("finish_date"))[0].id;
		let time = await adminService.loadTaskFinishDate(taskId);
		expect(time).toMatchObject([{
			finish_date:new Date("2021-07-29")
		}])
	})

	it("should get user answer",async() => {
		let userId = (await knex.select("id").from("users").where("username","timothy"))[0].id;
		let taskId = (await knex.select("*").from("admin_task").where("user_id",userId).orderBy("finish_date"))[0].id;
		await knex.insert({content:"25",user_id:userId,remarks:"daniel",task_id:taskId}).into("answer");
		await knex.insert({content:"hello",remarks:""}).into("question");
		let answer = await adminService.loadUserAnswer(taskId);
		expect(answer).toMatchObject([{
			username:"timothy",
			content:"25",
			remarks:"daniel",
			question:"hello"
		}])
	})

	it("should finish marking",async() => {
		let userId = (await knex.select("id").from("users").where("username","timothy"))[0].id;
		let taskId = (await knex.select("*").from("admin_task").where("user_id",userId).orderBy("finish_date"))[0].id;
		await adminService.finishMarkingAnswer(taskId);
		let task = await knex.select("*").from("admin_task").where("id",taskId);
		expect(task).toMatchObject([{
			user_id:userId,
			finish_date:new Date("2021-07-29"),
			is_marked:true
		}])
	})

	it("should print error and rollback in transaction when getting user answer get error",async() => {
		try{
			await adminService.loadUserAnswer("" as any as number);
			// console.log(task)
		}catch(e){
			expect(logger.error).toBeCalledTimes(1);
			expect(logger.error).toBeCalledWith("knex transaction rollback")
		}
	})

	afterEach(async() => {
		await knex("answer").del();
		await knex("admin_task").del();
		await knex("users").del();
		await knex("question").del();
	})

	afterAll(async() => {
		await knex.destroy();
	})
})