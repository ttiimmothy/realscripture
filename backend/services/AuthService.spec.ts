import Knex from "knex";
const knexfile = require("../knexfile");
const knex = Knex(knexfile["testing"]);
import {AuthService} from "./AuthService";

describe("AuthService",() => {
	let authService:AuthService;

	beforeEach(async() => {
		let user = {
			username:"timothy",
			phone:11122233,
			icon:"",
			qr_code:""
		}

		await knex.insert(user).into("users");
		authService = new AuthService(knex);
	})

	it("should find current user",async() => {
		let user = await authService.checkCurrentUser(11122233);
		expect(user).toMatchObject([{
			username:"timothy",
			phone:11122233,
			icon:"",
			is_admin:false,
			qr_code:""
		}])
	})

	afterEach(async() => {
		await knex("users").del();
	})

	afterAll(async() => {
		await knex.destroy();
	})
})