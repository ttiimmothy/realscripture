import {Knex} from "knex";

export async function seed(knex:Knex):Promise<void>{
	await knex("chatroom").where("room_name","daniel").orWhere("room_name","test").del();
	// Inserts seed entries
	await knex("chatroom").insert([
		{room_name:"announcement",room_icon:"trial2.png"},
		{room_name:"daniel",room_icon:"trial3.png"},
		{room_name:"test",room_icon:"trial4.png"},
	])
}