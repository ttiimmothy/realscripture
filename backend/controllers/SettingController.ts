import {Request,Response} from "express";
import {hashPassword} from "../hash";
import {logger} from "../logger";
import {SettingService} from "../services/SettingService";

export class SettingController{
	constructor(private settingService:SettingService){}

	changePassword = async(req:Request,res:Response) => {
		try{
			await this.settingService.changePassword(req.user?.id,await hashPassword(req.body.password));
			res.status(200).json({success:true});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	changeDefaultPassword = async(req:Request,res:Response) => {
		try{
			await this.settingService.changeDefaultPassword(req.user?.is_admin,await hashPassword(req.body.password));
			res.status(200).json({success:true});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}
}