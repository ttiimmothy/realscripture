import {Knex} from "knex";
import {logger} from "../logger";

export class SettingService{
	constructor(private knex:Knex){}

	async changePassword(id:number|undefined,password:string){
		if(id){
			await this.knex.update({password}).from("users").where("id",id);
		}
	}

	async changeDefaultPassword(isAdmin:boolean|undefined,password:string){
		let trx = await this.knex.transaction();
		try{
			if(isAdmin){
				let defaultPasswordId = (await trx.select("*").from("default_password"))[0].id;
				await trx.update({password}).from("default_password").where("id",defaultPasswordId);
			}
			await trx.commit();
		}catch(e){
			await trx.rollback();
			logger.error("knex transaction rollback");
			logger.error(e);
			throw e;
		}
	}
}