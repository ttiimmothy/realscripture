export interface Sentence{
	id:number;
	bible_id:number;
	content:string;
}