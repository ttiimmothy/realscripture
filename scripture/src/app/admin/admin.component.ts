import {Component,OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AccessGuard} from "../access.guard";
import {UserService} from "../user.service";

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit{
    constructor(private userService:UserService,private router:Router,private accessGuard:AccessGuard){}
    ngOnInit():void{
		if(localStorage.getItem("token")){
			this.userService.checkCurrentUsers().subscribe((result) => {
				// console.log(result)
				if(result && result.success){
					this.router.navigate(["/chat/1"]);
					// this.userService.token = localStorage.getItem("token");
					if(result.data.is_admin){
						this.accessGuard.user = "admin";
					}
				}else{
					this.router.navigate(["/admin/login"]);
				}
			})
		}
	}

	adminLogin(phoneNumber:string,password:string){
		this.userService.adminLogin(phoneNumber,password).subscribe((result) => {
			// console.log(result)
			if(result.success){
				this.userService.token = result.data;
				localStorage.setItem("token",result.data);
				this.router.navigate(["/chat/1"]);
				this.accessGuard.user = "admin";
				// console.log(this.userService.token)
			}
		})
	}
}