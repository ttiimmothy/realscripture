import {BibleService} from "./BibleService";
import Knex from "knex";
const knexfile = require("../knexfile");
const knex = Knex(knexfile["testing"])

describe("BibleService",() => {
	let bibleService:BibleService;

	beforeEach(async() => {
		await knex.insert({bible_id:1,content:"timothy"}).into("golden_sentence");
		bibleService = new BibleService(knex);
	})

	it("should get a bible script",async() => {
		let scripture = await bibleService.getBibleScripts(1);
		expect(scripture).toMatchObject([{
			bible_id:1,
			content:"timothy"
		}])
	})

	it("should insert new scripture with getting back the inserted data information",async() => {
		await bibleService.newScripture(2,"timo");
		let original = await knex.select("*").from("golden_sentence").orderBy("bible_id");
		expect(original).toMatchObject([
			{
				bible_id:1,
				content:"timothy"
			},
			{
				bible_id:2,
				content:"timo"
			}
		])
	})

	it("should get scripture id",async() => {
		let id = await bibleService.loadScriptureDetailId();
		expect(id).toMatchObject([{
			bible_id:1
		}])
	})

	it("should insert new scripture",async() => {
		await bibleService.insertNewScripture(2,"timo");
		let original = await knex.select("*").from("golden_sentence").orderBy("bible_id");
		expect(original).toMatchObject([
			{
				bible_id:1,
				content:"timothy"
			},
			{
				bible_id:2,
				content:"timo"
			}
		])
	})

	it("should get all bibles",async() => {
		let all = await bibleService.loadScriptureDetail();
		expect(all).toMatchObject([{
			bible_id:1,
			content:"timothy"
		}])
	})

	afterEach(async() => {
		await knex("golden_sentence").del()
	})

	afterAll(async() => {
		await knex.destroy();
	})
})