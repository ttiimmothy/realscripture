import {Component, OnInit} from '@angular/core';
import {BibleService} from "../bible.service";
import {UserService} from "../user.service";
import {Sentence, Tasks} from "./sentence";
import {Router} from "@angular/router";
import {AdminHomeService} from "../admin-home.service";

export const myFormats = {
	parse:{
		dateInput:"LL",
	},
	display:{
		dateInput:"YYYY-MM-DD",
		monthYearLabel:"MMM YYYY",
		dateA11yLabel:"LL",
		monthYearA11yLabel:"MMMM YYYY",
	},
	// useUtc:true
}

@Component({
    selector: 'app-admin-home',
    templateUrl: './admin-home.component.html',
    styleUrls: ['./admin-home.component.scss'],
})
export class AdminHomeComponent implements OnInit{
    constructor(private userService:UserService,private bibleService:BibleService,private router:Router,
	private adminHomeService:AdminHomeService){}
    ngOnInit():void{
		if(localStorage.getItem("token")){
			this.userService.checkCurrentUsers().subscribe((result) => {
				if(result && result.success){
					this.user = result.user;
					this.userService.token = localStorage.getItem("token");
				}
			})
		}
		this.bibleService.loadScripture().subscribe((result) => {
			if(result.success){
				this.goldenSentence = result.data;
			}
		})
		this.adminHomeService.loadTasks().subscribe((result) => {
			// console.log(result);
			if(result.success){
				this.unmarkedTasks = result.data;
			}
		})
		this.adminHomeService.loadMarkedTasks().subscribe((result) => {
			if(result.success){
				this.markedTasks = result.data;
			}
		})
	}
	user = "";
	goldenSentence:Sentence = {
		id:0,
		bible_id:0,
		content:""
	}
	unmarkedTasks:Tasks[] = [];
	markedTasks:Tasks[] = [];
	fromCalendarDate = "";
	toCalendarDate = "";
	markedTaskFromCalendarDate = "";
	markedTaskToCalendarDate = "";
	unmarked = true;

	searchTasks(startDate:any,endDate:any){
		if(new Date(endDate).getTime() > new Date(startDate).getTime()){
			this.adminHomeService.searchTasks(`${startDate["_i"].year}-${startDate["_i"].month + 1}-${startDate["_i"].date}`,
			`${endDate["_i"].year}-${endDate["_i"].month + 1}-${endDate["_i"].date}`).subscribe((result) => {
				// console.log(result)
				if(result.success){
					this.unmarkedTasks = result.data;
				}
			})
		}else if(new Date(endDate).getTime() < new Date(startDate).getTime()){
			this.unmarkedTasks = [];
		}
	}
	resetSearch(){
		this.adminHomeService.loadTasks().subscribe((result) => {
			// console.log(result);
			if(result.success){
				this.unmarkedTasks = result.data;
				this.fromCalendarDate = "";
				this.toCalendarDate = "";
			}
		})
	}
	searchMarkedTasks(startDate:any,endDate:any){
		// console.log(startDate);
		if(new Date(endDate).getTime() > new Date(startDate).getTime()){
			this.adminHomeService.searchMarkedTasks(`${startDate["_i"].year}-${startDate["_i"].month + 1}-${startDate["_i"].date}`,
			`${endDate["_i"].year}-${endDate["_i"].month + 1}-${endDate["_i"].date}`).subscribe((result) => {
				// console.log(result)
				if(result.success){
					this.markedTasks = result.data;
				}
			})
		}else if(new Date(endDate).getTime() < new Date(startDate).getTime()){
			this.markedTasks = [];
		}
	}
	resetMarkedTasksSearch(){
		this.adminHomeService.loadMarkedTasks().subscribe((result) => {
			// console.log(result);
			if(result.success){
				this.markedTasks = result.data;
				this.markedTaskFromCalendarDate = "";
				this.markedTaskToCalendarDate = "";
			}
		})
	}
	toggleUnmarked(){
		this.unmarked = !this.unmarked;
	}
	logout(){
		localStorage.removeItem("token");
		this.router.navigate(["/admin/login"]);
	}
}