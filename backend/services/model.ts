export interface User{
	id:number;
	username:string;
	phone:number;
	admin_password:string;
	icon:string;
	is_admin:boolean;
	qr_code:string;
	created_at:Date;
	updated_at:Date;
}
declare global{
	namespace Express{
		interface Request{
			user?:User
		}
	}
}
export interface Time{
	time:string;
	remark:string;
}