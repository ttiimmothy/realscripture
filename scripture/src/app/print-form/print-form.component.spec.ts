import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ComponentFixture, TestBed} from "@angular/core/testing";
import {MatIconModule} from "@angular/material/icon";
import {RouterTestingModule} from "@angular/router/testing";
import {of} from "rxjs";
import {AdminHomeService} from "../admin-home.service";
import {BottomBarComponent} from "../bottom-bar/bottom-bar.component";
import {PrintFormComponent} from "./print-form.component";

describe("PrintFormComponent",() => {
    let component:PrintFormComponent;
    let fixture:ComponentFixture<PrintFormComponent>;
	let adminHomeService:AdminHomeService;

    beforeEach(async() => {
        await TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				RouterTestingModule,
				MatIconModule
			],
            declarations: [
				PrintFormComponent,
				BottomBarComponent
			],
        }).compileComponents();
    })

    beforeEach(() => {
        fixture = TestBed.createComponent(PrintFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
		adminHomeService = TestBed.inject(AdminHomeService);
    })

    it("should create",() => {
        expect(component).toBeTruthy();
    })

	it("should get task finish date",() => {
		spyOn(adminHomeService,"loadTaskFinishDate").and.returnValue(of({success:true,data:{
			user_id:1,
			username:"timothy",
			finish_date:"2021-08-24"
		}}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.taskInfo).toEqual({
			user_id:1,
			username:"timothy",
			finish_date:"2021-08-24"
		})
	})

	it("cannot get task finish date",() => {
		spyOn(adminHomeService,"loadTaskFinishDate").and.returnValue(of({success:false}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.taskInfo).toEqual({
			user_id:0,
			username:"",
			finish_date:""
		})
	})
})
