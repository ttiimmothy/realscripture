import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ComponentFixture,discardPeriodicTasks,fakeAsync,TestBed,tick} from '@angular/core/testing';
import {FormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {RouterTestingModule} from "@angular/router/testing";
import {SocketIoModule} from "ngx-socket-io";
import {of,throwError} from "rxjs";
// import {Stream} from "stream";
import {AccessGuard} from "../access.guard";
import {config} from "../app.module";
import {BibleService} from "../bible.service";
import {BottomBarComponent} from "../bottom-bar/bottom-bar.component";
import {ChatroomService} from "../chatroom.service";
import {ImageService} from "../image.service";
import {UserService} from "../user.service";
import {ChatroomDetailComponent} from './chatroom-detail.component';

describe("ChatroomDetailComponent",() => {
	let component:ChatroomDetailComponent;
	let fixture:ComponentFixture<ChatroomDetailComponent>;
	let userService:UserService;
	let bibleService:BibleService;
	let accessGuard:AccessGuard;
	let chatroomService:ChatroomService;
	let imageService:ImageService;

	beforeEach(async() => {
		await TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				RouterTestingModule,
				SocketIoModule.forRoot(config),
				MatIconModule,
				FormsModule
			],
			declarations:[
				ChatroomDetailComponent,
				BottomBarComponent
			]
		}).compileComponents();
	})

	beforeEach(() => {
		fixture = TestBed.createComponent(ChatroomDetailComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
		userService = TestBed.inject(UserService);
		bibleService = TestBed.inject(BibleService);
		accessGuard = TestBed.inject(AccessGuard);
		chatroomService = TestBed.inject(ChatroomService);
		imageService = TestBed.inject(ImageService);
		localStorage.setItem("token","abc");
	})

	it("should create",() => {
		expect(component).toBeTruthy();
	})

	it("cannot get current user when localStorage token is empty",() => {
		localStorage.setItem("token","");
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy",data:{
			id:1,
			username:"timothy",
			phone:12345678,
			icon:"ronald.png",
			qr_code:"timo",
			is_admin:true
		}}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual({
			id:0,
			username:"",
			phone:0,
			icon:"",
			qr_code:"",
			is_admin:null
		})
	})

	it("should get current admin user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy",data:{
			id:1,
			username:"timothy",
			phone:12345678,
			icon:"ronald.png",
			qr_code:"timo",
			is_admin:true
		}}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual({
			id:1,
			username:"timothy",
			phone:12345678,
			icon:"ronald.png",
			qr_code:"timo",
			is_admin:true
		})
		expect(accessGuard.user).toEqual("admin");
	})

	it("should get current user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy",data:{
			id:1,
			username:"timothy",
			phone:12345678,
			icon:"ronald.png",
			qr_code:"timo",
			is_admin:false
		}}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual({
			id:1,
			username:"timothy",
			phone:12345678,
			icon:"ronald.png",
			qr_code:"timo",
			is_admin:false
		})
	})

	it("cannot get current user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:false}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual({
			id:0,
			username:"",
			phone:0,
			icon:"",
			qr_code:"",
			is_admin:null
		})
	})

	it("should get bible",() => {
		spyOn(bibleService,"loadScripture").and.returnValue(of({success:true,data:{id:1,bible_id:1,content:"timo"}}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.goldenSentence).toEqual({id:1,bible_id:1,content:"timo"});
	})

	it("cannot get bible",() => {
		spyOn(bibleService,"loadScripture").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.goldenSentence).toEqual({id:0,bible_id:0,content:""});
	})

	it("should get chatroom information",() => {
		spyOn(chatroomService,"findChatroomInformation").and.returnValue(of({success:true,data:{
			id:1,
			room_name:"timo",
			room_icon:"ronald.png",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.room).toEqual({
			id:1,
			room_name:"timo",
			room_icon:"ronald.png",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		})
	})

	it("cannot get chatroom information",() => {
		spyOn(chatroomService,"findChatroomInformation").and.returnValue(of({success:false}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.room).toEqual({
			id:0,
			room_name:"",
			room_icon:"",
			created_at:null,
			updated_at:null
		})
	})

	it("should get chat messages",() => {
		spyOn(chatroomService,"loadChatMessage").and.returnValue(of({success:true,data:[{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:"abc",
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}]}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:"abc",
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("cannot get chat messages",() => {
		spyOn(chatroomService,"loadChatMessage").and.returnValue(of({success:false}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([])
	})

	it("should get chat messages from the same user",() => {
		spyOn(chatroomService,"loadChatMessage").and.returnValue(of({success:true,data:[{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:"abc",
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}]}))
		component.user.id = 2;
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:"abc",
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get image messages",() => {
		spyOn(chatroomService,"loadChatMessage").and.returnValue(of({success:true,data:[{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:"abc",
			type:"image",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}]}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:"abc",
			type:"image",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get image messages",() => {
		spyOn(chatroomService,"loadChatMessage").and.returnValue(of({success:true,data:[{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:"abc",
			type:"sound",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}]}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:"abc",
			type:"sound",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get longer than 30 words of chat messages",() => {
		let string = "a";
		spyOn(chatroomService,"loadChatMessage").and.returnValue(of({success:true,data:[{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(31),
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}]}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(30) + " " + string,
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get longer than 37 words of chat messages from the same user",() => {
		let string = "a";
		spyOn(chatroomService,"loadChatMessage").and.returnValue(of({success:true,data:[{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(38),
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}]}))
		component.user.id = 2;
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(37) + " " + string,
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get longer than 60 words of chat messages",() => {
		let string = "a";
		spyOn(chatroomService,"loadChatMessage").and.returnValue(of({success:true,data:[{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(61),
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}]}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(30) + " " + string.repeat(30) + " " + string,
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get longer than 74 words of chat messages from the same user",() => {
		let string = "a";
		spyOn(chatroomService,"loadChatMessage").and.returnValue(of({success:true,data:[{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(75),
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}]}))
		component.user.id = 2;
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(37) + " " + string.repeat(37) + " " + string,
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get longer than 90 words of chat messages",() => {
		let string = "a";
		spyOn(chatroomService,"loadChatMessage").and.returnValue(of({success:true,data:[{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(91),
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}]}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(30) + " " + string.repeat(30) + " " + string.repeat(30) + " " + string,
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get longer than 111 words of chat messages from the same user",() => {
		let string = "a";
		spyOn(chatroomService,"loadChatMessage").and.returnValue(of({success:true,data:[{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(112),
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}]}))
		component.user.id = 2;
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(37) + " " + string.repeat(37) + " " + string.repeat(37) + " " + string,
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get longer than 120 words of chat messages",() => {
		let string = "a";
		spyOn(chatroomService,"loadChatMessage").and.returnValue(of({success:true,data:[{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(121),
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}]}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(30) + " " + string.repeat(30) + " " + string.repeat(30) + " " + string.repeat(30) + " "
			+ string,
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get chat messages in immediate response",() => {
		spyOn(chatroomService,"message").and.returnValue(of({
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:"abc",
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:"abc",
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get chat messages from the same user in immediate response",() => {
		spyOn(chatroomService,"message").and.returnValue(of({
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:"abc",
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}))
		component.user.id = 2;
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:"abc",
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get image messages in immediate response",() => {
		spyOn(chatroomService,"message").and.returnValue(of({
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:"abc",
			type:"image",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:"abc",
			type:"image",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get image messages in immediate response",() => {
		spyOn(chatroomService,"message").and.returnValue(of({
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:"abc",
			type:"sound",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:"abc",
			type:"sound",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get longer than 30 words of chat messages in immediate response",() => {
		let string = "a";
		spyOn(chatroomService,"message").and.returnValue(of({
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(31),
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(30) + " " + string,
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get longer than 37 words of chat messages from the same user in immediate response",() => {
		let string = "a";
		spyOn(chatroomService,"message").and.returnValue(of({
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(38),
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}))
		component.user.id = 2;
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(37) + " " + string,
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get longer than 60 words of chat messages in immediate response",() => {
		let string = "a";
		spyOn(chatroomService,"message").and.returnValue(of({
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(61),
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(30) + " " + string.repeat(30) + " " + string,
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get longer than 74 words of chat messages from the same user in immediate response",() => {
		let string = "a";
		spyOn(chatroomService,"message").and.returnValue(of({
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(75),
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}))
		component.user.id = 2;
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(37) + " " + string.repeat(37) + " " + string,
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get longer than 90 words of chat messages in immediate response",() => {
		let string = "a";
		spyOn(chatroomService,"message").and.returnValue(of({
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(91),
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(30) + " " + string.repeat(30) + " " + string.repeat(30) + " " + string,
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get longer than 111 words of chat messages from the same user in immediate response",() => {
		let string = "a";
		spyOn(chatroomService,"message").and.returnValue(of({
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(112),
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}))
		component.user.id = 2;
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(37) + " " + string.repeat(37) + " " + string.repeat(37) + " " + string,
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get longer than 120 words of chat messages in immediate response",() => {
		let string = "a";
		spyOn(chatroomService,"message").and.returnValue(of({
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(121),
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatMessages).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			send_user_id:2,
			chatroom_id:1,
			content:string.repeat(30) + " " + string.repeat(30) + " " + string.repeat(30) + " " + string.repeat(30) + " "
			+ string,
			type:"text",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("should get chatroom existing users",() => {
		spyOn(chatroomService,"getChatroomUser").and.returnValue(of({success:true,data:[{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			is_admin:true
		}]}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatroomUsers).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png",
			is_admin:true
		}])
	})

	it("cannot get chatroom existing users",() => {
		spyOn(chatroomService,"getChatroomUser").and.returnValue(of({success:false}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatroomUsers).toEqual([])
	})

	it("cannot get image url from blob",() => {
		component.createImageFromBlob("" as any as Blob,1);
		expect(component.imageToShow).toEqual([]);
	})

	it("should get image from service",() => {
		spyOn(imageService,"getImageWithoutHeaders").and.returnValue(of(new Blob));
		component.getImageFromService("abc",1);
		expect(imageService.getImageWithoutHeaders).toHaveBeenCalledWith("abc");
	})

	it("should get image from service to catch error",() => {
		const mock = spyOn(imageService,"getImageWithoutHeaders").and.returnValue(throwError({status:404}));
		component.getImageFromService("abc",1);
		expect(mock).toHaveBeenCalledWith("abc");
	})

	it("cannot get image url from blob for chat users icon",() => {
		component.createImageFromBlobForChatUsers("" as any as Blob,1);
		expect(component.userImageToShow).toEqual([]);
	})

	it("should get image from service for chat users icon",() => {
		spyOn(imageService,"getImageWithoutHeaders").and.returnValue(of(new Blob));
		component.getImageFromServiceForChatUsers("abc",1);
		expect(imageService.getImageWithoutHeaders).toHaveBeenCalledWith("abc");
	})

	it("should get image from service to catch error for chat users icon",() => {
		const mock = spyOn(imageService,"getImageWithoutHeaders").and.returnValue(throwError({status:404}));
		component.getImageFromServiceForChatUsers("abc",1);
		expect(mock).toHaveBeenCalledWith("abc");
	})

	it("cannot get image url from blob for image message",() => {
		component.createImageFromBlobForImageMessage("" as any as Blob,1);
		expect(component.imageMessageToShow).toEqual([]);
	})

	it("should get image from service for image message",() => {
		spyOn(imageService,"getImageWithoutHeaders").and.returnValue(of(new Blob));
		component.getImageFromServiceForImageMessage("abc",1);
		expect(imageService.getImageWithoutHeaders).toHaveBeenCalledWith("abc");
	})

	it("should get image from service to catch error for image message",() => {
		const mock = spyOn(imageService,"getImageWithoutHeaders").and.returnValue(throwError({status:404}));
		component.getImageFromServiceForImageMessage("abc",1);
		expect(mock).toHaveBeenCalledWith("abc");
	})

	it("should send message",() => {
		component.newMessage = "abc";
		component.id = 1;
		component.user.id = 1;
		spyOn(chatroomService,"sendMessage").and.returnValue(of());
		component.sendMessage("abc");
		expect(chatroomService.sendMessage).toHaveBeenCalledWith("abc",1,1);
		expect(component.newMessage).toEqual("");
	})

	it("should preview image",() => {
		let image = new Blob();
		component.readURL({
			currentTarget:{
				files:[image]
			}
		})
		expect(component.newImageIcon).toEqual(image);
	})

	it("cannot preview image",() => {
		component.readURL({
			currentTarget:{
				files:[]
			}
		})
		expect(component.newImageIcon).toEqual(undefined);
	})

	// it("should send image message",async() => {
	// 	component.imageSource = "abc";
	// 	component.id = 1;
	// 	component.user.id = 1;
	// 	spyOn(chatroomService,"sendPhotoMessage").and.returnValue(of());
	// 	await component.sendPhoto(new File([new Blob()],"ronald.png"));
	// 	// tick();
	// 	expect(component.imageSource).toEqual("");
	// })

	it("should close photo",() => {
		component.imageSource = "abc";
		component.closePhoto();
		expect(component.imageSource).toEqual("");
	})

	it("should start recording",fakeAsync(() => {
		component.isRecording = false;
		component.isRecordingInterval = false;
		component.startRecording();
		expect(component.isRecording).toBe(true);
		tick(1000);
		expect(component.isRecordingInterval).toBe(true);
		discardPeriodicTasks();
	}))

	it("should stop recording",async() => {
		// const constraints = {video:false,audio:true};
		// let mediaStream = await navigator.mediaDevices.getUserMedia(constraints);
		// component.successCallback(mediaStream);
		component.isRecording = true;
		component.stopRecording();
		expect(component.isRecording).toBe(false);
	})

	it("should show recording error",() => {
		component.errorCallback("");
		expect(component.error).toEqual("cannot play audio in your browser")
	})

	it("should get file url from blob for sound message",() => {
		component.createMessageFromBlobForSoundMessage(new Blob,1);
		expect(component.soundMessageToShow).toEqual([]);
	})

	it("cannot get file url from blob for sound message",() => {
		component.createMessageFromBlobForSoundMessage("" as any as Blob,1);
		expect(component.soundMessageToShow).toEqual([]);
	})
})