import {Request, Response} from "express";
import {logger} from "../logger";
import {QuestionService} from "../services/QuestionService";
// import {format} from "date-fns";

export class QuestionController{
	constructor(private questionService:QuestionService){}

	loadQuestion = async(req:Request,res:Response) => {
		try{
			let questions = await this.questionService.loadQuestion()
			res.status(200).json({success:true,data:questions})
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`)
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	submitRecord = async(req:Request,res:Response) => {
		try{
			// console.log(req.user?.username);
			await this.questionService.submitRecord(req.body.time,req.user?.id,new Date());
			res.status(200).json({success:true});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}
}