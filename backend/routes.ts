import express from "express";
// import {isLoggedIn} from "./guards";
import {adminController,authController,bibleController,chatroomController,questionController,
settingController,userController} from "./main";
// import {upload} from "./main";
import {uploadWithAws} from "./main";

export const userRoutes = express.Router();
export const authRoutes = express.Router();
export const questionRoutes = express.Router();
export const chatroomRoutes = express.Router();
export const bibleRoutes = express.Router();
export const adminRoutes = express.Router();
export const settingRoutes = express.Router();

export function initRoutes(){
	userRoutes.post("/login",userController.login);
	userRoutes.post("/admin/login",userController.adminLogin);
	userRoutes.post("/newUser",uploadWithAws.single("icon"),userController.signUp);

	authRoutes.get("/currentUserCheck",authController.checkCurrentUser);

	questionRoutes.get("/questionBank",questionController.loadQuestion);
	questionRoutes.post("/answer",questionController.submitRecord);

	chatroomRoutes.get("/chatroom",chatroomController.loadChatroomList);
	chatroomRoutes.get("/room/:id",chatroomController.findChatroomInformation);
	chatroomRoutes.get("/chatroomMessage/:id",chatroomController.loadChatMessage);
	chatroomRoutes.post("/messages",chatroomController.sendMessage);
	chatroomRoutes.post("/photoMessages",uploadWithAws.single("photo"),chatroomController.sendPhotoMessage);
	chatroomRoutes.post("/voiceMessages",uploadWithAws.single("message"),chatroomController.sendVoiceMessage);
	chatroomRoutes.post("/personAdd",chatroomController.searchPerson);
	chatroomRoutes.post("/personNewSearch",chatroomController.searchNewPerson);
	chatroomRoutes.get("/users/:id",chatroomController.getChatroomUser);
	chatroomRoutes.post("/newRoom",uploadWithAws.single("icon"),chatroomController.createChatroom);
	chatroomRoutes.post("/newChatroomUsers/:id",chatroomController.addUserToChatroom);

	bibleRoutes.get("/scripture",bibleController.getBibleScripts);
	bibleRoutes.get("/scriptureDetail",bibleController.loadScriptureDetail);

	adminRoutes.get("/tasks",adminController.loadUnmarkedTasks);
	adminRoutes.get("/markedTasks",adminController.loadMarkedTasks);
	adminRoutes.post("/tasksWithRange",adminController.loadUnmarkedTasksWithRange);
	adminRoutes.post("/markedTasksWithRange",adminController.loadMarkedTasksWithRange);
	adminRoutes.get("/taskFinishDate/:id",adminController.loadTaskFinishDate);
	adminRoutes.get("/dailyAnswer/:id",adminController.loadUserAnswer);
	adminRoutes.get("/markingDone/:id",adminController.finishMarkingAnswer);

	settingRoutes.post("/password",settingController.changePassword);
	settingRoutes.post("/defaultPassword",settingController.changeDefaultPassword);
}