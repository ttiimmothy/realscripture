import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {ComponentFixture,fakeAsync,TestBed,tick} from '@angular/core/testing';
import {FormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";
import {ActivatedRoute,Router} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {SocketIoModule} from "ngx-socket-io";
import {of,throwError} from "rxjs";
import {config} from "../app.module";
import {BibleService} from "../bible.service";
import {BottomBarComponent} from "../bottom-bar/bottom-bar.component";
import {ChatroomService} from "../chatroom.service";
import {ChatroomComponent} from "../chatroom/chatroom.component";
import {ImageService} from "../image.service";
import {UserService} from "../user.service";
import {AddUserComponent} from './add-user.component';

describe("AddUserComponent",() => {
    let component:AddUserComponent;
    let fixture:ComponentFixture<AddUserComponent>;
	let userService:UserService;
	let bibleService:BibleService;
	let chatroomService:ChatroomService;
	let imageService:ImageService;
	let router:Router;
	let route:ActivatedRoute;
	let sanitizer:DomSanitizer;

    beforeEach(async() => {
        await TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				SocketIoModule.forRoot(config),
				MatIconModule,
				FormsModule,
				RouterTestingModule.withRoutes(
					[{path:"chat/1",component:ChatroomComponent}]
				)
			],
            declarations:[
				AddUserComponent,
				BottomBarComponent,
				// ChatroomComponent,
			],
			// providers:[
			// 	UserService,
			// 	BibleService,
			// 	ChatroomService,
			// 	ImageService,
			// 	{provide:ActivatedRoute,useValue:{
			// 		snapshot:{
			// 			paramMap:{
			// 				get():{id:string}{
			// 					return {id:"1"};
			// 				}
			// 			}
			// 		}
			// 	}},
			// ]
        }).compileComponents();
    })

    beforeEach(() => {
		router = TestBed.inject(Router);
        fixture = TestBed.createComponent(AddUserComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
		userService = TestBed.inject(UserService);
		bibleService = TestBed.inject(BibleService);
		chatroomService = TestBed.inject(ChatroomService);
		imageService = TestBed.inject(ImageService);
		route = TestBed.inject(ActivatedRoute);
		sanitizer = TestBed.inject(DomSanitizer);
		// component = new AddUserComponent(userService,bibleService,imageService,chatroomService,route,router,sanitizer);
		// router = jasmine.createSpyObj<Router>("Router",["navigate"]);
		localStorage.setItem("token","abc");
    })

    it("should create",() => {
        expect(component).toBeTruthy();
    })

	it("cannot get current user when localStorage token is empty",() => {
		localStorage.setItem("token","");
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy"}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("");
	})

	it("should get current user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy"}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("timothy");
	})

	it("cannot get current user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("");
	})

	it("should load bible",() => {
		spyOn(bibleService,"loadScripture").and.returnValue(of({success:true,data:{id:1,bible_id:1,content:"timo"}}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.goldenSentence).toEqual({id:1,bible_id:1,content:"timo"});
	})

	it("cannot load bible",() => {
		spyOn(bibleService,"loadScripture").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.goldenSentence).toEqual({id:0,bible_id:0,content:""});
	})

	it("should get chatroom users",() => {
		spyOn(chatroomService,"getChatroomUser").and.returnValue(of({success:true,data:[
			{
				id:1,
				username:"timothy",
				icon:"timo.png",
				is_admin:false
			}
		]}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.alreadyInGroupUsers).toEqual([
			{
				id:1,
				username:"timothy",
				icon:"timo.png"
			}
		])
	})

	it("cannot get chatroom users",() => {
		spyOn(chatroomService,"getChatroomUser").and.returnValue(of({success:false}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.alreadyInGroupUsers).toEqual([]);
	})

	it("should search user",() => {
		spyOn(chatroomService,"searchNewPerson").and.returnValue(of({success:true,data:[
			{
				id:1,
				username:"timothy",
				icon:"timo.png",
			}
		]}))
		component.searchNewUser("timothy",[{id:2,username:"timo",icon:""}],[{id:3,username:"ronald",icon:""}]);
		expect(component.usernames).toEqual([
			{
				id:1,
				username:"timothy",
				icon:"timo.png",
			}
		])
	})

	it("cannot search user without username",() => {
		spyOn(chatroomService,"searchNewPerson").and.returnValue(of({success:true,data:[
			{
				id:1,
				username:"timothy",
				icon:"timo.png",
			}
		]}))
		component.searchNewUser("",[],[]);
		expect(component.usernames).toEqual([]);
	})

	it("cannot search user",() => {
		spyOn(chatroomService,"searchNewPerson").and.returnValue(of({success:false}))
		component.searchNewUser("timothy",[],[]);
		expect(component.usernames).toEqual([]);
	})

	it("should add user to chatroom",() => {
		component.addUser(1,"timothy","ronald.png");
		expect(component.choseUsernames).toEqual([
			{
				id:1,
				username:"timothy",
				icon:"ronald.png",
			}
		])
	})

	it("should delete user",() => {
		component.choseUsernames = [
			{
				id:1,
				username:"timothy",
				icon:"ronald.png",
			}
		]
		component.deleteUser("timothy");
		expect(component.choseUsernames).toEqual([])
	})

	it("cannot get image url from blob",() => {
		component.createImageFromBlob("" as any as Blob,1);
		expect(component.imageToShow).toEqual([]);
	})

	it("should get image from service",() => {
		spyOn(imageService,"getImageWithoutHeaders").and.returnValue(of(new Blob));
		component.getImageFromService("abc",1);
		expect(imageService.getImageWithoutHeaders).toHaveBeenCalledWith("abc");
	})

	it("should get image from service to catch error",() => {
		const mock = spyOn(imageService,"getImageWithoutHeaders").and.returnValue(throwError({status:404}));
		component.getImageFromService("abc",1);
		expect(mock).toHaveBeenCalledWith("abc");
	})

	// when there is tick inside the testing, fakeAsync is required
	it("should add users to current chatroom",fakeAsync(() => {
		spyOn(chatroomService,"addUserToChatroom").and.returnValue(of({success:true}));
		// spyOn(router,"navigate").and.callThrough();
		let routerNew = spyOn(router,"navigate");
		component.ngOnInit();
		fixture.detectChanges();
		component.id = 1;
		component.addUserToChatroom([{id:1,username:"timothy",icon:"ronald.png"}],1);
		expect(chatroomService.addUserToChatroom).toHaveBeenCalledWith(["timothy"],1);
		tick(1000); // waiting for 1000ms
		// fixture.detectChanges();
		expect(routerNew).toHaveBeenCalled();
		expect(routerNew).toHaveBeenCalledWith(["/chat/1"]);
	}))

	it("cannot add users to current chatroom",() => {
		spyOn(chatroomService,"addUserToChatroom").and.returnValue(of({success:false}));
		component.ngOnInit();
  		fixture.detectChanges();
		component.addUserToChatroom([{id:1,username:"timothy",icon:"ronald.png"}],1);
		expect(chatroomService.addUserToChatroom).toHaveBeenCalledWith(["timothy"],1);
	})
})