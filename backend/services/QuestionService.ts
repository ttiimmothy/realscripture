import {Knex} from "knex";
import {logger} from "../logger";
import {Time} from "./model";

export class QuestionService{
	constructor(private knex:Knex){}

	async loadQuestion(){
		return await this.knex.select("*").from("question").orderBy("id")
	}

	async submitRecord(time:Time[],userId:number|undefined,date:Date){
		if(userId){
			let trx = await this.knex.transaction();
			try{
				let taskId = (await this.knex.insert({user_id:userId,finish_date:date}).into("admin_task")
				.returning("id"))[0]
				for(let i = 0; i < time.length; i++){
					await this.knex.insert({content:time[i]["time"],user_id:userId,remarks:time[i]["remark"],task_id:taskId})
					.into("answer");
				}
				let chatroomId = (await trx.select("*").from("chatroom").where("room_name","announcement"))[0].id;
				await trx.insert({send_user_id:userId,chatroom_id:chatroomId,content:time[time.length - 1]["remark"]})
				.into("group_message");
				await trx.commit();
			}catch(e){
				logger.error("knex transaction rollback");
				await trx.rollback();
				throw e;
			}
		}
	}
}