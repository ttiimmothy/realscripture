import {SettingController} from "./SettingController";
import {Request,Response} from "express";
import {SettingService} from "../services/SettingService";
import {Knex} from "knex";
import {logger} from "../logger";

describe("SettingController",() => {
	let settingService:SettingService;
	let settingController:SettingController;
	let req:Request;
	let res:Response;
	beforeEach(() => {
		settingService = new SettingService({} as Knex);

		jest.spyOn(settingService,"changePassword").mockImplementation((userId:number|undefined,password:string) =>
		Promise.resolve());

		jest.spyOn(settingService,"changeDefaultPassword").mockImplementation((isAdmin:boolean|undefined,defaultPassword:string) =>
		Promise.resolve())

		req = {
			params:{},
			body:{},
			query:{},
			user:{
				id:1,
				username:"timothy"
			}
		} as any as Request

		res = {
			json:jest.fn((obj:any) => null),
			status:jest.fn((statusCode:number) => res)
		} as any as Response

		logger.error = jest.fn()

		settingController = new SettingController(settingService);
	})

	it("should change password",async() => {
		req.body = {password:"ttiimmothhy"};
		await settingController.changePassword(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true})
	})

	it("should change default password",async() => {
		req.body = {password:"ttiimmothhy"};
		req.user!.is_admin = true;
		await settingController.changeDefaultPassword(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true})
	})

	it("should print error when change password get error",async () => {
		req.path = "/setting/password";
		req.method = "post";
		req.body = {password:"ttiimmothhy"};
		req.user = undefined;
		res = {} as any;
		try{
			await settingController.changePassword(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:post path:/setting/password")
		}
	})

	it("should print error when change default password get error",async () => {
		req.path = "/setting/defaultPassword";
		req.method = "post";
		req.body = {password:"ttiimmothhy"};
		req.user = undefined;
		res = {} as any;
		try{
			await settingController.changeDefaultPassword(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:post path:/setting/defaultPassword")
		}
	})
})