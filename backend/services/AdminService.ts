import {Knex} from "knex";
import {logger} from "../logger";
// import {format} from "date-fns";

export class AdminService{
	constructor(private knex:Knex){}

	async loadUnmarkedTasks(){
		return await this.knex.select("admin_task.id","username","finish_date").from("admin_task")
		.innerJoin("users","user_id","users.id").where("is_marked",false).orderBy("finish_date");
	}

	async loadMarkedTasks(){
		return await this.knex.select("admin_task.id","username","finish_date").from("admin_task")
		.innerJoin("users","user_id","users.id").where("is_marked",true).orderBy("finish_date");
	}

	async loadUnmarkedTasksWithRange(start:string,end:string){
		// console.log(new Date(start + " 00:00:00 GMT+0000"));
		// console.log(end);
		// console.log(new Date(end + " 23:59:59 GMT+0000"));
		return await this.knex.select("admin_task.id","username","finish_date").from("admin_task")
		.innerJoin("users","user_id","users.id").where("is_marked",false).where("finish_date",">=",
		new Date(start + " 00:00:00 GMT+0000")).andWhere("finish_date","<=",new Date(end + " 23:59:59 GMT+0000"));
	}

	async loadMarkedTasksWithRange(start:string,end:string){
		return await this.knex.select("admin_task.id","username","finish_date").from("admin_task")
		.innerJoin("users","user_id","users.id").where("is_marked",true).where("finish_date",">=",
		new Date(start + " 00:00:00 GMT+0000")).andWhere("finish_date","<=",new Date(end + " 23:59:59 GMT+0000"));
	}

	async loadTaskFinishDate(taskId:number){
		return await this.knex.select("user_id","username","finish_date").from("admin_task").innerJoin("users","users.id","user_id")
		.where("admin_task.id",taskId);
	}

	async loadUserAnswer(taskId:number){
		let trx = await this.knex.transaction();
		try{
			let answers = await this.knex.select("answer.id","username","content","remarks","admin_task.is_marked").from("answer")
			.innerJoin("users","users.id","user_id").innerJoin("admin_task","admin_task.id","answer.task_id")
			.where("task_id",taskId).orderBy("answer.id");
			let questions = await this.knex.select("content").from("question").orderBy("id");
			// console.log(answers)
			// console.log(questions);
			for(let i = 0; i < answers.length; i++){
				answers[i].question = questions[i].content
			}
			// console.log(answers);
			await trx.commit()
			return answers;
		}catch(e){
			await trx.rollback();
			logger.error("knex transaction rollback");
			throw e;
		}
	}

	async finishMarkingAnswer(taskId:number){
		await this.knex("admin_task").update({is_marked:true}).where("id",taskId);
	}
}