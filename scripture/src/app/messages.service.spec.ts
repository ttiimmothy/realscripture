import {TestBed} from '@angular/core/testing';
import {MessagesService} from './messages.service';

describe('MessagesService',() => {
	let service:MessagesService;

	beforeEach(() => {
		TestBed.configureTestingModule({
		})
		service = TestBed.inject(MessagesService);
	})

	it('should be created',() => {
		expect(service).toBeTruthy();
	})

	it("should add message",() => {
		service.add("hello");
		expect(service.messageList).toEqual(["hello"]);
	})

	it("should clear messages",() => {
		service.clear();
		expect(service.messageList).toEqual([]);
	})
})