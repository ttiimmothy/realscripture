import {HttpClient,HttpErrorResponse,HttpHandler} from "@angular/common/http";
import {HttpClientTestingModule,HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from '@angular/core/testing';
import {Observable, of} from "rxjs";
import {AdminHomeService} from './admin-home.service';

describe('AdminHomeService', () => {
    let service:AdminHomeService;
	// let http:HttpClient;
	let httpTestingController:HttpTestingController
	let handleError:(operation?:string,result?:any) => Observable<any>;

    beforeEach(() => {
		TestBed.configureTestingModule({
			imports:[HttpClientTestingModule]
		})
		httpTestingController = TestBed.inject(HttpTestingController);
        service = TestBed.inject(AdminHomeService);
		handleError = (operation = "",result = "") => of("")
    })

    it('should be created', () => {
        expect(service).toBeTruthy();
    })

	it("searchTasks should able to search task",(done) => {
		let result = "";
		service.searchTasks("","").subscribe((data) => {
			expect(data).toEqual(result);
			done();
		})
		const testRequest = httpTestingController.expectOne('http://localhost:8160/admin/tasksWithRange');
		testRequest.flush(result);
	})

	it("searchMarkedTasks should able to search marked task",(done) => {
		let result = "";
		service.searchMarkedTasks("","").subscribe((data) => {
			expect(data).toEqual(result);
			done();
		})
		const testRequest = httpTestingController.expectOne('http://localhost:8160/admin/markedTasksWithRange');
		testRequest.flush(result);
	})

	it("finishMarking should able to finish mark",(done) => {
		let result = "";
		service.finishMarking(1).subscribe((data) => {
			expect(data).toEqual(result);
			done();
		})
		const testRequest = httpTestingController.expectOne('http://localhost:8160/admin/markingDone/1');
		testRequest.flush(result);
	})

	it("finishMarking should catch error when the link is incorrect",() => {
		const mockErrorResponse:HttpErrorResponse = new HttpErrorResponse({error:{},status:500,url:
		'http://localhost:8160/admin/markingDone/1',statusText:"Bad Request"});
		service.finishMarking(1).subscribe((data) => {
			// fail()
		},(error) => {
			spyOn(console,"error").and.returnValue();
			expect(console.error).toHaveBeenCalledWith(`finish marking error:${mockErrorResponse.error}`);
		})
		const testRequest = httpTestingController.expectOne(mockErrorResponse.url as string);
		expect(testRequest.request.method).toBe("GET");
		testRequest.flush(mockErrorResponse);
	})
})