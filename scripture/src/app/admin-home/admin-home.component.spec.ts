import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ComponentFixture,TestBed} from '@angular/core/testing';
import {FormsModule} from "@angular/forms";
import {MatNativeDateModule} from "@angular/material/core";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {Router} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import * as moment from "moment";
import {of} from "rxjs";
import {AdminHomeService} from "../admin-home.service";
import {AdminComponent} from "../admin/admin.component";
import {BibleService} from "../bible.service";
import {BottomBarComponent} from "../bottom-bar/bottom-bar.component";
import {UserService} from "../user.service";
import {AdminHomeComponent} from './admin-home.component';

describe('AdminHomeComponent',() => {
    let component:AdminHomeComponent;
    let fixture:ComponentFixture<AdminHomeComponent>;
	let userService:UserService;
	let bibleService:BibleService;
	let adminHomeService:AdminHomeService;
	let router:Router;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				RouterTestingModule.withRoutes(
					[{path:"admin/login",component:AdminComponent}]
				),
				MatIconModule,
				MatDatepickerModule,
				MatNativeDateModule,
				MatFormFieldModule,
				MatInputModule,
				BrowserAnimationsModule,
				FormsModule
			],
            declarations:[
				AdminHomeComponent,
				BottomBarComponent
			]
        }).compileComponents();
    })

    beforeEach(() => {
        fixture = TestBed.createComponent(AdminHomeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
		userService = TestBed.inject(UserService);
		bibleService = TestBed.inject(BibleService);
		adminHomeService = TestBed.inject(AdminHomeService);
		router = TestBed.inject(Router);
		localStorage.setItem("token","abc");
    })

    it("should create",() => {
        expect(component).toBeTruthy();
    })

	it("cannot get current user when localStorage token is empty",() => {
		localStorage.setItem("token","");
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy"}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("");
	})

	it("should get current user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy"}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("timothy");
	})

	it("cannot get current user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("");
	})

	it("should get bible",() => {
		spyOn(bibleService,"loadScripture").and.returnValue(of({success:true,data:{id:1,bible_id:1,content:"timo"}}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.goldenSentence).toEqual({id:1,bible_id:1,content:"timo"});
	})

	it("cannot get bible",() => {
		spyOn(bibleService,"loadScripture").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.goldenSentence).toEqual({id:0,bible_id:0,content:""});
	})

	it("should get unmarked tasks",() => {
		spyOn(adminHomeService,"loadTasks").and.returnValue(of({success:true,data:[{id:1,username:"timothy",
		finish_date:new Date("2021-08-24")}]}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.unmarkedTasks).toEqual([{id:1,username:"timothy",finish_date:new Date("2021-08-24")}]);
	})

	it("cannot get unmarked tasks",() => {
		spyOn(adminHomeService,"loadTasks").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.unmarkedTasks).toEqual([]);
	})

	it("should get marked tasks",() => {
		spyOn(adminHomeService,"loadMarkedTasks").and.returnValue(of({success:true,data:[{id:1,username:"timothy",
		finish_date:new Date("2021-08-24")}]}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.markedTasks).toEqual([{id:1,username:"timothy",finish_date:new Date("2021-08-24")}]);
	})

	it("cannot get marked tasks",() => {
		spyOn(adminHomeService,"loadMarkedTasks").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.markedTasks).toEqual([]);
	})

	it("should search unmarked tasks",() => {
		spyOn(adminHomeService,"searchTasks").and.returnValue(of({success:true,data:[{id:1,username:"timothy",
		finish_date:new Date("2021-08-24")}]}));
		component.ngOnInit();
		fixture.detectChanges();
		component.searchTasks(moment("2021-08-24"),moment("2021-08-25"));
		expect(component.unmarkedTasks).toEqual([{id:1,username:"timothy",finish_date:new Date("2021-08-24")}]);
	})

	it("cannot search unmarked tasks when end date is earlier than start date",() => {
		spyOn(adminHomeService,"searchTasks").and.returnValue(of({success:true,data:[{id:1,username:"timothy",
		finish_date:new Date("2021-08-24")}]}));
		component.ngOnInit();
		fixture.detectChanges();
		component.searchTasks(moment("2021-08-25"),moment("2021-08-24"));
		expect(component.unmarkedTasks).toEqual([]);
	})

	it("cannot search unmarked tasks when end date equals to start date",() => {
		spyOn(adminHomeService,"searchTasks").and.returnValue(of({success:true,data:[{id:1,username:"timothy",
		finish_date:new Date("2021-08-24")}]}));
		component.ngOnInit();
		fixture.detectChanges();
		component.searchTasks(moment("2021-08-24"),moment("2021-08-24"));
		expect(component.unmarkedTasks).toEqual([]);
	})

	it("cannot search unmarked tasks",() => {
		spyOn(adminHomeService,"searchTasks").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		component.searchTasks(moment("2021-08-24"),moment("2021-08-25"));
		expect(component.unmarkedTasks).toEqual([]);
	})

	it("should reset unmarked tasks search",() => {
		spyOn(adminHomeService,"loadTasks").and.returnValue(of({success:true,data:[{id:1,username:"timothy",
		finish_date:new Date("2021-08-24")}]}));
		component.ngOnInit();
		fixture.detectChanges();
		component.resetSearch();
		expect(component.unmarkedTasks).toEqual([{id:1,username:"timothy",finish_date:new Date("2021-08-24")}]);
	})

	it("cannot reset unmarked tasks search",() => {
		spyOn(adminHomeService,"loadTasks").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		component.resetSearch();
		expect(component.unmarkedTasks).toEqual([]);
	})

	it("should search marked tasks",() => {
		spyOn(adminHomeService,"searchMarkedTasks").and.returnValue(of({success:true,data:[{id:1,username:"timothy",
		finish_date:new Date("2021-08-24")}]}));
		component.ngOnInit();
		fixture.detectChanges();
		component.searchMarkedTasks(moment("2021-08-24"),moment("2021-08-25"));
		expect(component.markedTasks).toEqual([{id:1,username:"timothy",finish_date:new Date("2021-08-24")}]);
	})

	it("cannot search marked tasks when end date is earlier than start date",() => {
		spyOn(adminHomeService,"searchMarkedTasks").and.returnValue(of({success:true,data:[{id:1,username:"timothy",
		finish_date:new Date("2021-08-24")}]}));
		component.ngOnInit();
		fixture.detectChanges();
		component.searchMarkedTasks(moment("2021-08-25"),moment("2021-08-24"));
		expect(component.markedTasks).toEqual([]);
	})

	it("cannot search marked tasks when end date equals to start date",() => {
		spyOn(adminHomeService,"searchMarkedTasks").and.returnValue(of({success:true,data:[{id:1,username:"timothy",
		finish_date:new Date("2021-08-24")}]}));
		component.ngOnInit();
		fixture.detectChanges();
		component.searchMarkedTasks(moment("2021-08-24"),moment("2021-08-24"));
		expect(component.markedTasks).toEqual([]);
	})

	it("cannot search marked tasks",() => {
		spyOn(adminHomeService,"searchMarkedTasks").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		component.searchMarkedTasks(moment("2021-08-24"),moment("2021-08-25"));
		expect(component.markedTasks).toEqual([]);
	})

	it("should reset marked tasks search",() => {
		spyOn(adminHomeService,"loadMarkedTasks").and.returnValue(of({success:true,data:[{id:1,username:"timothy",
		finish_date:new Date("2021-08-24")}]}));
		component.ngOnInit();
		fixture.detectChanges();
		component.resetMarkedTasksSearch();
		expect(component.markedTasks).toEqual([{id:1,username:"timothy",finish_date:new Date("2021-08-24")}]);
	})

	it("cannot reset marked tasks search",() => {
		spyOn(adminHomeService,"loadMarkedTasks").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		component.resetMarkedTasksSearch();
		expect(component.markedTasks).toEqual([]);
	})

	it("toggle unmarked button to marked",() => {
		component.unmarked = true;
		component.ngOnInit();
		fixture.detectChanges();
		component.toggleUnmarked();
		expect(component.unmarked).toEqual(false);
	})

	it("should logout",() => {
		let routerNew = spyOn(router,"navigate");
		component.logout();
		expect(localStorage.getItem("token")).toEqual(null);
		expect(routerNew).toHaveBeenCalledWith(["/admin/login"]);
	})
})