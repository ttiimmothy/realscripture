import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from '@angular/core/testing';
import {ImageService} from './image.service';

describe('ImageService',() => {
	let service:ImageService;
	let httpTestingController:HttpTestingController;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports:[HttpClientTestingModule]
		})
		service = TestBed.inject(ImageService);
		httpTestingController = TestBed.inject(HttpTestingController);
	})

	it('should be created',() => {
		expect(service).toBeTruthy();
	})

	it("should get image",() => {
		let expected = new Blob;
		service.getImage("").subscribe((result) => {
			expect(result).toEqual(expected);
		})
		let testService = httpTestingController.expectOne("");
		testService.flush(expected);
	})

	it("should get image",() => {
		let expected = new Blob;
		service.getImageWithoutHeaders("").subscribe((result) => {
			expect(result).toEqual(expected);
		})
		let testService = httpTestingController.expectOne("");
		testService.flush(expected);
	})
})
