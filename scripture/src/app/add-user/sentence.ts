export interface Sentence{
	id:number;
	bible_id:number;
	content:string;
}
export interface NewUser{
	id:number;
	username:string;
	icon:string;
}
export interface ChatUser{
	id:number;
	username:string;
	icon:string;
	is_admin:boolean;
}