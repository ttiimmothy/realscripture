import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("chatroom",(table) => {
		table.increments();
		table.string("room_name");
		table.string("room_icon");
		table.timestamps(false,true)
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("chatroom");
}