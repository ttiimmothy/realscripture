import {Request, Response} from "express";
import {Knex} from "knex";
import {logger} from "../logger";
import {BibleService} from "../services/BibleService";
import {BibleController} from "./BibleController";
// import path from "path";
// import dotenv from "dotenv";
// dotenv.config({path:path.resolve(__dirname, "./.env")});

describe("BibleController",() => {
	let bibleController:BibleController;
	let bibleService:BibleService;
	let req:Request;
	let res:Response;

	beforeEach(() => {
		bibleService = new BibleService({} as Knex);

		jest.spyOn(bibleService,"getBibleScripts").mockImplementation((date:number) =>
		Promise.resolve([{id:1,bible_id:1,content:"hello world"}]));

		jest.spyOn(bibleService,"newScripture").mockImplementation((date:number,content:string) => Promise.resolve(
		[{id:1,bible_id:1,content:"hello world"}]))

		jest.spyOn(bibleService,"loadScriptureDetailId").mockImplementation(() => Promise.resolve([{bible_id:1}]));

		jest.spyOn(bibleService,"insertNewScripture").mockImplementation((id:number,content:string) => Promise.resolve());

		jest.spyOn(bibleService,"loadScriptureDetail").mockImplementation(() =>
		Promise.resolve([{id:1,bible_id:1,content:"hello"}]));

		bibleController = new BibleController(bibleService);

		req = {
			body:{},
			params:{},
			query:{},
			user:{
				id:1,
				username:"timothy"
			}
		} as any as Request;

		res = {
			json:jest.fn((obj:any) => null),
			status:jest.fn((statusCode:number) => res)
		} as any as Response;

		logger.error = jest.fn();
	})

	it("should get specific scripture",async() => {
		await bibleController.getBibleScripts(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:{id:1,bible_id:1,content:"hello world"}});
	})

	it("should get specific scripture through api when database has no that date data",async() => {
		jest.spyOn(bibleService,"getBibleScripts").mockImplementation((date:number) => Promise.resolve([]));
		await bibleController.getBibleScripts(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:{id:1,bible_id:1,content:"hello world"}});
	})

	it("should get all bible details",async() => {
		await bibleController.loadScriptureDetail(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:[{id:1,bible_id:1,content:"hello"}]});
	})

	it("should print error when getting bible script get error",async () => {
		req.path = "/bible/scripture";
		req.method = "get";
		res = {} as any
		try{
			await bibleController.getBibleScripts(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:get path:/bible/scripture")
		}
	})

	it("should print error when getting all bible scripts details get error",async () => {
		req.path = "/bible/scriptureDetail";
		req.method = "get";
		res = {} as any
		try{
			await bibleController.loadScriptureDetail(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:get path:/bible/scriptureDetail")
		}
	})
})