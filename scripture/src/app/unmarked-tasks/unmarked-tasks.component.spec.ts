import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {MatIconModule} from "@angular/material/icon";
import {Router} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {of} from "rxjs";
import {AdminHomeService} from "../admin-home.service";
import {AdminHomeComponent} from "../admin-home/admin-home.component";
import {BibleService} from "../bible.service";
import {BottomBarComponent} from "../bottom-bar/bottom-bar.component";
import {UserService} from "../user.service";
import {UnmarkedTasksComponent} from './unmarked-tasks.component';

describe("UnmarkedTasksComponent",() => {
    let component:UnmarkedTasksComponent;
    let fixture:ComponentFixture<UnmarkedTasksComponent>;
	let userService:UserService;
	let bibleService:BibleService;
	let adminHomeService:AdminHomeService;
	let router:Router;

    beforeEach(async() => {
        await TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				RouterTestingModule.withRoutes(
					[{path:"home/admin",component:AdminHomeComponent}]
				),
				MatIconModule
			],
            declarations:[
				UnmarkedTasksComponent,
				BottomBarComponent
			],
        }).compileComponents();
    })

    beforeEach(() => {
        fixture = TestBed.createComponent(UnmarkedTasksComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
		userService = TestBed.inject(UserService);
		bibleService = TestBed.inject(BibleService);
		adminHomeService = TestBed.inject(AdminHomeService);
		router = TestBed.inject(Router);
		localStorage.setItem("token","abc");
    })

    it("should create",() => {
        expect(component).toBeTruthy();
    })

	it("cannot get current user when localStorage token is empty",() => {
		localStorage.setItem("token","");
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy"}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("");
	})

	it("should get current user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy"}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("timothy");
	})

	it("cannot get current user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("");
	})

	it("should get bible",() => {
		spyOn(bibleService,"loadScripture").and.returnValue(of({success:true,data:{id:1,bible_id:1,content:"timo"}}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.goldenSentence).toEqual({id:1,bible_id:1,content:"timo"});
	})

	it("cannot get bible",() => {
		spyOn(bibleService,"loadScripture").and.returnValue(of({success:false,data:{id:1,bible_id:1,content:"timo"}}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.goldenSentence).toEqual({id:0,bible_id:0,content:""});
	})

	it("should get task information",() => {
		spyOn(adminHomeService,"loadTaskFinishDate").and.returnValue(of({success:true,data:{
			user_id:1,
			username:"timothy",
			finish_date:"2021-08-24"
		}}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.taskInfo).toEqual({
			user_id:1,
			username:"timothy",
			finish_date:"2021-08-24"
		})
	})

	it("cannot get task information",() => {
		spyOn(adminHomeService,"loadTaskFinishDate").and.returnValue(of({success:false}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.taskInfo).toEqual({
			user_id:0,
			username:"",
			finish_date:""
		})
	})

	it("should get user answer",() => {
		spyOn(adminHomeService,"loadUserAnswer").and.returnValue(of({success:true,data:[{
			id:1,
			username:"timothy",
			content:"ronald",
			remarks:"ronald",
			question:"timo",
			is_marked:true
		}]}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.answers).toEqual([{
			id:1,
			username:"timothy",
			content:"ronald",
			remarks:"ronald",
			question:"timo",
			is_marked:true
		}])
	})

	it("cannot get user answer",() => {
		spyOn(adminHomeService,"loadUserAnswer").and.returnValue(of({success:false}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.answers).toEqual([])
	})

	it("should get unmarked task user answer",() => {
		spyOn(adminHomeService,"loadUserAnswer").and.returnValue(of({success:true,data:[{
			id:1,
			username:"timothy",
			content:"ronald",
			remarks:"ronald",
			question:"timo",
			is_marked:false
		}]}))
		component.isMarked = true;
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.answers).toEqual([{
			id:1,
			username:"timothy",
			content:"ronald",
			remarks:"ronald",
			question:"timo",
			is_marked:false
		}])
		expect(component.isMarked).toBe(false);
	})

	it("should make visible",() => {
		component.showAnswer = [false,false];
		component.makeVisible(1);
		expect(component.showAnswer).toEqual([false,true]);
	})

	it("should make hidden",() => {
		component.showAnswer = [true,true];
		component.makeHidden(1);
		expect(component.showAnswer).toEqual([true,false]);
	})

	it("should finish marking",fakeAsync(() => {
		spyOn(adminHomeService,"finishMarking").and.returnValue(of({success:true}));
		let routerNew = spyOn(router,"navigate");
		component.finishMarking();
		tick(1000);
		expect(routerNew).toHaveBeenCalledWith(["/home/admin"]);
	}))

	it("cannot finish marking",fakeAsync(() => {
		spyOn(adminHomeService,"finishMarking").and.returnValue(of({success:false}));
		let routerNew = spyOn(router,"navigate");
		component.finishMarking();
		tick(1000);
		expect(routerNew).not.toHaveBeenCalled();
	}))
})