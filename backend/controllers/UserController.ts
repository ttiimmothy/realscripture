import {Request,Response} from "express";
import {logger} from "../logger";
import {UserService} from "../services/UserService";
import jwt from "jwt-simple";
import {checkPassword} from "../hash";

export class UserController{
	constructor(private userService:UserService){}

	login = async(req:Request,res:Response) => {
		try{
			// console.log(req.body);
			let user = (await this.userService.login(parseInt(req.body.phoneNumber)))[0]
			// console.log(user)
			if(!user){
				res.json({success:false,message:"user does not exist"})
				return
			}
			let password = (await this.userService.getUserPassword(user.id))[0].password;
			// console.log(password);
			if(!(await checkPassword(req.body.password,password))){
				res.json({success:false,message:"password is not correct"});
				return;
			}
			const token = jwt.encode({
				id:user.id,
				phone:user.phone
			},process.env.JWT_SECRET!)
			res.status(200).json({success:true,data:token,user:user.username})
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	adminLogin = async(req:Request,res:Response) => {
		try{
			// console.log(req.body);
			let user = (await this.userService.adminLogin(parseInt(req.body.phoneNumber)))[0]
			// console.log(user)
			if(!user){
				res.json({success:false,message:"this user is not admin"});
				return;
			}
			let password = (await this.userService.getUserPassword(user.id))[0].password;
			// console.log(password);
			if(!(await checkPassword(req.body.password,password))){
				res.json({success:false,message:"password is not correct"});
				return;
			}
			const token = jwt.encode({
				id:user.id,
				phone:user.phone
			},process.env.JWT_SECRET!)
			res.status(200).json({success:true,data:token,user:user.username})
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	signUp = async(req:Request,res:Response) => {
		try{
			if(req.file){
				req.body.icon = (req.file as any).key;
				// req.body.icon = req.file.filename;
				await this.userService.signUp(req.body.username,parseInt(req.body.phoneNumber),req.body.icon);
			}else{
				await this.userService.signUp(req.body.username,parseInt(req.body.phoneNumber));
			}
			res.status(200).json({success:true});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()});
		}
	}
}