import {Knex} from "knex";

export class AuthService{
	constructor(private knex:Knex){}

	async checkCurrentUser(phone:number){
		return await this.knex.select("id","username","phone","icon","is_admin","qr_code").from("users")
		.where("phone",phone);
	}
}