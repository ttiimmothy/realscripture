import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("admin_task",(table) => {
		table.increments();
		table.integer("user_id").unsigned();
		table.foreign("user_id").references("users.id");
		table.dateTime("finish_date");
		table.boolean("is_marked").defaultTo(false);
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("admin_task");
}