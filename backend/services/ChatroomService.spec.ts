import {ChatroomService} from "./ChatroomService";
import Knex from "knex";
import {logger} from "../logger";
const knexfile = require("../knexfile");
const knex = Knex(knexfile["testing"]);

describe("ChatroomService",() => {
	let chatroomService:ChatroomService;
	let chatroomId:any;
	let userId:any

	beforeEach(async() => {
		chatroomId = (await knex.insert({room_name:"timothy",room_icon:"daniel"}).into("chatroom").returning("id"))[0];
		userId = (await knex.insert({username:"timothy",phone:11222333,icon:"",qr_code:""}).into("users")
		.returning("id"))[0];
		await knex.insert({user_id:userId,chatroom_id:chatroomId}).into("user_chatroom");
		await knex.insert({send_user_id:userId,chatroom_id:chatroomId,content:"hello"}).into("group_message");
		chatroomService = new ChatroomService(knex);
		logger.error = jest.fn();
	})

	it("should get chatroom list",async() => {
		let chatroomList = await chatroomService.loadChatroomList(userId);
		expect(chatroomList).toMatchObject([{
			user_id:userId,
			chatroom_id:chatroomId,
			room_name:"timothy",
			room_icon:"daniel"
		}])
	})

	it("should get chatroom information",async() => {
		let information = await chatroomService.findChatroomInformation(chatroomId);
		expect(information).toMatchObject([{
			room_name:"timothy",
			room_icon:"daniel"
		}])
	})

	it("should get chatroom message",async() => {
		let messages = await chatroomService.loadChatMessage(chatroomId);
		expect(messages).toMatchObject([{
			username:"timothy",
			icon:"",
			send_user_id:userId,
			chatroom_id:chatroomId,
			content:"hello"
		}])
	})

	it("should send message",async() => {
		let sendMessage = await chatroomService.sendMessage("timo",chatroomId,userId);
		expect(sendMessage).toMatchObject([{
			username:"timothy",
			icon:"",
			send_user_id:userId,
			chatroom_id:chatroomId,
			content:"timo",
			type:"text"
		}])
	})

	it("should send image message",async() => {
		let sendPhotoMessage = await chatroomService.sendPhotoMessage("timo",chatroomId,userId,1);
		expect(sendPhotoMessage).toMatchObject([{
			username:"timothy",
			icon:"",
			send_user_id:userId,
			chatroom_id:chatroomId,
			content:"timo",
			type:"image"
		}])
	})

	it("should send sound message",async() => {
		let sendPhotoMessage = await chatroomService.sendVoiceMessage("timo",chatroomId,userId,1);
		expect(sendPhotoMessage).toMatchObject([{
			username:"timothy",
			icon:"",
			send_user_id:userId,
			chatroom_id:chatroomId,
			content:"timo",
			type:"sound"
		}])
	})

	it("should not send image message size",async() => {
		let sendPhotoMessage = await chatroomService.sendPhotoMessage("timo",chatroomId,userId,undefined);
		expect(sendPhotoMessage).toMatchObject([{
			username:"timothy",
			icon:"",
			send_user_id:userId,
			chatroom_id:chatroomId,
			content:"timo",
			type:"image"
		}])
	})

	it("should not send sound message size",async() => {
		let sendPhotoMessage = await chatroomService.sendVoiceMessage("timo",chatroomId,userId,undefined);
		expect(sendPhotoMessage).toMatchObject([{
			username:"timothy",
			icon:"",
			send_user_id:userId,
			chatroom_id:chatroomId,
			content:"timo",
			type:"sound"
		}])
	})

	it("should search person in creating chatroom",async() => {
		let user = await chatroomService.searchPerson("timothy",[],2);
		expect(user).toMatchObject([{
			id:userId,
			username:"timothy",
			icon:""
		}])
	})

	it("should search person in adding people to chatroom",async() => {
		await knex.insert({username:"timo",icon:"daniel",phone:33322211,qr_code:""}).into("users");
		let user = await chatroomService.searchNewPerson("timothy",[],["timo"]);
		expect(user).toMatchObject([{
			id:userId,
			username:"timothy",
			icon:""
		}])
	})

	it("should create chatroom",async() => {
		let userIds:number = await knex.insert({username:"timo",icon:"daniel",phone:33322211,qr_code:""})
		.into("users").returning("id");
		// userIds[0] is adminId
		await chatroomService.createChatroom("dennis","daniel",["timothy"],userIds[0]);
		let chatroomList = await knex.select("chatroom.*","user_chatroom.*").from("chatroom").innerJoin("user_chatroom",
		"chatroom_id","chatroom.id").innerJoin("users","user_id","users.id").orderBy("chatroom.id").orderBy("users.id");
		let newChatroomId = (await knex.select("id").from("chatroom").where("room_name","dennis"))[0].id;
		expect(chatroomList).toMatchObject([
			{
				user_id:userId,
				chatroom_id:chatroomId,
				room_name:"timothy",
				room_icon:"daniel",
				is_admin:false
			},
			{
				user_id:userId,
				chatroom_id:newChatroomId,
				room_name:"dennis",
				room_icon:"daniel",
				is_admin:false
			},
			{
				user_id:userIds[0],
				chatroom_id:newChatroomId,
				room_name:"dennis",
				room_icon:"daniel",
				is_admin:true
			}
		])
	})

	it("should create chatroom without admin",async() => {
		// let userIds:number = await knex.insert({username:"timo",icon:"daniel",phone:33322211,qr_code:""})
		// .into("users").returning("id");
		await chatroomService.createChatroom("dennis","daniel",["timothy"],undefined);
		let chatroomList = await knex.select("chatroom.*","user_chatroom.*").from("chatroom").innerJoin("user_chatroom",
		"chatroom_id","chatroom.id").innerJoin("users","user_id","users.id").orderBy("chatroom.id").orderBy("users.id");
		let newChatroomId = (await knex.select("id").from("chatroom").where("room_name","dennis"))[0].id;
		expect(chatroomList).toMatchObject([
			{
				user_id:userId,
				chatroom_id:chatroomId,
				room_name:"timothy",
				room_icon:"daniel",
				is_admin:false
			},
			{
				user_id:userId,
				chatroom_id:newChatroomId,
				room_name:"dennis",
				room_icon:"daniel",
				is_admin:false
			}
		])
	})

	it("should add users to current chatroom",async() => {
		let userIds:number = await knex.insert({username:"timo",icon:"daniel",phone:33322211,qr_code:""})
		.into("users").returning("id");
		await chatroomService.addUserToChatroom(["timo"],chatroomId);
		let chatroomList = await knex.select("chatroom.*","user_chatroom.*").from("chatroom").innerJoin("user_chatroom",
		"chatroom_id","chatroom.id").innerJoin("users","user_id","users.id").orderBy("chatroom.id").orderBy("users.id");
		expect(chatroomList).toMatchObject([
			{
				user_id:userId,
				chatroom_id:chatroomId,
				room_name:"timothy",
				room_icon:"daniel"
			},
			{
				user_id:userIds[0],
				chatroom_id:chatroomId,
				room_name:"timothy",
				room_icon:"daniel"
			}
		])
	})

	it("should get chatroom users",async() => {
		let chatroomUsers = await chatroomService.getChatroomUser(chatroomId);
		expect(chatroomUsers).toMatchObject([{
			id:userId,
			username:"timothy",
			icon:"",
			is_admin:false
		}])
	})

	it("should get undefined when calling chatroom list database if req.user.id is undefined",async() => {
		let chatroomList = await chatroomService.loadChatroomList(undefined);
		expect(chatroomList).toBe(undefined)
	})

	it("should get undefined when searching person to add to a new chatroom if req.user.id is undefined",async() => {
		let users = await chatroomService.searchPerson("timo",[],undefined);
		expect(users).toBe(undefined)
	})

	it("should print error and rollback in transaction when sending message get error",async() => {
		try{
			await chatroomService.sendMessage("","" as any as number,"" as any as number);
			// console.log(task)
		}catch(e){
			expect(logger.error).toBeCalledTimes(1);
			expect(logger.error).toBeCalledWith("knex transaction rollback")
		}
	})

	it("should print error and rollback in transaction when sending image message get error",async() => {
		try{
			await chatroomService.sendPhotoMessage("","" as any as number,"" as any as number,0);
			// console.log(task)
		}catch(e){
			expect(logger.error).toBeCalledTimes(1);
			expect(logger.error).toBeCalledWith("knex transaction rollback")
		}
	})

	it("should print error and rollback in transaction when sending sound message get error",async() => {
		try{
			await chatroomService.sendVoiceMessage("","" as any as number,"" as any as number,0);
			// console.log(task)
		}catch(e){
			expect(logger.error).toBeCalledTimes(1);
			expect(logger.error).toBeCalledWith("knex transaction rollback")
		}
	})

	it("should print error and rollback in transaction when creating chatroom get error",async() => {
		try{
			await chatroomService.createChatroom("","",undefined as any as string[],undefined);
		}catch(e){
			expect(logger.error).toBeCalledTimes(1);
			expect(logger.error).toBeCalledWith("knex transaction rollback")
		}
	})

	it("should print error and rollback in transaction when adding person to existing chatroom get error",async() => {
		try{
			await chatroomService.addUserToChatroom(undefined as any as string[],"" as any as number);
		}catch(e){
			expect(logger.error).toBeCalledTimes(1);
			expect(logger.error).toBeCalledWith("knex transaction rollback")
		}
	})

	afterEach(async() => {
		await knex("group_message").del();
		await knex("user_chatroom").del();
		await knex("users").del();
		await knex("chatroom").del();
		await knex("file_detail").del();
	})

	afterAll(async() => {
		await knex.destroy();
	})
})