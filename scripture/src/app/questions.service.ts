import {HttpClient,HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable,of} from "rxjs";
import {catchError} from "rxjs/operators";
import {environment} from "src/environments/environment";
import {Time} from "./home/question";
// import {Question} from "./question";
// import {MessagesService} from "./messages.service";

@Injectable({
	providedIn:"root"
})
export class QuestionsService {
	constructor(private http:HttpClient){}
	token = localStorage.getItem("token");
	// authOptions = {
	// 	headers:new HttpHeaders({"Authorization":`Bearer ${this.token}`})
	// }
	postOptions = {
		headers:new HttpHeaders({"Authorization":`Bearer ${this.token}`,"Content-type":"application/json"})
	}
	loadQuestion():Observable<any>{
		return this.http.get(`${environment.BACKEND_PATH}/question/questionBank`,
		{headers:new HttpHeaders({"Authorization":`Bearer ${this.token}`})}).pipe(
			catchError(this.handleError<any>("load question"))
		)
	}

	submit(time:Time[]):Observable<any>{
		return this.http.post(`${environment.BACKEND_PATH}/question/answer`,JSON.stringify({time}),
		this.postOptions).pipe(
			catchError(this.handleError<any>("submit record"))
		)
	}

	private handleError<T>(operation = "operation",result?:T){
		return (error:any):Observable<T> => {
			console.error(`${operation}:${error}`);
			return of(result as T);
		}
	}
}
