import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ComponentFixture,TestBed} from '@angular/core/testing';
import {FormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {RouterTestingModule} from "@angular/router/testing";
import {of} from "rxjs";
import {BibleService} from "../bible.service";
import {BottomBarComponent} from "../bottom-bar/bottom-bar.component";
import {UserService} from "../user.service";
import {NoticeComponent} from './notice.component';

describe("NoticeComponent",() => {
	let component:NoticeComponent;
	let fixture:ComponentFixture<NoticeComponent>;
	let userService:UserService;
	let bibleService:BibleService;

	beforeEach(async() => {
		await TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				RouterTestingModule,
				FormsModule,
				MatIconModule
			],
			declarations:[
				NoticeComponent,
				BottomBarComponent
			]
		}).compileComponents();
	})

	beforeEach(() => {
		fixture = TestBed.createComponent(NoticeComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
		userService = TestBed.inject(UserService);
		bibleService = TestBed.inject(BibleService);
	})

	it("should create",() => {
		expect(component).toBeTruthy();
	})

	it("should get current admin user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy",data:{is_admin:true}}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.isAdmin).toBe(true);
	})

	it("cannot get current admin user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.isAdmin).toBe(false);
	})

	it("should get bible",() => {
		spyOn(bibleService,"loadScripture").and.returnValue(of({success:true,data:{id:1,bible_id:1,content:"timo"}}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.goldenSentence).toEqual({id:1,bible_id:1,content:"timo"});
	})

	it("cannot get bible",() => {
		spyOn(bibleService,"loadScripture").and.returnValue(of({success:false,data:{id:1,bible_id:1,content:"timo"}}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.goldenSentence).toEqual({id:0,bible_id:0,content:""});
	})

	it("should show password input",() => {
		component.passwordInput = false;
		component.resetPasswordInput = true;
		component.showPasswordInput();
		expect(component.passwordInput).toBe(true);
		expect(component.resetPasswordInput).toBe(false);
	})

	it("should show reset password input",() => {
		component.passwordInput = true;
		component.resetPasswordInput = false;
		component.showResetPasswordInput();
		expect(component.passwordInput).toBe(false);
		expect(component.resetPasswordInput).toBe(true);
	})

	it("should change password",() => {
		component.password = "abc";
		component.passwordAgain = "abc";
		spyOn(userService,"changePassword").and.returnValue(of({success:true}));
		component.changePassword("abc","abc");
		expect(component.password).toEqual("");
		expect(component.passwordAgain).toEqual("");
	})

	it("cannot change password when password is different to passwordAgain",() => {
		component.password = "abc";
		component.passwordAgain = "abcd";
		spyOn(userService,"changePassword").and.returnValue(of({success:true}));
		component.changePassword("abc","abcd");
		expect(component.password).toEqual("abc");
		expect(component.passwordAgain).toEqual("abcd");
	})

	it("cannot change password when result is not successful",() => {
		component.password = "abc";
		component.passwordAgain = "abc";
		spyOn(userService,"changePassword").and.returnValue(of({success:false}));
		component.changePassword("abc","abc");
		expect(component.password).toEqual("abc");
		expect(component.passwordAgain).toEqual("abc");
	})

	it("should type confirm",() => {
		component.confirmAgain = false;
		component.typeConfirmAgain();
		expect(component.confirmAgain).toBe(true);
	})

	it("should change default password",() => {
		component.confirmText = "Confirm";
		component.confirmAgain = true;
		component.resetPasswordInput = true;
		spyOn(userService,"changeDefaultPassword").and.returnValue(of({success:true}));
		component.changeDefaultPassword("Confirm","abc");
		expect(component.confirmText).toEqual("");
		expect(component.confirmAgain).toBe(false);
		expect(component.resetPasswordInput).toBe(false);
	})

	it("cannot change default password when confirm is not typed in",() => {
		component.confirmText = "Confirm";
		component.confirmAgain = true;
		component.resetPasswordInput = true;
		spyOn(userService,"changeDefaultPassword").and.returnValue(of({success:true}));
		component.changeDefaultPassword("","abc");
		expect(component.confirmText).toEqual("Confirm");
		expect(component.confirmAgain).toBe(true);
		expect(component.resetPasswordInput).toBe(true);
	})

	it("cannot change default password when result is not successful",() => {
		component.confirmText = "Confirm";
		component.confirmAgain = true;
		component.resetPasswordInput = true;
		spyOn(userService,"changeDefaultPassword").and.returnValue(of({success:false}));
		component.changeDefaultPassword("Confirm","abc");
		expect(component.confirmText).toEqual("Confirm");
		expect(component.confirmAgain).toBe(true);
		expect(component.resetPasswordInput).toBe(true);
	})
})