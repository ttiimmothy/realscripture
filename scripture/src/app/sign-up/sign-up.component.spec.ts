import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Router} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {of} from "rxjs";
import {UserService} from "../user.service";
import {SignUpComponent} from './sign-up.component';

describe('SignUpComponent',() => {
    let component:SignUpComponent;
    let fixture:ComponentFixture<SignUpComponent>;
	let userService:UserService;
	let router:Router;

    beforeEach(async() => {
        await TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				RouterTestingModule
			],
            declarations:[SignUpComponent],
        }).compileComponents();
    })

    beforeEach(() => {
        fixture = TestBed.createComponent(SignUpComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
		userService = TestBed.inject(UserService);
		router = TestBed.inject(Router);
    })

    it("should create",() => {
        expect(component).toBeTruthy();
    })

	it("should preview image",() => {
		let image = new Blob();
		component.readURL({
			currentTarget:{
				files:[image]
			}
		})
		expect(component.newImageIcon).toEqual(image);
	})

	it("cannot preview image",() => {
		component.readURL({
			currentTarget:{
				files:[]
			}
		})
		expect(component.newImageIcon).toEqual(undefined);
	})

	it("should register",fakeAsync(() => {
		spyOn(userService,"signUp").and.returnValue(of({success:true}));
		let routerNew = spyOn(router,"navigate");
		component.signUp(null,"","");
		tick(1000);
		expect(routerNew).toHaveBeenCalledWith(["/chat"]);
	}))

	it("cannot register",fakeAsync(() => {
		spyOn(userService,"signUp").and.returnValue(of({success:false}));
		let routerNew = spyOn(router,"navigate");
		component.signUp(null,"","");
		tick(1000);
		expect(routerNew).not.toHaveBeenCalled();
	}))
})