import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("group_message",(table) => {
		table.increments();
		table.integer("send_user_id").unsigned();
		table.foreign("send_user_id").references("users.id");
		table.integer("chatroom_id").unsigned();
		table.foreign("chatroom_id").references("chatroom.id");
		table.text("content");
		table.string("type").defaultTo("text").notNullable();
		table.timestamps(false,true);
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("group_message");
}