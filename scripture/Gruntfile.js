module.exports = function(grunt){
    // Project configuration
    grunt.initConfig({
        pkg:grunt.file.readJSON("package.json"),
        uglify:{
            options:{
                banner:'/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
            },
            build:{
                src:"src/<%= pkg.name %>.js",
                dest:"build/<%= pkg.name %>.min.js",
            },
        },
		concat:{
			options:{
			  	separator:'\n/*next file*/\n\n'  // this will be put between conc files
			},
			dist:{
				src:["scripts/hello.js","scripts/main.js"],
				dest:"scripts/built.js"
			}
		},
		strip_code:{
			options:{
				start_comment:"test-code",
				end_comment:"end-test-code"
			},
			your_target:{
				// a list of files you want to strip code from
				src:"src/*.ts"
			}
		},
		meta:{
			src: {
				main:"src/app",
				test:"src/app"
			},
			bin: {
				coverage:"bin/coverage"
			}
		},
		jasmine:{
			coverage:{
				src:["src/*spec.ts"],
				options:{
					specs:["src/*spec.ts"],
					template:require("grunt-template-jasmine-istanbul"),
					templateOptions:{
						coverage:"<%= meta.bin.coverage %>/coverage.json",
						report:[
							{
								type:"html",
								options: {
									dir:"<%= meta.bin.coverage %>/html"
								}
							},
							{
								type:"cobertura",
								options: {
									dir:"<%= meta.bin.coverage %>/cobertura"
								}
							},
							{
								type:"text-summary"
							}
						],
						template:require("grunt-template-jasmine-requirejs"),
						templateOptions:{
							requireConfig:{
								baseUrl:".grunt/grunt-contrib-jasmine/<%= meta.src.main %>/js/"
							}
						},
					},
					thresholds:{
                        lines:75,
                        statements:75,
                        branches:75,
                        functions:90
                    }
				}
			}
		},
		jshint:{
			all:["Gruntfile.js"]
		}
    });

    // Load the plugin that provides the uglify task
    grunt.loadNpmTasks("grunt-contrib-uglify"); // Javascript minification
	grunt.loadNpmTasks("grunt-contrib-concat"); // Javascript concatenation
	grunt.loadNpmTasks("grunt-contrib-jasmine");
	grunt.loadNpmTasks("grunt-contrib-jshint");
	grunt.loadNpmTasks("grunt-strip-code");

    // Default task(s)
    grunt.registerTask("default",["uglify"]);
	grunt.registerTask("test",[
		"concat",
		"jshint",
		"jasmine"
	]);
	grunt.registerTask("deploy",[
		"concat",
		"strip-code",
		"jshint",
		"uglify"
	]);
};