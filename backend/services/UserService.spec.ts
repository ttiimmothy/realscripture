import {UserService} from "./UserService";
import Knex from "knex";
import {hashPassword} from "../hash";
import {logger} from "../logger";
const knexfile = require("../knexfile");
const knex = Knex(knexfile["testing"]);

describe("UserService",() => {
	let userService:UserService;
	let adminId:any;
	let hashedPassword:string

	beforeEach(async() => {
		hashedPassword = await hashPassword("ttiimmothhy");
		await knex.insert({username:"timothy",phone:11122233,icon:"timo",qr_code:"",password:hashedPassword}).into("users");
		adminId = (await knex.insert({username:"admin",phone:11122333,icon:"timo",qr_code:"",is_admin:true,
		password:hashedPassword}).into("users").returning("id"))[0];
		await knex.insert({password:hashedPassword}).into("default_password");
		userService = new UserService(knex);
		logger.error = jest.fn();
	})

	it("should login",async() => {
		let users = await userService.login(11122233);
		expect(users).toMatchObject([{
			username:"timothy",
			phone:11122233,
			icon:"timo",
			qr_code:""
		}])
	})

	it("should login as admin",async() => {
		let users = await userService.adminLogin(11122233);
		expect(users).toMatchObject([])
	})

	it("should get admin password",async() => {
		let password = await userService.getUserPassword(adminId);
		expect(password).toMatchObject([{
			id:adminId,
			password:hashedPassword
		}])
	})

	it("should register with icon",async() => {
		await knex.insert({room_name:"announcement"}).into("chatroom");
		await userService.signUp("timo",33222111,"daniel");
		let users = await knex.select("*").from("users").where("username","timo");
		expect(users).toMatchObject([{
			username:"timo",
			phone:33222111,
			icon:"daniel",
			is_admin:false,
			qr_code:""
		}])
		await knex("user_chatroom").del();
		await knex("chatroom").del();
	})

	it("should register without icon",async() => {
		await knex.insert({room_name:"announcement"}).into("chatroom");
		await userService.signUp("timo",33222111);
		let users = await knex.select("*").from("users").where("username","timo");
		expect(users).toMatchObject([{
			username:"timo",
			phone:33222111,
			icon:"unknown.png",
			is_admin:false,
			qr_code:""
		}])
		await knex("user_chatroom").del();
		await knex("chatroom").del();
	})

	it("should print error and rollback in transaction when signing up get error",async() => {
		try{
			await userService.signUp(undefined as any as string,"" as any as number);
		}catch(e){
			expect(logger.error).toBeCalledTimes(1);
			expect(logger.error).toBeCalledWith("knex transaction rollback")
		}
	})

	afterEach(async() => {
		await knex("users").del();
		await knex("default_password").del();
	})

	afterAll(async() => {
		await knex.destroy();
	})
})