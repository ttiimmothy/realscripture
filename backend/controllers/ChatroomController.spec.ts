import {Server as SocketIO} from "socket.io";
import {Request, Response} from "express";
import {Knex} from "knex";
import {ChatroomService} from "../services/ChatroomService";
import {ChatroomController} from "./ChatroomController";
import {logger} from "../logger";

export interface RequestWithTestUser extends Request{
	user:{
		id:number;
		username:string;
		phone:number;
		icon:string;
		is_admin:boolean;
		admin_password:string;
		qr_code:string;
		created_at:Date;
		updated_at:Date;
	}
}

export interface User{
	id:number;
	username:string;
}

describe("ChatroomController",() => {
	let chatroomController:ChatroomController;
	let chatroomService:ChatroomService;
	let req:Request;
	let res:Response;
	let io:SocketIO;

	beforeEach(() => {
		chatroomService = new ChatroomService({} as Knex);
		jest.spyOn(chatroomService,"loadChatroomList").mockImplementation((userId:number|undefined) =>
		Promise.resolve([{id:1,user_id:1,chatroom_id:1,room_name:"timothy",room_icon:"timothy.png",created_at:
		new Date("2021-07-30"),updated_at:new Date("2021-07-30")}]))

		jest.spyOn(chatroomService,"findChatroomInformation").mockImplementation((id:number) => Promise.resolve([{id:1,
		room_name:"timothy",room_icon:"timothy.png"}]));

		jest.spyOn(chatroomService,"loadChatMessage").mockImplementation((roomId:number) => Promise.resolve
		([{id:1,username:"timothy",icon:"timothy.png",send_user_id:1,chatroom_id:1,content:"hello",type:"text",
		created_at:new Date("2021-07-30"),updated_at:new Date("2021-07-30")}]));

		jest.spyOn(chatroomService,"sendMessage").mockImplementation((message:string,roomId:number,userId:number) =>
		Promise.resolve([{id:1,username:"timothy",
		icon:"timothy.png",send_user_id:1,chatroom_id:1,content:"hello",type:"text",created_at:new Date("2021-07-30"),
		updated_at:new Date("2021-07-30")}]));

		jest.spyOn(chatroomService,"sendPhotoMessage").mockImplementation((message:string,roomId:number,userId:number,
		size:number|undefined) => Promise.resolve([{id:1,username:"timothy",icon:"timothy.png",send_user_id:1,chatroom_id:1,
		content:"hello",type:"image",created_at:new Date("2021-07-30"),updated_at:new Date("2021-07-30")}]));

		jest.spyOn(chatroomService,"sendVoiceMessage").mockImplementation((message:string,roomId:number,userId:number,
		size:number|undefined) => Promise.resolve([{id:1,username:"timothy", icon:"timothy.png",send_user_id:1,chatroom_id:1,
		content:"hello",type:"sound",created_at:new Date("2021-07-30"),updated_at:new Date("2021-07-30")}]));

		jest.spyOn(chatroomService,"searchPerson").mockImplementation((username:string,existUser:string[],
		currentUserId:number|undefined) => Promise.resolve([{id:1,username:"timothy",icon:"daniel.png"}]));

		jest.spyOn(chatroomService,"searchNewPerson").mockImplementation((username:string,existUser:string[],
		alreadyInGroupUser:string[]) => Promise.resolve([{id:1,username:"timothy",icon:"daniel.png"}]));

		jest.spyOn(chatroomService,"getChatroomUser").mockImplementation((chatroomId:number) => Promise.resolve([{id:1,
		username:"timothy",icon:"daniel.png",}]));

		jest.spyOn(chatroomService,"createChatroom").mockImplementation((name:string,icon:string,usernames:string[],
		adminId:number|undefined) => Promise.resolve());

		jest.spyOn(chatroomService,"addUserToChatroom").mockImplementation((users:string[],roomId:number) =>
		Promise.resolve());

		io = {
			emit:jest.fn(() => null),
			to:jest.fn(() => io),
			join:jest.fn(() => null)
		} as any as SocketIO;

		chatroomController = new ChatroomController(chatroomService,io);

		req = {
			body:{},
			params:{},
			query:{},
			user:{
				id:2,
				username:"timo",
				phone:12345678,
				icon:"",
				is_admin:false,
				admin_password:"",
				qr_code:"",
				created_at:new Date("2021-07-30"),
				updated_at:new Date("2021-07-30")
			}
		} as any as Request;

		res = {
			json:jest.fn((obj:any) => null),
			status:jest.fn((statusCode:number) => res)
		} as any as Response;

		logger.error = jest.fn()
	})

	it("should get chatroom list",async() => {
		await chatroomController.loadChatroomList(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:[{id:1,user_id:1,chatroom_id:1,room_name:"timothy",
		room_icon:"timothy.png",created_at:new Date("2021-07-30"),updated_at:new Date("2021-07-30")}]});
	})

	it("should get chatroom information",async() => {
		req.params.id = "1";
		await chatroomController.findChatroomInformation(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:{id:1,room_name:"timothy",room_icon:"timothy.png"}});
	})

	it("should get all chatroom messages",async() => {
		req.params.id = "1";
		await chatroomController.loadChatMessage(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:[{id:1,username:"timothy",icon:"timothy.png",send_user_id:1,
		chatroom_id:1,content:"hello",type:"text",created_at:new Date("2021-07-30"),updated_at:new Date("2021-07-30")}]})
	})

	it("should able to send message",async() => {
		req.body = {chatroomId:1};
		await chatroomController.sendMessage(req,res);
		expect(io.emit).toBeCalledTimes(1);
		expect(io.emit).toBeCalledWith("getMessage",{id:1,username:"timothy",
		icon:"timothy.png",send_user_id:1,chatroom_id:1,content:"hello",type:"text",created_at:new Date("2021-07-30"),
		updated_at:new Date("2021-07-30")});
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true});
	})

	it("should able to send image message",async() => {
		req.body = {chatroomId:1};
		req.file = {
			filename:"hello"
		} as any
		await chatroomController.sendPhotoMessage(req,res);
		expect(io.emit).toBeCalledTimes(1);
		expect(io.emit).toBeCalledWith("getMessage",{id:1,username:"timothy",
		icon:"timothy.png",send_user_id:1,chatroom_id:1,content:"hello",type:"image",created_at:new Date("2021-07-30"),
		updated_at:new Date("2021-07-30")});
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true});
	})

	it("should able to send sound message",async() => {
		req.body = {chatroomId:1};
		req.file = {
			filename:"hello"
		} as any
		await chatroomController.sendVoiceMessage(req,res);
		expect(io.emit).toBeCalledTimes(1);
		expect(io.emit).toBeCalledWith("getMessage",{id:1,username:"timothy",
		icon:"timothy.png",send_user_id:1,chatroom_id:1,content:"hello",type:"sound",created_at:new Date("2021-07-30"),
		updated_at:new Date("2021-07-30")});
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true});
	})

	it("should search people to add to new chatroom",async() => {
		await chatroomController.searchPerson(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:[{id:1,username:"timothy",icon:"daniel.png"}]});
	})

	it("should search person to add to existing chatroom",async() => {
		await chatroomController.searchNewPerson(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:[{id:1,username:"timothy",icon:"daniel.png"}]});;
	})

	it("should get chatroom users",async() => {
		req.params.id = "1";
		await chatroomController.getChatroomUser(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:[{id:1,username:"timothy",icon:"daniel.png",}]});
	})

	it("should create chatroom with icon",async() => {
		req.body = {name:"timo",icon:"",users:"[]"};
		req.file = {
			filename:"daniel.png"
		} as any;
		await chatroomController.createChatroom(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true});
	})

	it("should create chatroom without icon",async() => {
		req.body = {name:"timo",icon:"",users:"[]"};
		await chatroomController.createChatroom(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true});
	})

	it("should add users to chatroom",async() => {
		req.params.id = "1";
		await chatroomController.addUserToChatroom(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true});
	})

	it(`should get the same result in getting chatroom list for user when req.user is undefined or when req.user is
	defined`,async () => {
		req.user = undefined;
		await chatroomController.loadChatroomList(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:[{id:1,user_id:1,chatroom_id:1,room_name:"timothy",
		room_icon:"timothy.png",created_at:new Date("2021-07-30"),updated_at:new Date("2021-07-30")}]});
	})

	it(`should get the same result in searching people for creating new chatroom when req.user is undefined
	or when req.user is defined`,async () => {
		req.user = undefined;
		await chatroomController.searchPerson(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:[{id:1,username:"timothy",icon:"daniel.png"}]});
	})

	it(`should get the same result in creating chatroom when req.user is undefined
	or when req.user is defined`,async () => {
		req.body = {name:"timo", icon:"", users:"[]"};
		req.user = undefined;
		await chatroomController.createChatroom(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true});
	})

	it("should print error when getting bible script get error",async () => {
		req.path = "/chatroom/chatroom";
		req.method = "get";
		res = {} as any
		try{
			await chatroomController.loadChatroomList(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:get path:/chatroom/chatroom")
		}
	})

	it("should print error when finding chatroom information get error",async () => {
		req.path = "/chatroom/room/1";
		req.method = "get";
		res = {} as any
		try{
			await chatroomController.findChatroomInformation(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:get path:/chatroom/room/1")
		}
	})

	it("should print error when get chat messages of specific chatroom get error",async () => {
		req.path = "/chatroom/chatroomMessage/1";
		req.method = "get";
		res = {} as any
		try{
			await chatroomController.loadChatMessage(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:get path:/chatroom/chatroomMessage/1")
		}
	})

	it("should print error when send text message get error",async () => {
		req.path = "/chatroom/messages";
		req.method = "post";
		res = {} as any
		try{
			await chatroomController.sendMessage(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:post path:/chatroom/messages")
		}
	})

	it("should print error when send image message get error",async () => {
		req.path = "/chatroom/photoMessages";
		req.method = "post";
		res = {} as any
		try{
			await chatroomController.sendPhotoMessage(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:post path:/chatroom/photoMessages")
		}
	})

	it("should print error when send sound message get error",async () => {
		req.path = "/chatroom/voiceMessages";
		req.method = "post";
		res = {} as any
		try{
			await chatroomController.sendVoiceMessage(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:post path:/chatroom/voiceMessages")
		}
	})

	it("should print error when search people to add in new chatroom get error",async () => {
		req.path = "/chatroom/peopleAdd";
		req.method = "post";
		res = {} as any
		try{
			await chatroomController.searchPerson(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:post path:/chatroom/peopleAdd")
		}
	})

	it("should print error when search people to add in existing chatroom get error",async () => {
		req.path = "/chatroom/peopleNewSearch";
		req.method = "post";
		res = {} as any
		try{
			await chatroomController.searchNewPerson(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:post path:/chatroom/peopleNewSearch")
		}
	})

	it("should print error when get user list for specific chatroom get error",async () => {
		req.path = "/chatroom/users/1";
		req.method = "get";
		res = {} as any
		try{
			await chatroomController.getChatroomUser(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:get path:/chatroom/users/1")
		}
	})

	it("should print error when create chatroom get error",async () => {
		req.path = "/chatroom/newRoom";
		req.method = "post";
		res = {} as any;
		req.body = {name:"timo",icon:"",users:"[]"};
		try{
			await chatroomController.createChatroom(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:post path:/chatroom/newRoom")
		}
	})

	it("should print error when add people to existing chatroom get error",async () => {
		req.path = "/chatroom/newChatroomUsers/1";
		req.method = "post";
		res = {} as any
		try{
			await chatroomController.addUserToChatroom(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:post path:/chatroom/newChatroomUsers/1")
		}
	})
})