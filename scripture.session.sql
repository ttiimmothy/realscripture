/ Users / likinting / Desktop / scripturebible / realProject
SELECT *
from users -- change phone type to VARCHAR
ALTER TABLE users
ALTER COLUMN phone TYPE VARCHAR(50);
-- insert user data
COPY users(username, phone, icon)
from '/Users/likinting/Desktop/scripturebible/realProject/user_phone_image.csv' WITH (FORMAT CSV, HEADER TRUE, QUOTE '"');
-- COPY users(icon)
-- from '/Users/likinting/Desktop/scripturebible/realProject/icon.csv' WITH (FORMAT CSV, HEADER TRUE, QUOTE '"');
-- ALTER TABLE users
-- ALTER COLUMN phone TYPE INTEGER USING phone::integer;
-- ALTER TABLE assets
-- ALTER COLUMN asset_no TYPE INT USING asset_no::integer;
-- alter table users
-- alter column phone TYPE INTEGER USING (phone::integer) -- BULK
-- INSERT users(username, phone, admin_password, icon, is_admin)
-- FROM '/Users/likinting/Desktop/scripturebible/realProject/users.csv' WITH (format CSV, HEADER);
-- BULK
-- INSERT test
-- FROM 'C:\Test\test1.csv' WITH (
--     rowterminator = '\n',
--     fieldterminator = ','
--   ) LOAD DATA INFILE '/Users/likinting/Desktop/scripturebible/realProject/users_2.csv' INTO TABLE users COLUMNS TERMINATED BY ',' LINES TERMINATED BY '\n'
-- delete from users
-- where id = 1;
truncate TABLE users;