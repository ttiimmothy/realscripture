import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ComponentFixture,TestBed} from '@angular/core/testing';
import {Router} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {of} from "rxjs";
import {AccessGuard} from "../access.guard";
import {ChatroomComponent} from "../chatroom/chatroom.component";
import {UserService} from "../user.service";
import {LoginComponent} from "./login.component";

describe("LoginComponent",() => {
	let component:LoginComponent;
	let fixture:ComponentFixture<LoginComponent>;
	let userService:UserService;
	let router:Router;
	let accessGuard:AccessGuard;

	beforeEach(async() => {
		await TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				RouterTestingModule.withRoutes(
					[
						{path:"chat/1",component:ChatroomComponent},
						{path:"",component:LoginComponent}
					]
				)
			],
			declarations:[LoginComponent]
		}).compileComponents();
	})

	beforeEach(() => {
		fixture = TestBed.createComponent(LoginComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
		userService = TestBed.inject(UserService);
		router = TestBed.inject(Router);
		accessGuard = TestBed.inject(AccessGuard);
		localStorage.setItem("token","abc");
	})

	it("should create",() => {
		expect(component).toBeTruthy();
	})

	it("cannot get jwt token when localStorage token is empty",() => {
		localStorage.setItem("token","");
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy"}));
		let routerNew = spyOn(router,"navigate");
		component.ngOnInit();
		fixture.detectChanges();
		// expect(userService.token).toEqual("abc");
		expect(routerNew).not.toHaveBeenCalled();
	})

	it("should get jwt token",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy"}));
		let routerNew = spyOn(router,"navigate");
		component.ngOnInit();
		fixture.detectChanges();
		// expect(userService.token).toEqual("abc");
		expect(routerNew).toHaveBeenCalledWith(["/chat/1"]);
	})

	it("cannot get jwt token",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:false}));
		let routerNew = spyOn(router,"navigate");
		component.ngOnInit();
		fixture.detectChanges();
		// expect(userService.token).toEqual(null);
		expect(routerNew).toHaveBeenCalledWith(["/"]);
	})

	it("should login",() => {
		spyOn(userService,"login").and.returnValue(of({success:true,data:"cde"}));
		let routerNew = spyOn(router,"navigate");
		component.login("","");
		expect(routerNew).toHaveBeenCalledWith(["/chat/1"]);
		expect(accessGuard.user).toEqual("");
		expect(localStorage.getItem("token")).toEqual("cde");
	})

	it("cannot login",() => {
		spyOn(userService,"login").and.returnValue(of({success:false}));
		let routerNew = spyOn(router,"navigate");
		component.ngOnInit();
		fixture.detectChanges();
		component.login("","");
		expect(routerNew).not.toHaveBeenCalled();
		expect(localStorage.getItem("token")).toEqual("abc");
	})
})