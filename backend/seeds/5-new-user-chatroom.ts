import {Knex} from "knex";

export async function seed(knex:Knex):Promise<void>{
	// Deletes ALL existing entries
	let userId = (await knex.select("*").from("users").where("username","timothy"))[0].id;
	let timoUserId = (await knex.select("*").from("users").where("username","timo"))[0].id;
	let adminId = (await knex.select("*").from("users").where("username","ttiimmothhy"))[0].id;
	// console.log(userId)
	await knex("user_chatroom").where("user_id",userId).del();
	let announcementChatroomId = (await knex.select("id").from("chatroom").where("room_name","announcement"))[0].id;
	let chatroomId = (await knex.select("id").from("chatroom").where("room_name","daniel"))[0].id;
	let testChatroomId = (await knex.select("id").from("chatroom").where("room_name","test"))[0].id;
	// Inserts seed entries
	await knex("user_chatroom").insert([
		{user_id:adminId,chatroom_id:announcementChatroomId,is_admin:true},
		{user_id:userId,chatroom_id:announcementChatroomId},
		{user_id:timoUserId,chatroom_id:announcementChatroomId},
		{user_id:userId,chatroom_id:chatroomId,is_admin:true},
		{user_id:userId,chatroom_id:testChatroomId,is_admin:true},
		{user_id:timoUserId,chatroom_id:chatroomId},
	])
}