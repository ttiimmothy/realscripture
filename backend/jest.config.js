module.exports = {
    preset:"ts-jest",
    testEnvironment:"node",
	setupFiles: ["<rootDir>/test/setup-tests.ts"],
  	reporters:["default","jest-junit"]
}