import {Knex} from "knex";

export async function seed(knex:Knex):Promise<void>{
	// Deletes ALL existing entries
	await knex("question").del();

	// Inserts seed entries
	await knex("question").insert([
		{content:"How are you?",remarks:""},
		{content:"How old are you?",remarks:""},
		{content:"What are you doing?",remarks:""},
		{content:"Who are you?",remarks:""},
		{content:"What are you?",remarks:""},
		{content:"Why are you here?",remarks:""},
	])
}
