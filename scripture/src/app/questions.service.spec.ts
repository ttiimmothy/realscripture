import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from '@angular/core/testing';
import {QuestionsService} from './questions.service';

describe('QuestionsService',() => {
	let service:QuestionsService;
	let httpTestingController:HttpTestingController;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports:[HttpClientTestingModule]
		})
		service = TestBed.inject(QuestionsService);
		httpTestingController = TestBed.inject(HttpTestingController)
	})

	it('should be created',() => {
		expect(service).toBeTruthy();
	})

	it("should submit answer",() => {
		let expected = "";
		service.submit([{time:"",remark:""}]).subscribe((result) => {
			expect(result).toEqual(expected);
		})
		let testService = httpTestingController.expectOne("http://localhost:8160/question/answer");
		testService.flush(expected);
	})
})
