import {HttpClient,HttpHeaders} from "@angular/common/http";
import {Injectable} from '@angular/core';
import {Observable, of} from "rxjs";
import {catchError} from "rxjs/operators";

@Injectable({
	providedIn:"root"
})
export class ImageService{
	constructor(private http:HttpClient){}
	token = localStorage.getItem("token");
	// authOptions = {
	// 	headers:new HttpHeaders({"Authorization":`Bearer ${this.token}`})
	// }
	getImage(imageUrl:string):Observable<Blob>{
		// console.log(imageUrl);
		return this.http.get(imageUrl,{headers:new HttpHeaders({"Authorization":`Bearer ${this.token}`})
		,responseType:"blob"}).pipe(
			catchError(this.handleError<any>("load image"))
		)
	}
	getImageWithoutHeaders(imageUrl:any):Observable<Blob>{
		// console.log(imageUrl);
		return this.http.get(imageUrl,{responseType:"blob"}).pipe(
			catchError(this.handleError<any>("load image without authorization key"))
		)
	}
	private handleError<T>(operation = "operation",result?:T){
		return(error:any):Observable<T> => {
			console.error(`${operation}:${error}`);
			return of(result as T);
		}
	}
}
