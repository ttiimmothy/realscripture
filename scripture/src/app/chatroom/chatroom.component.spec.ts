import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ComponentFixture,TestBed} from "@angular/core/testing";
import {MatIconModule} from "@angular/material/icon";
import {RouterTestingModule} from "@angular/router/testing";
import {SocketIoModule} from "ngx-socket-io";
import {of,throwError} from "rxjs";
import {config} from "../app.module";
import {BibleService} from "../bible.service";
import {BottomBarComponent} from "../bottom-bar/bottom-bar.component";
import {ChatroomService} from "../chatroom.service";
import {ImageService} from "../image.service";
import {UserService} from "../user.service";
import {ChatroomComponent} from "./chatroom.component";

describe("ChatroomComponent",() => {
	let component:ChatroomComponent;
	let fixture:ComponentFixture<ChatroomComponent>;
	let userService:UserService;
	let bibleService:BibleService;
	let chatroomService:ChatroomService;
	let imageService:ImageService;

	beforeEach(async() => {
		await TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				RouterTestingModule,
				SocketIoModule.forRoot(config),
				MatIconModule
			],
			declarations:[
				ChatroomComponent,
				BottomBarComponent
			]
		}).compileComponents();
	})

	beforeEach(() => {
		fixture = TestBed.createComponent(ChatroomComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
		userService = TestBed.inject(UserService);
		bibleService = TestBed.inject(BibleService);
		chatroomService = TestBed.inject(ChatroomService);
		imageService = TestBed.inject(ImageService);
		localStorage.setItem("token","abc");
	})

	it("should create",() => {
		expect(component).toBeTruthy();
	})

	it("cannot get current user when localStorage token is empty",() => {
		localStorage.setItem("token","");
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy",data:{
			id:1,
			username:"timothy",
			phone:12345678,
			icon:"ronald.png",
			qr_code:"timo",
			is_admin:true
		}}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual({
			id:0,
			username:"",
			phone:0,
			icon:"",
			qr_code:"",
			is_admin:null
		})
	})

	it("should get current user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy",data:{
			id:1,
			username:"timothy",
			phone:12345678,
			icon:"ronald.png",
			qr_code:"timo",
			is_admin:true
		}}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual({
			id:1,
			username:"timothy",
			phone:12345678,
			icon:"ronald.png",
			qr_code:"timo",
			is_admin:true
		})
	})

	it("cannot get current user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:false}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual({
			id:0,
			username:"",
			phone:0,
			icon:"",
			qr_code:"",
			is_admin:null
		})
	})

	it("should get bible",() => {
		spyOn(bibleService,"loadScripture").and.returnValue(of({success:true,data:{id:1,bible_id:1,content:"timo"}}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.goldenSentence).toEqual({id:1,bible_id:1,content:"timo"});
	})

	it("cannot get bible",() => {
		spyOn(bibleService,"loadScripture").and.returnValue(of({success:false,data:{id:1,bible_id:1,content:"timo"}}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.goldenSentence).toEqual({id:0,bible_id:0,content:""});
	})

	it("should get all chat rooms",() => {
		spyOn(chatroomService,"loadChatroom").and.returnValue(of({success:true,data:[{
			id:1,
			user_id:1,
			chatroom_id:1,
			room_name:"timothy",
			room_icon:"ronald.png",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}]}))
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatroomList).toEqual([{
			id:1,
			user_id:1,
			chatroom_id:1,
			room_name:"timothy",
			room_icon:"ronald.png",
			created_at:new Date("2021-08-24"),
			updated_at:new Date("2021-08-24")
		}])
	})

	it("cannot get all chat rooms",() => {
		spyOn(chatroomService,"loadChatroom").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.chatroomList).toEqual([]);
	})


	it("cannot get image url from blob",() => {
		component.createImageFromBlob("" as any as Blob,1);
		expect(component.imageToShow).toEqual([]);
	})

	it("should get image from service",() => {
		spyOn(imageService,"getImageWithoutHeaders").and.returnValue(of(new Blob));
		component.getImageFromService("abc",1);
		expect(imageService.getImageWithoutHeaders).toHaveBeenCalledWith("abc");
	})

	it("should get image from service to catch error",() => {
		const mock = spyOn(imageService,"getImageWithoutHeaders").and.returnValue(throwError({status:404}));
		component.getImageFromService("abc",1);
		expect(mock).toHaveBeenCalledWith("abc");
	})
})