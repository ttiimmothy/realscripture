import {Request, Response} from "express";
import fetch from "node-fetch";
import {logger} from "../logger";
import {BibleService} from "../services/BibleService";

export class BibleController{
	constructor(private bibleService:BibleService){}

	getBibleScripts = async(req:Request,res:Response) => {
		try{
			let today = new Date().getDate();
			let scripture = await this.bibleService.getBibleScripts(today);
			if(scripture.length === 0){
				const request = await fetch("https://api.scripture.api.bible/v1/bibles",{
					headers:{
						// "api-key":"f153df929945bb80fab2c1e94d75ee52"
						"api-key":process.env.BIBLE_AUTHORIZATION_KEY!
					}
				})
				const result = await request.json();
				let newBible = await this.bibleService.newScripture(today,result.data[today - 1].name);
				res.status(200).json({success:true,data:newBible[0]});
			}else{
				res.status(200).json({success:true,data:scripture[0]});
			}
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	loadScriptureDetail = async(req:Request,res:Response) => {
		try{
			let bibleIds = await this.bibleService.loadScriptureDetailId();
			const request = await fetch("https://api.scripture.api.bible/v1/bibles",{
				headers:{
					// "api-key":"f153df929945bb80fab2c1e94d75ee52"
					"api-key":process.env.BIBLE_AUTHORIZATION_KEY!
				}
			})
			const result = await request.json();
			for(let i = 0; i < 31; i++){
				let isExist = false;
				for(let bible of bibleIds){
					// console.log(bible.bible_id)
					if(bible.bible_id === i){
						isExist = true;
						break;
					}
				}
				if(!isExist){
					// console.log(result)
					await this.bibleService.insertNewScripture(i,result.data[i].name);
				}
			}
			let bibleDetails = await this.bibleService.loadScriptureDetail();
			res.status(200).json({success:true,data:bibleDetails});

		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}
}