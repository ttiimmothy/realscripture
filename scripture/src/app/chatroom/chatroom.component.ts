import {Component,OnInit} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";
import {environment} from "src/environments/environment";
// import {AccessGuard} from "../access.guard";
import {BibleService} from "../bible.service";
import {User} from "../chatroom-detail/chatMessage";
import {ChatroomService} from "../chatroom.service";
import {ImageService} from "../image.service";
import {UserService} from "../user.service";
import {Chatroom, Sentence} from "./chatroom";

@Component({
	selector:'app-chatroom',
	templateUrl:'./chatroom.component.html',
	styleUrls:['./chatroom.component.scss']
})
export class ChatroomComponent implements OnInit{
	constructor(private userService:UserService,private chatroomService:ChatroomService,private imageService:ImageService,
	private bibleService:BibleService,private sanitizer:DomSanitizer){}
	ngOnInit():void{
		this.chatroomService.token = localStorage.getItem("token");
		if(localStorage.getItem("token")){
			this.userService.checkCurrentUsers().subscribe((result) => {
				// console.log(result)
				if(result && result.success){
					this.user = result.data;
				}
				// console.log(this.userService.user)
			})
		// this.user = this.userService.user
		}
		this.bibleService.loadScripture().subscribe((result) => {
			if(result.success){
				this.goldenSentence = result.data;
			}
		})
		this.chatroomService.loadChatroom().subscribe((result) => {
			// console.log(result.data);
			if(result.success){
				this.chatroomList = result.data;
				for(let chatroom of this.chatroomList){
					// console.log(`${this.imagePath}/${chatroom.room_icon}`);
					this.getImageFromService(`${this.imagePath}/${chatroom.room_icon}`,chatroom.chatroom_id);
				}
				// console.log(this.isImageLoading);
			}
		})
	}
	user:User = {
		id:0,
		username:"",
		phone:0,
		icon:"",
		qr_code:"",
		is_admin:null
	}
	goldenSentence:Sentence = {
		id:0,
		bible_id:0,
		content:""
	}
	chatroomList:Chatroom[] = [];
	imageToShow:(string|ArrayBuffer|null|any)[] = [];
	isImageLoading:boolean[] = [];

	createImageFromBlob(image:Blob,index:number){
		let reader = new FileReader();
		reader.addEventListener("load",() => {
			this.imageToShow[index] = this.sanitizer.bypassSecurityTrustResourceUrl(reader.result as string);
			// console.log(reader.result);
		},false);
		if(image){
			reader.readAsDataURL(image);
		}
	}
	getImageFromService(imageUrl:string,index:number){
		this.isImageLoading[index] = true;
		// this.imageService.getImage(imageUrl).subscribe((data) => {
		this.imageService.getImageWithoutHeaders(imageUrl).subscribe((data) => {
			// console.log(data);
			this.createImageFromBlob(data,index);
			this.isImageLoading[index] = false;
		},(error) => {
			this.isImageLoading[index] = false;
			// console.log(error);
		})
  	}

	// imagePath = environment.BACKEND_PATH;
	imagePath = environment.IMAGE_PATH;
}