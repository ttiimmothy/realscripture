import {HttpClientTestingModule,HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from '@angular/core/testing';
import {SocketIoModule} from "ngx-socket-io";
import {config} from "./app.module";
import {ChatroomService} from './chatroom.service';

describe('ChatroomService',() => {
	let service:ChatroomService;
	let httpTestingController:HttpTestingController;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				SocketIoModule.forRoot(config)
			]
		})
		httpTestingController = TestBed.inject(HttpTestingController);
		service = TestBed.inject(ChatroomService);
	})

	it('should be created',() => {
		expect(service).toBeTruthy();
	})

	it("should send message",() => {
		let expected = "";
		service.sendMessage("",0,0).subscribe((result) => {
			expect(result).toEqual(expected);
		})
		let testService = httpTestingController.expectOne("http://localhost:8160/chatroom/messages");
		testService.flush(expected);
	})

	it("should send image message",() => {
		let expected = "";
		service.sendPhotoMessage({} as File,0,0).subscribe((result) => {
			expect(result).toEqual(expected);
		})
		let testService = httpTestingController.expectOne("http://localhost:8160/chatroom/photoMessages");
		testService.flush(expected);
	})

	it("should send sound message",() => {
		let expected = "";
		service.sendVoiceMessage({} as File,0,0).subscribe((result) => {
			expect(result).toEqual(expected);
		})
		let testService = httpTestingController.expectOne("http://localhost:8160/chatroom/voiceMessages");
		testService.flush(expected);
	})

	it("should search person to add to a new chatroom",() => {
		let expected = "";
		service.searchPerson("",[]).subscribe((result) => {
			expect(result).toEqual(expected);
		})
		let testService = httpTestingController.expectOne("http://localhost:8160/chatroom/personAdd");
		testService.flush(expected);
	})

	it("should search new person to add to an existed chatroom",() => {
		let expected = "";
		service.searchNewPerson("",[],[]).subscribe((result) => {
			expect(result).toEqual(expected);
		})
		let testService = httpTestingController.expectOne("http://localhost:8160/chatroom/personNewSearch");
		testService.flush(expected);
	})

	it("should create new chatroom",() => {
		let expected = "";
		service.createRoom("",{} as File,[]).subscribe((result) => {
			expect(result).toEqual(expected);
		})
		let testService = httpTestingController.expectOne("http://localhost:8160/chatroom/newRoom");
		testService.flush(expected);
	})

	it("should add user to an existed chatroom",() => {
		let expected = "";
		service.addUserToChatroom([],1).subscribe((result) => {
			expect(result).toEqual(expected);
		})
		let testService = httpTestingController.expectOne("http://localhost:8160/chatroom/newChatroomUsers/1");
		testService.flush(expected);
	})
})