import {Knex} from "knex";
import {hashPassword} from "../hash";

export async function seed(knex:Knex):Promise<void>{
	await knex("users").where("username","timothy").del();
	// Inserts seed entries
	await knex("users").insert([
		{username:"timothy",phone:12345678,icon:"trial1.png",qr_code:"",password:await hashPassword("12345678")},
		{username:"timo",phone:99999999,icon:"trial2.png",qr_code:"",password:await hashPassword("12345678")},
		{username:"ttiimmothhy",phone:78901234,icon:"trial2.png",is_admin:true,qr_code:"",
		password:await hashPassword("ttiimmothhy")},
	])
}