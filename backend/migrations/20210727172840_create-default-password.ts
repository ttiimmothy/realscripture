import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("default_password",(table) => {
		table.increments();
		table.text("password");
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("default_password");
}