import {QuestionService} from "./QuestionService";
import Knex from "knex";
import {logger} from "../logger";
import {Time} from "./model";
const knexfile = require("../knexfile");
const knex = Knex(knexfile["testing"]);


describe("QuestionService",() => {
	let questionService:QuestionService;
	let userId:any;

	beforeEach(async() => {
		userId = (await knex.insert({username:"timothy",phone:11122233,icon:"",qr_code:""}).into("users")
		.returning("id"))[0]
		await knex.insert({content:"how are you",remarks:""}).into("question");
		questionService = new QuestionService(knex);

		logger.error = jest.fn();
	})

	it("should get all questions",async() => {
		let questions = await questionService.loadQuestion();
		expect(questions).toMatchObject([{
			content:"how are you",
			remarks:""
		}])
	})

	it("should submit answer",async() => {
		// console.log(userId)
		await knex.insert({room_name:"announcement"}).into("chatroom");
		await questionService.submitRecord([{time:"25",remark:"aws"}],userId,new Date("2021-07-31"));
		let taskId = (await knex.select("*").from("admin_task").where("user_id",userId))[0].id;
		let answer = await knex.select("content","remarks","task_id").from("answer").innerJoin("admin_task",
		"answer.user_id","admin_task.user_id");
		expect(answer).toMatchObject([{
			content:"25",
			remarks:"aws",
			task_id:taskId
		}])
		await knex("group_message").del();
		await knex("chatroom").del();
		await knex("answer").del();
		await knex("admin_task").del();
	})

	it("should submit nothing when req.user.id is undefined",async() => {
		await questionService.submitRecord([{time:"25",remark:"aws"}],undefined,new Date("2021-07-31"));
		// await knex.select("*").from("admin_task").where("user_id",userId)
		let answer = await knex.select("content","remarks","task_id").from("answer").innerJoin("admin_task",
		"answer.user_id","admin_task.user_id");
		expect(answer).toMatchObject([])
		await knex("answer").del();
		await knex("admin_task").del();
	})

	// it("should submit nothing when time array is empty",async() => {
	// 	await questionService.submitRecord([],["aws"],userId,new Date("2021-07-31"));
	// 	let answer = await knex.select("content","remarks","task_id").from("answer").innerJoin("admin_task",
	// 	"answer.user_id","admin_task.user_id");
	// 	expect(answer).toMatchObject([])
	// 	await knex("answer").del();
	// 	await knex("admin_task").del();
	// })

	it("should print error and rollback in transaction when submitting answer get error",async() => {
		try{
			await questionService.submitRecord(undefined as any as Time[],1,new Date("2021-08-02"));
		}catch(e){
			expect(logger.error).toBeCalledTimes(1);
			expect(logger.error).toBeCalledWith("knex transaction rollback")
		}
	})

	afterEach(async() => {
		await knex("question").del();
		await knex("users").del();
	})

	afterAll(async() => {
		await knex.destroy();
	})
})