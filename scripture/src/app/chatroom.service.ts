import {HttpClient,HttpHeaders} from "@angular/common/http";
import {Injectable} from '@angular/core';
import {Socket} from "ngx-socket-io";
import {Observable, of} from "rxjs";
import {catchError} from "rxjs/operators";
import {environment} from "src/environments/environment";
import {ChatMessage} from "./chatroom-detail/chatMessage";
// import {ChatroomDetailComponent} from "./chatroom-detail/chatroom-detail.component";

@Injectable({
	providedIn: 'root'
})
export class ChatroomService{
	constructor(private http:HttpClient,private socket:Socket){}
	token = localStorage.getItem("token");
	// authOptions = {
	// 	headers:new HttpHeaders({"Authorization":`Bearer ${this.token}`})
	// }
	postOptions = {
		headers:new HttpHeaders({"Authorization":`Bearer ${this.token}`,"Content-type":"application/json"})
	}
	loadChatroom():Observable<any>{
		// console.log(this.token);
		return this.http.get(`${environment.BACKEND_PATH}/chatroom/chatroom`,
		{headers:new HttpHeaders({"Authorization":`Bearer ${this.token}`})}).pipe(
			catchError(this.handleError("load chatroom"))
		)
	}
	findChatroomInformation(id:number):Observable<any>{
		// console.log(this.token);
		return this.http.get(`${environment.BACKEND_PATH}/chatroom/room/${id}`,
		{headers:new HttpHeaders({"Authorization":`Bearer ${this.token}`})}).pipe(
			catchError(this.handleError("find chatroom information"))
		)
	}
	loadChatMessage(chatroomId:number):Observable<any>{
		return this.http.get(`${environment.BACKEND_PATH}/chatroom/chatroomMessage/${chatroomId}`,
		{headers:new HttpHeaders({"Authorization":`Bearer ${this.token}`})}).pipe(
			catchError(this.handleError("load chat message"))
		)
	}
	getChatroomUser(chatroomId:number):Observable<any>{
		return this.http.get(`${environment.BACKEND_PATH}/chatroom/users/${chatroomId}`,
		{headers:new HttpHeaders({"Authorization":`Bearer ${this.token}`})}).pipe(
			catchError(this.handleError("get chatroom users"))
		)
	}
	sendMessage(message:string,chatroomId:number,userId:number):Observable<any>{
		// console.log(message);
		return this.http.post(`${environment.BACKEND_PATH}/chatroom/messages`,JSON.stringify({message,chatroomId,userId}),
		this.postOptions).pipe(
			catchError(this.handleError("send chat message"))
		)
	}
	sendPhotoMessage(file:File,chatroomId:number,userId:number):Observable<any>{
		// console.log(message);
		const formData = new FormData();
		formData.append("photo",file);
		formData.append("chatroomId",chatroomId + "");
		formData.append("userId",userId + "");
		return this.http.post(`${environment.BACKEND_PATH}/chatroom/photoMessages`,formData,
		{headers:new HttpHeaders({"authorization":`Bearer ${this.token}`})}).pipe(
			catchError(this.handleError("send photo message"))
		)
	}
	sendVoiceMessage(message:File,chatroomId:number,userId:number):Observable<any>{
		// console.log(message);
		const formData = new FormData();
		formData.append("message",message);
		formData.append("chatroomId",chatroomId + "");
		formData.append("userId",userId + "");
		return this.http.post(`${environment.BACKEND_PATH}/chatroom/voiceMessages`,formData,
		{headers:new HttpHeaders({"authorization":`Bearer ${this.token}`})}).pipe(
			catchError(this.handleError("send voice message"))
		)
	}
	searchPerson(name:string,alreadyExistName:string[]):Observable<any>{
		return this.http.post(`${environment.BACKEND_PATH}/chatroom/personAdd`,JSON.stringify({name,alreadyExistName}),
		this.postOptions).pipe(
			catchError(this.handleError("search person"))
		)
	}
	searchNewPerson(name:string,alreadyExistName:string[],alreadyInGroupName:string[]):Observable<any>{
		return this.http.post(`${environment.BACKEND_PATH}/chatroom/personNewSearch`,JSON.stringify({name,alreadyExistName,
		alreadyInGroupName}),this.postOptions).pipe(
			catchError(this.handleError("search new person"))
		)
	}
	createRoom(name:string,icon:File,users:string[]):Observable<any>{
		let form = new FormData();
		form.append("name",name);
		form.append("icon",icon);
		// console.log(icon);
		form.append("users",JSON.stringify(users));
		return this.http.post(`${environment.BACKEND_PATH}/chatroom/newRoom`,form,{headers:new HttpHeaders(
		{"Authorization":`Bearer ${this.token}`})}).pipe(
			catchError(this.handleError("create room"))
		)
	}
	addUserToChatroom(users:string[],id:number):Observable<any>{
		return this.http.post(`${environment.BACKEND_PATH}/chatroom/newChatroomUsers/${id}`,JSON.stringify({users}),
		this.postOptions).pipe(
			catchError(this.handleError("add new chatroom users"))
		)
	}

	handleError<T>(operation = "operations",result?:T){
		return(error:any):Observable<T> => {
			console.error(`${operation}:${error}`);
			return of(result as T);
		}
	}

	message(){
		return this.socket.fromEvent<ChatMessage>("getMessage");
	}
	// get messageSocket():Observable<ChatMessage>{
	// 	return this.message
	// }
	joinChatroom(chatroomId:number){
		this.socket.emit("joinRoom",chatroomId);
	}
	leaveRoom(chatroomId:number){
		this.socket.emit("leaveRoom",chatroomId);
	}
}
