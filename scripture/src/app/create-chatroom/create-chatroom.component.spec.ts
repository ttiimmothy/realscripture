import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ComponentFixture,fakeAsync,TestBed, tick} from '@angular/core/testing';
import {FormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {Router} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {SocketIoModule} from "ngx-socket-io";
import {of, throwError} from "rxjs";
import {config} from "../app.module";
import {BibleService} from "../bible.service";
import {BottomBarComponent} from "../bottom-bar/bottom-bar.component";
import {ChatroomService} from "../chatroom.service";
import {ImageService} from "../image.service";
import {UserService} from "../user.service";
import {CreateChatroomComponent} from './create-chatroom.component';

describe("CreateChatroomComponent",() => {
    let component:CreateChatroomComponent;
    let fixture:ComponentFixture<CreateChatroomComponent>;
	let userService:UserService;
	let bibleService:BibleService;
	let chatroomService:ChatroomService;
	let imageService:ImageService;
	let router:Router;

    beforeEach(async() => {
        await TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				RouterTestingModule,
				SocketIoModule.forRoot(config),
				MatIconModule,
				FormsModule
			],
            declarations:[
				CreateChatroomComponent,
				BottomBarComponent
			]
        }).compileComponents();
    })

    beforeEach(() => {
        fixture = TestBed.createComponent(CreateChatroomComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
		userService = TestBed.inject(UserService);
		bibleService = TestBed.inject(BibleService);
		chatroomService = TestBed.inject(ChatroomService);
		imageService = TestBed.inject(ImageService);
		router = TestBed.inject(Router);
		localStorage.setItem("token","abc");
    })

    it("should create",() => {
        expect(component).toBeTruthy();
    })

	it("cannot get current user when localStorage token is empty",() => {
		localStorage.setItem("token","");
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy"}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("");
	})

	it("should get current user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy"}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("timothy");
	})

	it("cannot get current user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("");
	})

	it("should get bible",() => {
		spyOn(bibleService,"loadScripture").and.returnValue(of({success:true,data:{id:1,bible_id:1,content:"timo"}}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.goldenSentence).toEqual({id:1,bible_id:1,content:"timo"});
	})

	it("cannot get bible",() => {
		spyOn(bibleService,"loadScripture").and.returnValue(of({success:false,data:{id:1,bible_id:1,content:"timo"}}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.goldenSentence).toEqual({id:0,bible_id:0,content:""});
	})

	it("should search available users",() => {
		spyOn(chatroomService,"searchPerson").and.returnValue(of({success:true,data:[{
			id:1,
			username:"timothy",
			icon:"ronald.png"
		}]}))
		component.search("timo",[
			{
				id:1,
				username:"timothy",
				icon:"ronald.png"
			}
		])
		expect(component.usernames).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png"
		}])
	})

	it("cannot search available users",() => {
		spyOn(chatroomService,"searchPerson").and.returnValue(of({success:false}))
		component.search("timo",[]);
		expect(component.usernames).toEqual([])
	})

	it("cannot search available users without username input",() => {
		spyOn(chatroomService,"searchPerson").and.returnValue(of({success:true,data:[{
			id:1,
			username:"timothy",
			icon:"ronald.png"
		}]}))
		component.search("",[]);
		expect(component.usernames).toEqual([])
	})

	it("should add an user",() => {
		component.addUser(1,"timothy","ronald.png");
		expect(component.choseUsernames).toEqual([{
			id:1,
			username:"timothy",
			icon:"ronald.png"
		}])
	})

	it("should delete an user",() => {
		component.choseUsernames = [{
			id:1,
			username:"timothy",
			icon:"ronald.png"
		}]
		component.deleteUser("timothy");
		expect(component.choseUsernames).toEqual([]);
	})

	it("cannot get image url from blob",() => {
		component.createImageFromBlob("" as any as Blob,1);
		expect(component.imageToShow).toEqual([]);
	})

	it("should get image from service",() => {
		spyOn(imageService,"getImageWithoutHeaders").and.returnValue(of(new Blob));
		component.getImageFromService("abc",1);
		expect(imageService.getImageWithoutHeaders).toHaveBeenCalledWith("abc");
	})

	it("should get image from service to catch error",() => {
		const mock = spyOn(imageService,"getImageWithoutHeaders").and.returnValue(throwError({status:404}));
		component.getImageFromService("abc",1);
		expect(mock).toHaveBeenCalledWith("abc");
	})

	it("should preview image",() => {
		let image = new Blob();
		component.readURL({
			currentTarget:{
				files:[image]
			}
		})
		expect(component.newImageIcon).toEqual(image);
	})

	it("cannot preview image",() => {
		component.readURL({
			currentTarget:{
				files:[]
			}
		})
		expect(component.newImageIcon).toEqual(undefined);
	})

	it("should create chat room",fakeAsync(() => {
		spyOn(chatroomService,"createRoom").and.returnValue(of({success:true}));
		let routerNew = spyOn(router,"navigate");
		component.createRoom("timo",new File([new Blob],"ronald.png"),[
			{
				id:1,
				username:"timothy",
				icon:"ronald.png"
			}
		])
		tick(1000);
		expect(routerNew).toHaveBeenCalledWith(["/chat"]);
	}))

	it("cannot create chat room",fakeAsync(() => {
		spyOn(chatroomService,"createRoom").and.returnValue(of({success:false}));
		let routerNew = spyOn(router,"navigate");
		component.createRoom("timo",new File([new Blob],"ronald.png"),[])
		tick(1000);
		expect(routerNew).not.toHaveBeenCalled();
	}))
})