import {Component,OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {AccessGuard} from "../access.guard";
// import {Store} from "@ngrx/store";
import {UserService} from "../user.service";

@Component({
	selector:"app-login",
	templateUrl:"./login.component.html",
	styleUrls:["./login.component.scss"]
})
export class LoginComponent implements OnInit{
	constructor(public userService:UserService,private router:Router,private accessGuard:AccessGuard){}
	ngOnInit():void{
		if(localStorage.getItem("token")){
			this.userService.checkCurrentUsers().subscribe((result) => {
				// console.log(result)
				if(result && result.success){
					this.router.navigate(["/chat/1"]);
					// this.userService.token = localStorage.getItem("token");
				}else{
					this.router.navigate(["/"]);
				}
			})
		}
	}
	// logo = require("../../../images/bible.png");
	login(phoneNumber:string,password:string){
		this.userService.login(phoneNumber,password).subscribe((result) => {
			// console.log(result)
			if(result.success){
				// this.userService.token = result.data;
				localStorage.setItem("token",result.data);
				this.router.navigate(["/chat/1"]);
				// this.userService.token = localStorage.getItem("token");
				this.accessGuard.user = "";
				// console.log(this.userService.token)
			}
		})
	}
}