import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {SoundService} from "./sound.service";

describe("SoundService", () => {
    let service:SoundService;
	let httpTestingController:HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
			imports:[HttpClientTestingModule]
		})
        service = TestBed.inject(SoundService);
		httpTestingController = TestBed.inject(HttpTestingController);
    })

    it("should be created",() => {
        expect(service).toBeTruthy();
    })

	it("should get sound url",()=>{
		let expected = new Blob;
		service.getSound("").subscribe((result) => {
			expect(result).toEqual(expected);
		})
		let testService = httpTestingController.expectOne("");
		testService.flush(expected);
	})
})
