import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {MatIconModule} from "@angular/material/icon";
import {RouterTestingModule} from "@angular/router/testing";
import {of} from "rxjs";
import {BibleService} from "../bible.service";
import {BottomBarComponent} from "../bottom-bar/bottom-bar.component";
import {UserService} from "../user.service";
import {BibleDetailComponent} from './bible-detail.component';

describe('BibleDetailComponent', () => {
    let component:BibleDetailComponent;
    let fixture:ComponentFixture<BibleDetailComponent>;
	let userService:UserService;
	let bibleService:BibleService;

    beforeEach(async() => {
        await TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				RouterTestingModule,
				MatIconModule
			],
            declarations:[
				BibleDetailComponent,
				BottomBarComponent
			]
        }).compileComponents();
    })

    beforeEach(() => {
        fixture = TestBed.createComponent(BibleDetailComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
		userService = TestBed.inject(UserService);
		bibleService = TestBed.inject(BibleService);
		localStorage.setItem("token","abc");
    })

    it("should create",() => {
        expect(component).toBeTruthy();
    })

	it("cannot get current user when localStorage token is empty",() => {
		localStorage.setItem("token","");
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy"}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("");
	})

	it("should get current user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy"}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("timothy");
	})

	it("cannot get current user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.user).toEqual("");
	})

	it("should get bible details",() => {
		spyOn(bibleService,"loadScriptureDetails").and.returnValue(of({success:true,data:[{id:1,bible_id:1,
		content:"timo"}]}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.bibleDetails).toEqual([{id:1,bible_id:1,content:"timo"}]);
	})

	it("cannot get bible details",() => {
		spyOn(bibleService,"loadScriptureDetails").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.bibleDetails).toEqual([]);
	})
})