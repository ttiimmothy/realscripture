import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {AdminHomeService} from "../admin-home.service";

@Component({
    selector: "app-print-form",
    templateUrl: "./print-form.component.html",
    styleUrls: ["./print-form.component.scss"],
})
export class PrintFormComponent implements OnInit{
    constructor(private adminHomeService:AdminHomeService,private route:ActivatedRoute){}

    ngOnInit():void{
		this.adminHomeService.loadTaskFinishDate(this.id).subscribe((result) => {
			if(result.success){
				this.taskInfo = result.data;
			}
		})
		for(let i = 0; i < 6; i++){
			this.length[i] = "";
		}
	}

	taskInfo = {
		user_id:0,
		username:"",
		finish_date:""
	}
	id = Number(this.route.snapshot.paramMap.get("id"));

	length:string[] = [];
}