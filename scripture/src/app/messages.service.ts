import {Injectable} from "@angular/core";

@Injectable({
	providedIn:"root"
})
export class MessagesService{
	constructor(){}
	messageList:string[] = [];

	add(message:string){
		this.messageList.push(message);
	}

	clear(){
		this.messageList = [];
	}
}