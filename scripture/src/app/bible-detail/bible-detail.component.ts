import {Component, OnInit} from '@angular/core';
import {BibleService} from "../bible.service";
import {UserService} from "../user.service";
import {Sentence} from "./sentence";

@Component({
    selector: 'app-bible-detail',
    templateUrl: './bible-detail.component.html',
    styleUrls: ['./bible-detail.component.scss'],
})
export class BibleDetailComponent implements OnInit{
    constructor(private userService:UserService,private bibleService:BibleService){}
    ngOnInit():void{
		if(localStorage.getItem("token")){
			this.userService.checkCurrentUsers().subscribe((result) => {
				if(result && result.success){
					this.user = result.user;
					this.userService.token = localStorage.getItem("token");
				}
			})
		}
		this.bibleService.loadScriptureDetails().subscribe((result) => {
			// console.log(result);
			if(result.success){
				this.bibleDetails = result.data;
			}
		})
	}

	user = "";
	bibleDetails:Sentence[] = []
}