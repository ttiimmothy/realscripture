export interface Sentence{
	id:number;
	bible_id:number;
	content:string;
}
export interface Tasks{
	id:number;
	username:string;
	finish_date:Date;
}