import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ComponentFixture,TestBed} from "@angular/core/testing";
import {MatIconModule} from "@angular/material/icon";
import {RouterTestingModule} from "@angular/router/testing";
import {of} from "rxjs";
import {UserService} from "../user.service";
import {BottomBarComponent} from "./bottom-bar.component";

describe("BottomBarComponent",() => {
	let component:BottomBarComponent;
	let fixture:ComponentFixture<BottomBarComponent>;
	let userService:UserService;

	beforeEach(async() => {
		await TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				RouterTestingModule,
				MatIconModule
			],
			declarations:[BottomBarComponent]
		}).compileComponents();
	})

	beforeEach(() => {
		fixture = TestBed.createComponent(BottomBarComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
		userService = TestBed.inject(UserService);
	})

	it("should create",() => {
		expect(component).toBeTruthy();
	})

	it("should get admin user information",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,data:{is_admin:true}}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.isAdmin).toEqual(true);
	})

	it("cannot get user information",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(component.isAdmin).toEqual(false);
	})
})
