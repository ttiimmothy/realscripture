export interface ChatMessage{
	id:number;
	username:string;
	icon:string;
	send_user_id:number;
	chatroom_id:number;
	content:string;
	type:string;
	created_at:Date;
	updated_at:Date;
}
export interface Chatroom{
	id:number;
	room_name:string;
	room_icon:string;
	created_at:Date|null;
	updated_at:Date|null;
}
export interface User{
	id:number;
	username:string;
	phone:number;
	icon:string;
	qr_code:string;
	is_admin:boolean|null;
}
export interface ChatUser{
	id:number;
	username:string;
	icon:string;
	is_admin:boolean;
}
export interface Sentence{
	id:number;
	bible_id:number;
	content:string;
}