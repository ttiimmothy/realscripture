import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
// import {environment} from "src/environments/environment";
// import {ImageService} from "../image.service";
import {UserService} from "../user.service";

@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {
    constructor(private userService:UserService,private router:Router){}
    ngOnInit(): void{}

	newImageIcon:any;
	imageSource:string|ArrayBuffer|null = "";
	readURL(event:any):void{
		if(event.currentTarget && event.currentTarget.files && event.currentTarget.files[0]){
			this.newImageIcon = event.currentTarget.files[0];
			const file = event.currentTarget.files[0];
			const reader = new FileReader();
			reader.onload = () => this.imageSource = reader.result;
			reader.readAsDataURL(file);
		}
	}

	signUp(file:File|null,phone:string,username:string){
		this.userService.signUp(file,phone,username).subscribe((result) => {
			if(result.success){
				setTimeout(() => {
					this.router.navigate(["/chat"]);
				},1000)
			}
		})
	}
}