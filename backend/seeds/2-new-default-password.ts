import {Knex} from "knex";
import {hashPassword} from "../hash";

export async function seed(knex:Knex):Promise<void>{
	// Deletes ALL existing entries
	await knex("default_password").del();
	// Inserts seed entries
	await knex("default_password").insert([
		{password:await hashPassword("123456")},
	])
}