// Http testing module and mocking controller
import {HttpClientTestingModule,HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {UserService} from './user.service';

describe('UserService',() => {
	let service:UserService;
	let httpTestingController:HttpTestingController;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports:[HttpClientTestingModule]
		})
		service = TestBed.inject(UserService);
		httpTestingController = TestBed.inject(HttpTestingController);
	})

	it('should be created',() => {
		expect(service).toBeTruthy();
	})

	it("should login",() => {
		let expected = "";
		service.login("","").subscribe((result) => {
			expect(result).toEqual(expected)
		})
		let testService = httpTestingController.expectOne("http://localhost:8160/user/login");
		testService.flush(expected);
	})

	it("should login as admin",() => {
		let expected = "";
		service.adminLogin("","").subscribe((result) => {
			expect(result).toEqual(expected);
		})
		let testService = httpTestingController.expectOne("http://localhost:8160/user/admin/login");
		expect(testService.request.method).toBe("POST");
		testService.flush(expected);
	})

	it("should sign up without icon",() => {
		let expected = "";
		service.signUp(null,"","").subscribe((result) => {
			expect(result).toEqual(expected);
		})
		let testService = httpTestingController.expectOne("http://localhost:8160/user/newUser");
		expect(testService.request.method).toBe("POST");
		testService.flush(expected);
	})

	it("should sign up with icon",() => {
		let expected = "";
		service.signUp(new File(["foo"],""),"","").subscribe((result) => {
			expect(result).toEqual(expected);
		})
		let testService = httpTestingController.expectOne("http://localhost:8160/user/newUser");
		expect(testService.request.method).toBe("POST");
		testService.flush(expected);
	})

	it("should change password",() => {
		let expected = "";
		service.changePassword("").subscribe((result) => {
			expect(result).toEqual(expected);
		})
		let testService = httpTestingController.expectOne("http://localhost:8160/setting/password");
		expect(testService.request.method).toBe("POST");
		testService.flush(expected);
	})

	it("should change default password",() => {
		let expected = "";
		service.changeDefaultPassword("").subscribe((result) => {
			expect(result).toEqual(expected);
		})
		let testService = httpTestingController.expectOne("http://localhost:8160/setting/defaultPassword");
		expect(testService.request.method).toBe("POST");
		testService.flush(expected);
	})
})