export interface NewUser{
	id:number;
	username:string;
	icon:string;
}
export interface Sentence{
	id:number;
	bible_id:number;
	content:string;
}