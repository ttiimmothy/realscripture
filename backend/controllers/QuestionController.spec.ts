import {Knex} from "knex";
import {QuestionService} from "../services/QuestionService";
import {QuestionController} from "./QuestionController";
import {Request,Response} from "express";
import {logger} from "../logger";
import {Time} from "../services/model";

describe("QuestionController",() => {
	let questionController:QuestionController;
	let questionService:QuestionService;
	let req:Request;
	let res:Response;

	beforeEach(() => {
		questionService = new QuestionService({} as Knex);
		jest.spyOn(questionService,"loadQuestion").mockImplementation(() => Promise.resolve([{id:1,content:"25",
		remarks:"hello",created_at:new Date("2021-07-30"),updated_at:new Date("2021-07-30")}]));

		jest.spyOn(questionService,"submitRecord").mockImplementation((time:Time[],userId:number|undefined,date:Date) =>
		Promise.resolve());

		req = {
			body:{},
			params:{},
			query:{},
			user:{
				id:1,
				username:"timothy"
			}
		} as any as Request;

		res = {
			json:jest.fn((obj:any) => null),
			status:jest.fn((statusCode:number) => res)
		} as any as Response;

		questionController = new QuestionController(questionService);

		logger.error = jest.fn();
	})

	it("should get all questions",async() => {
		await questionController.loadQuestion(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:[{id:1,content:"25",remarks:"hello",created_at:
		new Date("2021-07-30"),updated_at:new Date("2021-07-30")}]});
	})

	it("should submit answer",async() => {
		req.body = {time:[],answer:[]}
		await questionController.submitRecord(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true});
	})

	it(`should get the same result in submit answer when req.user is undefined or when req.user is
	defined`,async () => {
		req.body = {time:[],answer:[]};
		req.user = undefined;
		await questionController.submitRecord(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true});
	})

	it("should print error when get question bank get error",async () => {
		req.path = "/question/questionBank";
		req.method = "get";
		res = {} as any
		try{
			await questionController.loadQuestion(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:get path:/question/questionBank")
		}
	})

	it("should print error when submit record get error",async () => {
		req.path = "/question/answer";
		req.method = "post";
		res = {} as any
		try{
			await questionController.submitRecord(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:post path:/question/answer")
		}
	})
})