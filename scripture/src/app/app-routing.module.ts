import {NgModule} from '@angular/core';
import {RouterModule,Routes} from '@angular/router';
import {AccessGuard} from "./access.guard";
import {AddUserComponent} from "./add-user/add-user.component";
import {AdminHomeComponent} from "./admin-home/admin-home.component";
import {AdminComponent} from "./admin/admin.component";
import {BibleDetailComponent} from "./bible-detail/bible-detail.component";
import {ChatroomDetailComponent} from "./chatroom-detail/chatroom-detail.component";
import {ChatroomComponent} from "./chatroom/chatroom.component";
import {CreateChatroomComponent} from "./create-chatroom/create-chatroom.component";
import {HomeComponent} from "./home/home.component";
import {LoginComponent} from "./login/login.component";
import {NoticeComponent} from "./notice/notice.component";
import {PrintFormComponent} from "./print-form/print-form.component";
import {SignUpComponent} from "./sign-up/sign-up.component";
import {UnmarkedTasksComponent} from "./unmarked-tasks/unmarked-tasks.component";

const routes:Routes = [
	{path:"home/admin/task/:id",component:UnmarkedTasksComponent,data:{requiresAdminLogin:true},canActivate:[AccessGuard]},
	{path:"home/admin/bible",component:BibleDetailComponent,data:{requiresAdminLogin:true},canActivate:[AccessGuard]},
	{path:"home/admin/print/:id",component:PrintFormComponent,data:{requiresAdminLogin:true},canActivate:[AccessGuard]},
	{path:"home/admin",component:AdminHomeComponent,data:{requiresAdminLogin:true},canActivate:[AccessGuard]},
	{path:"sign/up",component:SignUpComponent,data:{requiresAdminLogin:true},canActivate:[AccessGuard]},
	{path:"home",component:HomeComponent,data:{requiresUserLogin:true},canActivate:[AccessGuard]},
	{path:"notice",component:NoticeComponent,data:{requiresLogin:true},canActivate:[AccessGuard]},
	{path:"chat/add/user/:id",component:AddUserComponent,data:{requiresLogin:true},canActivate:[AccessGuard]},
	{path:"chat/create",component:CreateChatroomComponent,data:{requiresLogin:true},canActivate:[AccessGuard]},
	{path:"chat/:id",component:ChatroomDetailComponent,data:{requiresLogin:true},canActivate:[AccessGuard]},
	{path:"chat",component:ChatroomComponent,data:{requiresLogin:true},canActivate:[AccessGuard]},
	{path:"admin/login",component:AdminComponent},
	{path:"",component:LoginComponent},
	{path:"**",redirectTo:"",pathMatch:"full"},
]

@NgModule({
	imports:[RouterModule.forRoot(routes)],
	exports:[RouterModule]
})
export class AppRoutingModule{}