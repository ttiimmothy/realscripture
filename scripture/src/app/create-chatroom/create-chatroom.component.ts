import {Component,OnInit} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {Router} from "@angular/router";
import {environment} from "src/environments/environment";
import {AccessGuard} from "../access.guard";
import {BibleService} from "../bible.service";
// import {Observable, Subject} from "rxjs";
// import {debounceTime, distinctUntilChanged, switchMap} from "rxjs/operators";
import {ChatroomService} from "../chatroom.service";
import {ImageService} from "../image.service";
import {UserService} from "../user.service";
import {NewUser, Sentence} from "./newUser";

@Component({
    selector:"app-create-chatroom",
    templateUrl:"./create-chatroom.component.html",
    styleUrls:["./create-chatroom.component.scss"],
})
export class CreateChatroomComponent implements OnInit{
    constructor(private userService:UserService,private chatroomService:ChatroomService,private imageService:ImageService,
	private router:Router,private bibleService:BibleService,private sanitizer:DomSanitizer){}
    ngOnInit():void{
		if(localStorage.getItem("token")){
			this.userService.checkCurrentUsers().subscribe((result) => {
				// console.log(result)
				if(result && result.success){
					this.user = result.user;
				}
			})
		}
		this.bibleService.loadScripture().subscribe((result) => {
			// console.log(result)
			if(result.success){
				this.goldenSentence = result.data;
			}
		})
	}
	// private searchTerms = new Subject<string>();
	user = "";
	goldenSentence:Sentence = {
		id:0,
		bible_id:0,
		content:""
	}
	// newMessage = "";
	usernames:NewUser[] = [];
	choseUsernames:NewUser[] = [];
	searchKeyword = "";
	search(username:string,existUser:NewUser[]){
		if(username){
			this.chatroomService.searchPerson(username,existUser.map(user => user.username)).subscribe((result) => {
				// console.log(result)
				if(result.success){
					this.usernames = result.data;
					for(let user of this.usernames){
						this.getImageFromService(`${this.imagePath}/${user.icon}`,user.id);
					}
				}
			})
		}else{
			this.usernames = [];
		}
	}

	addUser(id:number,username:string,icon:string){
		this.choseUsernames.push({id,username,icon});
		for(let user of this.choseUsernames){
			this.getImageFromService(`${this.imagePath}/${user.icon}`,user.id);
		}
		this.usernames = [];
		this.searchKeyword = "";
	}
	deleteUser(username:string){
		this.choseUsernames = this.choseUsernames.filter((user) => user.username !== username);
	}

	// imagePath = environment.BACKEND_PATH;
	imagePath = environment.IMAGE_PATH;
	imageToShow:(string|ArrayBuffer|null|any)[] = [];
	createImageFromBlob(image:Blob,index:number){
		let reader = new FileReader();
		reader.addEventListener("load",() => {
			this.imageToShow[index] = this.sanitizer.bypassSecurityTrustResourceUrl(reader.result as string);
		},false);
		if(image){
			reader.readAsDataURL(image);
		}
	}
	getImageFromService(imageUrl:string,index:number){
		// this.imageService.getImage(imageUrl).subscribe((data) => {
		this.imageService.getImageWithoutHeaders(imageUrl).subscribe((data) => {
			// console.log(data);
			this.createImageFromBlob(data,index);
		},(error) => {
			// console.log(error);
		})
	}

	newImageIcon:any;
	imageSource:string|ArrayBuffer|null = "";
	readURL(event:any):void{
		if(event.currentTarget && event.currentTarget.files && event.currentTarget.files[0]){
			this.newImageIcon = event.currentTarget.files[0];
			const file = event.currentTarget.files[0];
			const reader = new FileReader();
			reader.onload = () => this.imageSource = reader.result;
			reader.readAsDataURL(file);
		}
	}
	createRoom(name:string,image:File,users:NewUser[]){
		this.chatroomService.createRoom(name,image,users.map((user) => user.username)).subscribe((result) => {
			if(result.success){
				setTimeout(() => {
					this.router.navigate(["/chat"])
				},1000)
			}
		})
	}
}