import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ComponentFixture,fakeAsync,TestBed, tick} from '@angular/core/testing';
import {ActivatedRoute, Router} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {of} from "rxjs";
import {AccessGuard} from "../access.guard";
import {ChatroomComponent} from "../chatroom/chatroom.component";
import {UserService} from "../user.service";
import {AdminComponent} from './admin.component';

describe('AdminComponent', () => {
    let component:AdminComponent;
    let fixture:ComponentFixture<AdminComponent>;
	let userService:UserService;
	let accessGuard:AccessGuard;
	let router:Router;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				RouterTestingModule.withRoutes(
					[
						{path:"chat/1",component:ChatroomComponent},
						{path:"admin/login",component:AdminComponent}
					]
				)
			],
            declarations:[
				AdminComponent
			]
        }).compileComponents();
    })

    beforeEach(() => {
		router = TestBed.inject(Router);
        fixture = TestBed.createComponent(AdminComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
		userService = TestBed.inject(UserService);
		accessGuard = TestBed.inject(AccessGuard);
		localStorage.setItem("token","abc");
    })

    it("should create",() => {
        expect(component).toBeTruthy();
    })

	it("cannot get current user when localStorage token is empty",() => {
		localStorage.setItem("token","");
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy",data:{is_admin:true}}));
		component.ngOnInit();
		fixture.detectChanges();
		expect(accessGuard.user).toEqual("");
	})

	it("should get admin user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy",data:{is_admin:true}}));
		let routerNew = spyOn(router,"navigate");
		// tick();
		component.ngOnInit();
		fixture.detectChanges();
		expect(accessGuard.user).toEqual("admin");
		expect(routerNew).toHaveBeenCalledWith(["/chat/1"]);
	})

	it("cannot get success result when check current users",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:false}));
		component.ngOnInit();
		// tick();
		fixture.detectChanges();
		expect(userService.checkCurrentUsers).toHaveBeenCalled();
	})

	it("cannot get admin user",() => {
		spyOn(userService,"checkCurrentUsers").and.returnValue(of({success:true,user:"timothy",data:{is_admin:false}}));
		component.ngOnInit();
		// tick();
		fixture.detectChanges();
		expect(accessGuard.user).toEqual("");
	})

	it("should login as admin",() => {
		spyOn(userService,"adminLogin").and.returnValue(of({success:true,data:"cba"}));
		component.ngOnInit();
		// tick();
		fixture.detectChanges();
		component.adminLogin("","");
		expect(localStorage.token).toEqual("cba");
		expect(accessGuard.user).toEqual("admin");
	})

	it("cannot login as admin when the service returns {success:false}",() => {
		spyOn(userService,"adminLogin").and.returnValue(of({success:false}));
		component.ngOnInit();
		fixture.detectChanges();
		component.adminLogin("","");
		expect(accessGuard.user).toEqual("");
	})
})
