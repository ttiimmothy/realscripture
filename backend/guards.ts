import {Request,Response,NextFunction} from "express";
import jwt from "jwt-simple";
import {Bearer} from "permit";
import {logger} from "./logger";
import {authService} from "./main";
import {User} from "./services/model";

const permit = new Bearer({
	query:"access_token"
})

export async function isLoggedIn(req:Request,res:Response,next:NextFunction){
	try{
		const token = permit.check(req);
		// console.log(token)
		const payload = jwt.decode(token,process.env.JWT_SECRET!)
		if(!payload){
			res.json({message:"Permission denied"})
			return
		}
		// console.log(payload.phone);
		const user:User = (await authService.checkCurrentUser(payload.phone))[0];
		if(user){
            req.user = user;
			// console.log(req.user?.id + " guards");
            return next();
        }else{
            return res.json({message:"User does not exist"});
        }
	}catch(e){
		logger.error(e);
		res.status(500).json({message:"Permission denied"})
	}
}