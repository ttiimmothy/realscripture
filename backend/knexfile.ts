// Update with your config settings.
import dotenv from "dotenv";
dotenv.config();
// logger.debug(path.join(__dirname + "/./env/.env"));
// console.log(process.env)

module.exports = {
    development:{
        // environment
        // debug:true, // 佢會log低每次行嘅sql落terminal度,debug嗰陣必定要uncomment咗呢句
        client:"postgresql",
        connection:{
            database:process.env.DB_NAME,
            user:process.env.DB_USERNAME,
            password:process.env.DB_PASSWORD,
        },
        pool:{
            min:2,
            max:10,
        },
        migrations:{
            tableName:"knex_migrations",
        },
    },
    staging:{
        client:"postgresql",
        connection:{
            database:process.env.DB_NAME,
            user:process.env.DB_USERNAME,
            password:process.env.DB_PASSWORD,
        },
        pool:{
            min:2,
            max:10,
        },
        migrations:{
            tableName:"knex_migrations",
        },
    },
    production:{
        client:"postgresql",
        connection:{
            database:process.env.POSTGRES_DB,
            user:process.env.POSTGRES_USER,
            password:process.env.POSTGRES_PASSWORD,
			host:process.env.POSTGRES_HOST,
			port:process.env.PORT
        },
        pool:{
            min:2,
            max:10,
        },
        migrations:{
            tableName:"knex_migrations",
        },
    },

	// this testing branch is for my computer use

    // testing:{
    //     client:"postgresql",
    //     connection:{
    //         database:process.env.TEST_BD,
    //         user:process.env.DB_USERNAME,
    //         password:process.env.DB_PASSWORD,
	// 		// host:process.env.POSTGRES_HOST,
    //     },
    //     pool:{
    //         min:2,
    //         max:10,
    //     },
    //     migrations:{
    //         tableName:"knex_migrations",
    //     },
    // },

	// this testing branch is for gitlab use

	testing:{
        client:"postgresql",
        connection:{
            database:process.env.POSTGRES_DB,
            user:process.env.POSTGRES_USER,
            password:process.env.POSTGRES_PASSWORD,
			host:process.env.POSTGRES_HOST,
        },
        pool:{
            min:2,
            max:10,
        },
        migrations:{
            tableName:"knex_migrations",
        },
    }
}