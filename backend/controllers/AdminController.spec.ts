import {AdminService} from "../services/AdminService";
import {AdminController} from "./AdminController";
import {Request,Response} from "express";
import {Knex} from "knex";
// import {Logger} from "winston";
import {logger} from "../logger";

describe("AdminController",() => {
	let adminController:AdminController;
	let adminService:AdminService;
	let req:Request;
	let res:Response;

	beforeEach(() => {
		adminService = new AdminService({} as Knex);
		jest.spyOn(adminService,"loadUnmarkedTasks").mockImplementation(() => Promise.resolve([{"id":1
		,username:"timothy",finish_date:new Date("2021-07-29")}]));

		jest.spyOn(adminService,"loadUnmarkedTasksWithRange").mockImplementation((start:string,finish:string) =>
		Promise.resolve([{"id":1,username:"timothy",finish_date:new Date("2021-07-29")}]));

		jest.spyOn(adminService,"loadMarkedTasks").mockImplementation(() => Promise.resolve([{"id":1
		,username:"timothy",finish_date:new Date("2021-07-29")}]));

		jest.spyOn(adminService,"loadMarkedTasksWithRange").mockImplementation((start:string,finish:string) =>
		Promise.resolve([{"id":1,username:"timothy",finish_date:new Date("2021-07-29")}]));

		jest.spyOn(adminService,"loadTaskFinishDate").mockImplementation((taskId:number) =>
		Promise.resolve([{user_id:1,username:"timothy",finish_date:new Date("2021-07-29")}]));

		jest.spyOn(adminService,"loadUserAnswer").mockImplementation((taskId:number) => Promise.resolve([{id:1,
		username:"timothy",content:"25",remarks:"hello"}]));

		jest.spyOn(adminService,"finishMarkingAnswer").mockImplementation((taskId:number) => Promise.resolve());

		adminController = new AdminController(adminService);

		req = {
			body:{},
			query:{},
			params:{},
			path:{},
			method:{},
			user:{
				id:1,
				username:"Timothy"
			}
		} as any as Request;

		res = {
			json:jest.fn((obj:any) => null),
			status:jest.fn((statusCode:number) => res)
		} as any as Response;

		logger.error = jest.fn();
	})

	it("should get unmarked tasks",async () => {
		await adminController.loadUnmarkedTasks(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:[{"id":1,username:"timothy",
		finish_date:new Date("2021-07-29")}]});
	})

	it("should get unmarked tasks with time range",async () => {
		req.body = {start:"2021-07-28",end:"2021-07-30"};
		await adminController.loadUnmarkedTasksWithRange(req,res);
		expect(adminService.loadUnmarkedTasksWithRange).toBeCalledWith("2021-07-28","2021-07-30");
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:[{"id":1,username:"timothy",
		finish_date:new Date("2021-07-29")}]});
	})

	it("should get marked tasks",async () => {
		await adminController.loadMarkedTasks(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:[{"id":1,username:"timothy",
		finish_date:new Date("2021-07-29")}]});
	})

	it("should get marked tasks with time range",async () => {
		req.body = {start:"2021-07-28",end:"2021-07-30"};
		await adminController.loadMarkedTasksWithRange(req,res);
		expect(adminService.loadMarkedTasksWithRange).toBeCalledWith("2021-07-28","2021-07-30");
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:[{"id":1,username:"timothy",
		finish_date:new Date("2021-07-29")}]});
	})

	it("should get finish date of tasks",async () => {
		await adminController.loadTaskFinishDate(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:{user_id:1,username:"timothy",finish_date:new Date("2021-07-29")}});
	})

	it("should get user answer",async () => {
		await adminController.loadUserAnswer(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true,data:[{id:1,username:"timothy",content:"25",remarks:"hello"}]});
	})

	it("should mark user finish work",async () => {
		await adminController.finishMarkingAnswer(req,res);
		expect(res.json).toBeCalledTimes(1);
		expect(res.json).toBeCalledWith({success:true});
	})

	it("should print error when getting unmarked tasks get error",async () => {
		req.path = "/admin/tasks";
		req.method = "get";
		res = {} as any
		try{
			await adminController.loadUnmarkedTasks(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:get path:/admin/tasks")
		}
	})

	it("should print error when getting unmarked tasks within a time range get error",async () => {
		req.path = "/admin/tasksWithRange";
		req.method = "post";
		res = {} as any
		try{
			await adminController.loadUnmarkedTasksWithRange(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:post path:/admin/tasksWithRange")
		}
	})

	it("should print error when getting marked tasks get error",async () => {
		req.path = "/admin/markedTasks";
		req.method = "get";
		res = {} as any
		try{
			await adminController.loadMarkedTasks(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:get path:/admin/markedTasks")
		}
	})

	it("should print error when getting marked tasks within a time range get error",async () => {
		req.path = "/admin/markedTasksWithRange";
		req.method = "post";
		res = {} as any
		try{
			await adminController.loadMarkedTasksWithRange(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:post path:/admin/markedTasksWithRange")
		}
	})

	it("should print error when getting task finish date get error",async () => {
		req.path = "/admin/taskFinishDate/1";
		req.method = "get";
		res = {} as any
		try{
			await adminController.loadTaskFinishDate(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:get path:/admin/taskFinishDate/1")
		}
	})

	it("should print error when getting user answers get error",async () => {
		req.path = "/admin/dailyAnswer/1";
		req.method = "get";
		res = {} as any
		try{
			await adminController.loadUserAnswer(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:get path:/admin/dailyAnswer/1")
		}
	})

	it("should print error when finish marking get error",async () => {
		req.path = "/admin/markingDone/1";
		req.method = "get";
		res = {} as any
		try{
			await adminController.finishMarkingAnswer(req,res);
		}catch(e){
			expect(logger.error).toBeCalledTimes(2);
			expect(logger.error).toBeCalledWith(e);
			expect(logger.error).toBeCalledWith("method:get path:/admin/markingDone/1")
		}
	})
})