import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot,CanActivate,Router,RouterStateSnapshot,UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {UserService} from "./user.service";

@Injectable({
	providedIn:"root"
})
export class AccessGuard implements CanActivate{
	constructor(private userService:UserService,private router:Router){}
	user = "";
	token = localStorage.getItem("token");

	canActivate(route:ActivatedRouteSnapshot,state:RouterStateSnapshot):Observable<boolean|UrlTree>|Promise<boolean|UrlTree>|
	boolean|UrlTree{
		const url = state.url;
		return this.checkLogin(route,url);
	}

	checkLogin(route:ActivatedRouteSnapshot,url:string){
		this.userService.checkCurrentUsers().subscribe((result) => {
			if(result && result.success && result.data.is_admin){
				this.user = "admin";
			}
		})
		const requiresLogin = route.data.requiresLogin || false;
		if(requiresLogin){
			if(this.userService.token){
				return true;
			}
			this.router.navigate(["/"])
			return false;
		}
		const requiresUserLogin = route.data.requiresUserLogin || false;
		if(requiresUserLogin){
			if(this.userService.token && this.user !== "admin"){
				return true;
			}else if(this.user === "admin"){
				this.router.navigate(["/admin/login"]);
				return false;
			}else{
				this.router.navigate(["/"]);
				return false;
			}
		}
		const requiresAdminLogin = route.data.requiresAdminLogin || false;
		if(requiresAdminLogin){
			if(this.userService.token && this.user === "admin"){
				return true;
			}else{
				this.router.navigate(["/"]);
				return false;
			}
		}
		return false;
	}
}