import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("users",(table) => {
		table.increments();
		table.string("username").unique()
		table.integer("phone").unique().notNullable();
		table.string("icon");
		table.boolean("is_admin").defaultTo(false);
		table.string("qr_code");
		table.text("password");
		table.timestamps(false,true);
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("users");
}