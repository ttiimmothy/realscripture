import {Knex} from "knex";
import {logger} from "../logger";

export class UserService{
	constructor(private knex:Knex){}

	async login(phoneNumber:number){
		return await this.knex.select("*").from("users").where("phone",phoneNumber).andWhere("is_admin",false);
	}

	async adminLogin(phoneNumber:number){
		return await this.knex.select("*").from("users").where("phone",phoneNumber).andWhere("is_admin",true);
	}

	async getUserPassword(userId:number){
		return await this.knex.select("*").from("users").where("id",userId);
	}

	async signUp(username:string,phone:number,icon:string = "unknown.png"){
		let trx = await this.knex.transaction();
		try{
			let password = (await trx.select("*").from("default_password"))[0].password;
			let userId = (await trx.insert({username,phone,icon,qr_code:"",password}).into("users").returning('id'))[0];
			let chatroomId = (await trx.select("*").from("chatroom").where("room_name","announcement"))[0].id;
			await trx.insert({user_id:userId,chatroom_id:chatroomId}).into("user_chatroom");
			await trx.commit();
		}catch(e){
			await trx.rollback();
			logger.error("knex transaction rollback");
			throw e;
		}
	}
}