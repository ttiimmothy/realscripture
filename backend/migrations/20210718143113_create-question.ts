import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("question",(table) => {
		table.increments();
		table.text("content");
		table.text("remarks");
		table.timestamps(false,true)
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("question");
}