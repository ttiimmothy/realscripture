import {Route} from "@angular/compiler/src/core";
import {TestBed} from "@angular/core/testing";
import {ActivatedRouteSnapshot, RouterStateSnapshot, UrlSegment} from "@angular/router";
import {ActivateGuard} from "./activate.guard";

function fakeRouterState(url:string):RouterStateSnapshot{
	return {
		url
	} as RouterStateSnapshot;
}

describe("ActivateGuard", () => {
    let guard:ActivateGuard;
	let route:ActivatedRouteSnapshot;
	let url = "/";
	let segment:UrlSegment[];
	let routeMock:Route;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        guard = TestBed.inject(ActivateGuard);
		route = {} as any as ActivatedRouteSnapshot;
		segment = [] as UrlSegment[];
		routeMock = {} as any as Route;
    })

    it("should be created", () => {
        expect(guard).toBeTruthy();
    })

	it("should be true when calling canActivate",() => {
		expect(guard.canActivate(route,fakeRouterState(url))).toEqual(true);
	})

	it("should be true when calling canActivateChild",() => {
		expect(guard.canActivateChild(route,fakeRouterState(url))).toEqual(true);
	})

	it("should be true when calling canDeactivate",() => {
		expect(guard.canDeactivate({},route,fakeRouterState(url))).toEqual(true);
	})

	it("should be true when calling canLoad",() => {
		expect(guard.canLoad(routeMock,segment)).toEqual(true);
	})
})