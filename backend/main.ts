import cors from "cors";
import express from "express";
import dotenv from "dotenv";
import Knex from "knex";
import {adminRoutes,authRoutes,bibleRoutes,chatroomRoutes,initRoutes,questionRoutes,settingRoutes,userRoutes} from "./routes";
import {UserController} from "./controllers/UserController";
import {UserService} from "./services/UserService";
import {QuestionService} from "./services/QuestionService";
import {QuestionController} from "./controllers/QuestionController";
import {ChatroomService} from "./services/ChatroomService";
import {ChatroomController} from "./controllers/ChatroomController";
import multer from "multer";
import path from "path";
import {isLoggedIn} from "./guards";
import http from "http";
import {Socket,Server as SocketIO} from "socket.io";
import {logger} from "./logger";
import {BibleController} from "./controllers/BibleController";
import {BibleService} from "./services/BibleService";
import {AuthService} from "./services/AuthService";
import {AuthController} from "./controllers/AuthController";
import {AdminService} from "./services/AdminService";
import {AdminController} from "./controllers/AdminController";
import multerS3 from "multer-s3";
// import aws from "aws-sdk";
// const S3 = require("../../node_modules/aws-sdk/clients/s3");
import S3 from 'aws-sdk/clients/s3';
import {SettingService} from "./services/SettingService";
import {SettingController} from "./controllers/SettingController";
import {exec} from "child_process";
import fs from "fs";
const knexfile = require("./knexfile");
const knex = Knex(knexfile[process.env.NODE_ENV || "development"]);
dotenv.config();

const app = express();
app.use(cors({
	origin:["http://localhost:4260","https://scripture.denominator.ml"]
}))
app.use(express.urlencoded({extended:true}));
app.use(express.json());

const s3 = new S3({
	accessKeyId:process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey:process.env.AWS_SECRET_ACCESS_KEY,
	region:"ap-southeast-1",
	// bucket:"image.scripture"
})
const storage = multer.diskStorage({
	destination:function(req,file,cb){
		cb(null,path.resolve("./uploads"));
	},
	filename:function(req,file,cb){
		cb(null,`${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`)
	}
})
const awsStorage = multerS3({
	s3:s3,
	// acl:'public-read',
	bucket:"image.scripture",
	metadata:(req,file,cb) => {
		cb(null,{fieldName:file.fieldname});
	},
	key:(req,file,cb) => {
		cb(null,`${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
	},
})
export const upload = multer({storage});
export const uploadWithAws = multer({storage:awsStorage});

export const server = new http.Server(app);
export const io = new SocketIO(server,{
	cors:{
		origin:["http://localhost:4260","https://scripture.denominator.ml"]
	}
})

io.on("connection",async(socket:Socket) => {
	try{
		// console.log((socket.request as express.Request).user)
		// let roomIds = await knex.select("chatroom_id").from("user_chatroom").where("user_id",(socket.request as express.Request).user?.id)
		// console.log(roomIds)
		socket.on("joinRoom",(chatroomId) => {
			// console.log(`chatroom ${chatroomId}`)
			socket.join(`userChatroom ${chatroomId}`);
		})
		socket.on("leaveRoom",(chatroomId) => {
			// console.log(`chatroom ${chatroomId}`)
			socket.leave(`userChatroom ${chatroomId}`);
		})
	}catch(e){
		logger.error(e);
	}
})

export const userService = new UserService(knex);
export const authService = new AuthService(knex);
export const questionService = new QuestionService(knex);
export const chatroomService = new ChatroomService(knex);
export const bibleService = new BibleService(knex);
export const adminService = new AdminService(knex);
export const settingService = new SettingService(knex);
export const userController = new UserController(userService);
export const authController = new AuthController();
export const questionController = new QuestionController(questionService);
export const chatroomController = new ChatroomController(chatroomService,io);
export const bibleController = new BibleController(bibleService);
export const adminController = new AdminController(adminService);
export const settingController = new SettingController(settingService);
initRoutes();
app.use("/user",userRoutes);
app.use(isLoggedIn);
app.use("/auth",authRoutes);
app.use("/question",questionRoutes);
app.use("/chatroom",chatroomRoutes);
app.use("/bible",bibleRoutes);
app.use("/admin",adminRoutes);
app.use("/setting",settingRoutes);
app.use(express.static("uploads"));

function execSample(args: string){
	exec(args,{encoding:"utf8"},async(error,stdout) => {
		// console.log(stdout);
		// console.log(stdout.split(" "));
		let storageNumber;
		let storage = ((stdout.split(" "))[1].split("\t./"))[0];
		if(storage[storage.length - 1] === "K"){
			storageNumber = parseInt(storage) * 1000;
		}else if(storage[storage.length - 1] === "M"){
			storageNumber = parseInt(storage) * 1000 * 1000;
		}else if(storage[storage.length - 1] === "G"){
			storageNumber = parseInt(storage) * 1000 * 1000 * 1000;
		}else{
			storageNumber = 0;
		}
        if(storageNumber > 700 * 1000 * 1000){
			let files = await knex.select("*").from("file_detail").orderBy("id");
			for(let i = 0; i < 5; i++){
				try{
					fs.unlinkSync(files[i].name)
				}catch(e){
					logger.error(e);
				}
			}
		}
    })
}
setInterval(() => {
	execSample("du -sh ./uploads");
},1209600000)

server.listen(8160,() => {
	console.log("Listening to http://localhost:8160");
})