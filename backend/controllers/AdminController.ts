import {Request, Response} from "express";
import {logger} from "../logger";
import {AdminService} from "../services/AdminService";
export class AdminController{
	constructor(private adminService:AdminService){}

	loadUnmarkedTasks = async(req:Request,res:Response) => {
		try{
			let tasks = await this.adminService.loadUnmarkedTasks();
			// console.log(tasks);
			res.status(200).json({success:true,data:tasks});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	loadMarkedTasks = async(req:Request,res:Response) => {
		try{
			let tasks = await this.adminService.loadMarkedTasks();
			// console.log(tasks);
			res.status(200).json({success:true,data:tasks});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	loadUnmarkedTasksWithRange = async(req:Request,res:Response) => {
		try{
			let tasks = await this.adminService.loadUnmarkedTasksWithRange(req.body.start,req.body.end);
			res.status(200).json({success:true,data:tasks});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	loadMarkedTasksWithRange = async(req:Request,res:Response) => {
		try{
			let tasks = await this.adminService.loadMarkedTasksWithRange(req.body.start,req.body.end);
			res.status(200).json({success:true,data:tasks});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	loadTaskFinishDate = async(req:Request,res:Response) => {
		try{
			let nameAndDate = (await this.adminService.loadTaskFinishDate(parseInt(req.params.id)))[0];
			res.status(200).json({success:true,data:nameAndDate});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	loadUserAnswer = async(req:Request,res:Response) => {
		try{
			let answers = await this.adminService.loadUserAnswer(parseInt(req.params.id));
			res.status(200).json({success:true,data:answers});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	finishMarkingAnswer = async(req:Request,res:Response) => {
		try{
			await this.adminService.finishMarkingAnswer(parseInt(req.params.id));
			res.status(200).json({success:true});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}
}