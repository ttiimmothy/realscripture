export interface Chatroom{
	id:number;
	user_id:number;
	chatroom_id:number;
	room_name:string;
	room_icon:string;
	created_at:Date;
	updated_at:Date;
}
export interface Sentence{
	id:number;
	bible_id:number;
	content:string;
}