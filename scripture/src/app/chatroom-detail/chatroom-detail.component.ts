import {AfterViewChecked, Component,ElementRef,OnDestroy,OnInit,ViewChild} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {ActivatedRoute} from "@angular/router";
import {environment} from "src/environments/environment";
// import {AccessGuard} from "../access.guard";
import {BibleService} from "../bible.service";
import {ChatroomService} from "../chatroom.service";
import {ImageService} from "../image.service";
import {UserService} from "../user.service";
import {ChatMessage,Chatroom,ChatUser,Sentence,User} from "./chatMessage";
import * as RecordRTC from "recordrtc";
import {SoundService} from "../sound.service";
import {AccessGuard} from "../access.guard";
import {compressImageToBase64, toImage} from "@beenotung/tslib/image";

@Component({
	selector: 'app-chatroom-detail',
	templateUrl: './chatroom-detail.component.html',
	styleUrls: ['./chatroom-detail.component.scss']
})
export class ChatroomDetailComponent implements OnInit,AfterViewChecked,OnDestroy{
	@ViewChild("scrollContainer") private myScrollContainer!:ElementRef;
	constructor(private userService:UserService,private chatroomService:ChatroomService,private route:ActivatedRoute,
	private imageService:ImageService,private bibleService:BibleService,private sanitizer:DomSanitizer,
	private soundService:SoundService,private accessGuard:AccessGuard){}
	ngOnInit():void{
		this.bibleService.loadScripture().subscribe((result) => {
			if(result.success){
				this.goldenSentence = result.data;
			}
		})
		this.chatroomService.joinChatroom(this.id);
		if(localStorage.getItem("token")){
			this.userService.checkCurrentUsers().subscribe((result) => {
				// console.log(result.data);
				if(result.success){
					this.user = result.data;
				}
				if(result.data.is_admin){
					this.accessGuard.user = "admin";
				}
			})
		}
		this.chatroomService.findChatroomInformation(this.id).subscribe((result) => {
			if(result.success){
				this.room = result.data;
			}
		})
		this.chatroomService.loadChatMessage(this.id).subscribe((result) => {
			// console.log(result.data);
			if(result.success){
				for(let i = 0; i < result.data.length; i++){
					// if(result.data[i].type === "text"){
					this.chatMessages.push(result.data[i]);
					// }
				}
				for(let message of this.chatMessages){
					if(message.send_user_id !== this.user.id){
						this.getImageFromService(`${this.imagePath}/${message.icon}`,message.id);
					}
					if(message.type === "image"){
						this.getImageFromServiceForImageMessage(`${this.imagePath}/${message.content}`,message.id)
					}
					if(message.type === "text" && message.send_user_id === this.user.id){
						// console.log(this.user.id)
						if(message.content.length > 111){
							message.content = message.content.substring(0,37) + " " + message.content.substring(37,74)
							+ " " + message.content.substring(74,111) + " " + message.content.substring(111);
						}else if(message.content.length > 74){
							message.content = message.content.substring(0,37) + " " + message.content.substring(37,74)
							+ " " + message.content.substring(74);
						}else if(message.content.length > 37){
							message.content = message.content.substring(0,37) + " " + message.content.substring(37);
						}else{
							message.content = message.content;
						}
					}
					if(message.type === "text" && message.send_user_id !== this.user.id){
						if(message.content.length > 120){
							message.content = message.content.substring(0,30) + " " + message.content.substring(30,60)
							+ " " + message.content.substring(60,90) + " " + message.content.substring(90,120) + " " +
							message.content.substring(120);
						}else if(message.content.length > 90){
							message.content = message.content.substring(0,30) + " " + message.content.substring(30,60)
							+ " " + message.content.substring(60,90) + " " + message.content.substring(90);
						}else if(message.content.length > 60){
							message.content = message.content.substring(0,30) + " " + message.content.substring(30,60)
							+ " " + message.content.substring(60);
						}else if(message.content.length > 30){
							message.content = message.content.substring(0,30) + " " + message.content.substring(30);
						}else{
							message.content = message.content;
						}
					}
					if(message.type === "sound"){
						this.soundMessageToShow[message.id] = this.sanitizer.bypassSecurityTrustResourceUrl(
							`${this.imagePath}/${message.content}`
						)
					}
					// if(message.type === "sound"){
					// 	this.getSoundFromServiceForSoundMessage(`${this.imagePath}/${message.content}`,message.id)
					// }
				}
			}
		})
		this.chatroomService.message().subscribe((message) => {
			// console.log(message.send_user_id);
			this.chatMessages.push(message);
			this.getImageFromService(`${this.imagePath}/${message.icon}`,message.id);
			if(message.type === "image"){
				this.getImageFromServiceForImageMessage(`${this.imagePath}/${message.content}`,message.id)
			}
			if(message.type === "text" && message.send_user_id === this.user.id){
				if(message.content.length > 111){
					message.content = message.content.substring(0,37) + " " + message.content.substring(37,74)
					+ " " + message.content.substring(74,111) + " " + message.content.substring(111);
				}else if(message.content.length > 74){
					message.content = message.content.substring(0,37) + " " + message.content.substring(37,74)
					+ " " + message.content.substring(74);
				}else if(message.content.length > 37){
					message.content = message.content.substring(0,37) + " " + message.content.substring(37);
				}else{
					message.content = message.content;
				}
			}
			if(message.type === "text" && message.send_user_id !== this.user.id){
				if(message.content.length > 120){
					message.content = message.content.substring(0,30) + " " + message.content.substring(30,60)
					+ " " + message.content.substring(60,90) + " " + message.content.substring(90,120) + " " +
					message.content.substring(120);
				}else if(message.content.length > 90){
					message.content = message.content.substring(0,30) + " " + message.content.substring(30,60)
					+ " " + message.content.substring(60,90) + " " + message.content.substring(90);
				}else if(message.content.length > 60){
					message.content = message.content.substring(0,30) + " " + message.content.substring(30,60)
					+ " " + message.content.substring(60);
				}else if(message.content.length > 30){
					message.content = message.content.substring(0,30) + " " + message.content.substring(30);
				}else{
					message.content = message.content;
				}
			}
			// if(message.type === "sound"){
			// 	this.getSoundFromServiceForSoundMessage(`${this.imagePath}/${message.content}`,message.id)
			// }
			if(message.type === "sound"){
				this.soundMessageToShow[message.id] = this.sanitizer.bypassSecurityTrustResourceUrl(
					`${this.imagePath}/${message.content}`
				)
			}
		})
		this.chatroomService.getChatroomUser(this.id).subscribe((result) => {
			if(result.success){
				this.chatroomUsers = result.data;
				for(let user of this.chatroomUsers){
					this.getImageFromServiceForChatUsers(`${this.imagePath}/${user.icon}`,user.id);
				}
			}
		})
	}
	ngAfterViewChecked(){
        this.scrollToBottom()
    }
	user:User = {
		id:0,
		username:"",
		phone:0,
		icon:"",
		qr_code:"",
		is_admin:null
	}
	id = Number(this.route.snapshot.paramMap.get("id"));
	room:Chatroom = {
		id:0,
		room_name:"",
		room_icon:"",
		created_at:null,
		updated_at:null
	}
	goldenSentence:Sentence = {
		id:0,
		bible_id:0,
		content:""
	}
	chatroomUsers:ChatUser[] = []
	chatMessages:ChatMessage[] = [];

	// imagePath = environment.BACKEND_PATH;
	imagePath = environment.IMAGE_PATH;
	newMessage = "";
	imageToShow:(string|ArrayBuffer|null|SafeResourceUrl)[] = [];
	userImageToShow:(string|ArrayBuffer|null|SafeResourceUrl)[] = [];
	imageMessageToShow:(string|ArrayBuffer|null|SafeResourceUrl)[] = [];

	createImageFromBlob(image:Blob,index:number){
		let reader = new FileReader();
		reader.addEventListener("load",() => {
			this.imageToShow[index] = this.sanitizer.bypassSecurityTrustResourceUrl(reader.result as string);
		},false);
		if(image){
			reader.readAsDataURL(image);
		}
	}
	getImageFromService(imageUrl:string,index:number){
		// this.imageService.getImage(imageUrl).subscribe((data) => {
		this.imageService.getImageWithoutHeaders(imageUrl).subscribe((data) => {
			// console.log(data);
			this.createImageFromBlob(data,index);
		},(error) => {
			// console.log(error);
		})
	}

	createImageFromBlobForChatUsers(image:Blob,index:number){
		let reader = new FileReader();
		reader.addEventListener("load",() => {
			this.userImageToShow[index] = this.sanitizer.bypassSecurityTrustResourceUrl(reader.result as string);
		},false);
		if(image){
			reader.readAsDataURL(image);
		}
	}
	getImageFromServiceForChatUsers(imageUrl:string,index:number){
		// this.imageService.getImage(imageUrl).subscribe((data) => {
		this.imageService.getImageWithoutHeaders(imageUrl).subscribe((data) => {
			// console.log(data);
			this.createImageFromBlobForChatUsers(data,index);
		},(error) => {
			// console.log(error);
		})
	}

	createImageFromBlobForImageMessage(image:Blob,index:number){
		let reader = new FileReader();
		reader.addEventListener("load",() => {
			this.imageMessageToShow[index] = this.sanitizer.bypassSecurityTrustResourceUrl(reader.result as string);
		},false);
		if(image){
			reader.readAsDataURL(image);
		}
	}
	getImageFromServiceForImageMessage(imageUrl:string,index:number){
		// this.imageService.getImage(imageUrl).subscribe((data) => {
		this.imageService.getImageWithoutHeaders(imageUrl).subscribe((data) => {
			// console.log(data);
			this.createImageFromBlobForImageMessage(data,index);
		},(error) => {
			// console.log(error);
		})
  	}

	sendMessage(message:string){
		this.chatroomService.sendMessage(message,this.id,this.user.id).subscribe();
		// this.chatroomService.sendMessageWithSocket(message);
		this.newMessage = "";
	}

	newImageIcon:any;
	imageSource:string|ArrayBuffer|null = "";
	readURL(event:any):void{
		if(event.currentTarget && event.currentTarget.files && event.currentTarget.files[0]){
			this.newImageIcon = event.currentTarget.files[0];
			const file = event.currentTarget.files[0];
			const reader = new FileReader();
			reader.onload = () => this.imageSource = reader.result;
			reader.readAsDataURL(file);
			// console.log(this.imageSource);
			event.currentTarget.value = null;
		}
	}

	async sendPhoto(photo:File){
		// console.log(photo);
		let realImage = await toImage(photo);
		// console.log(realImage);
		let newUrl = await compressImageToBase64({image:realImage,mimeType:"image/png",maximumLength:200*1000});
		// console.log(newUrl);
		const res = await fetch(newUrl);
		const blob = await res.blob();
		const file = new File([blob],"image.png",{
			type:"image/png"
		})
		this.chatroomService.sendPhotoMessage(file,this.id,this.user.id).subscribe();
		this.imageSource = "";
	}
	closePhoto(){
		this.imageSource = "";
	}

	scrollToBottom():void{
		// console.log(this.myScrollContainer);
		this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    }

	isRecording = false;
	isRecordingInterval = false
	startRecording(){
		this.isRecording = true;
		setInterval(() => {
			this.isRecordingInterval = !this.isRecordingInterval
		},1000)
		let mediaConstraints = {
			video: false,
			audio: true
		}
		navigator.mediaDevices.getUserMedia(mediaConstraints).then(this.successCallback.bind(this),
		this.errorCallback.bind(this));
	}
	stopRecording(){
		this.isRecording = false;
		if(this.record){
			this.record.stop((obj:Blob) => {
				let message = new File([obj],"message.mp3",{
					type: "audio/mp3",
				})
				this.chatroomService.sendVoiceMessage(message,this.id,this.user.id).subscribe();
			})
		}
	}

	record:any;
	error:any;
	soundMessageToShow:(SafeResourceUrl|any)[] = [];
	/**
	* Will be called automatically.
	*/
	successCallback(stream:any) {
		let options = {
			mimeType:"audio/wav",
			numberOfAudioChannels:1,
			sampleRate:16000,
		}
		//Start Actual Recording
		let StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
		this.record = new StereoAudioRecorder(stream,options);
		this.record.record();
	}

	errorCallback(error:string) {
		this.error = "cannot play audio in your browser";
	}

	createMessageFromBlobForSoundMessage(file:Blob,index:number){
		let reader = new FileReader();
		reader.addEventListener("load",() => {
			this.soundMessageToShow[index] = this.sanitizer.bypassSecurityTrustResourceUrl(reader.result as string);
			// console.log(this.soundMessageToShow);
		},false);
		if(file){
			reader.readAsDataURL(file);
		}
	}
	// getSoundFromServiceForSoundMessage(soundUrl:string,index:number){
	// 	// console.log(soundUrl);
	// 	this.soundService.getSound(soundUrl).subscribe((data) => {
	// 		this.createMessageFromBlobForSoundMessage(data,index);
	// 	},error => {
	// 		console.log(error);
	// 	})
  	// }

	ngOnDestroy():void{
		this.chatroomService.leaveRoom(this.id);
	}
}