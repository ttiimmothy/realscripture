import {Request, Response} from "express";
import {logger} from "../logger";
import {ChatroomService} from "../services/ChatroomService";
import {Server as SocketIO} from "socket.io";
// import {RequestWithTestUser} from "./ChatroomController.spec";

export class ChatroomController{
	constructor(private chatroomService:ChatroomService,private io:SocketIO){}

	loadChatroomList = async(req:Request,res:Response) => {
		try{
			let chatroom = await this.chatroomService.loadChatroomList(req.user?.id);
			// console.log(req.user?.id);
			logger.debug(JSON.stringify(chatroom,null,4));
			res.status(200).json({success:true,data:chatroom});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	findChatroomInformation = async(req:Request,res:Response) => {
		try{
			let chatroomInformation = (await this.chatroomService.findChatroomInformation(parseInt(req.params.id)))[0];
			res.status(200).json({success:true,data:chatroomInformation});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	loadChatMessage = async(req:Request,res:Response) => {
		try{
			let chatroomMessage = await this.chatroomService.loadChatMessage(parseInt(req.params.id));
			res.status(200).json({success:true,data:chatroomMessage});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	sendMessage = async(req:Request,res:Response) => {
		try{
			// console.log(req.body.userId)
			let message = await this.chatroomService.sendMessage(req.body.message,req.body.chatroomId,req.body.userId);
			// console.log(`chatroom ${req.body.chatroomId}`)
			this.io.to(`userChatroom ${parseInt(req.body.chatroomId)}`).emit("getMessage",message[0]);
			res.status(200).json({success:true});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	sendPhotoMessage = async(req:Request,res:Response) => {
		try{
			// console.log(req.body)
			if(req.file){
				req.body.photo = (req.file as any).key;
				// req.body.photo = req.file.filename;
				// console.log(req.file.size);
			}
			let message = await this.chatroomService.sendPhotoMessage(req.body.photo,parseInt(req.body.chatroomId),
			parseInt(req.body.userId),req.file?.size);
			this.io.to(`userChatroom ${parseInt(req.body.chatroomId)}`).emit("getMessage",message[0]);
			res.status(200).json({success:true});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	sendVoiceMessage = async(req:Request,res:Response) => {
		try{
			// console.log(req.body)
			if(req.file){
				req.body.sound = (req.file as any).key;
				// req.body.sound = req.file.filename;
			}
			let message = await this.chatroomService.sendVoiceMessage(req.body.sound,parseInt(req.body.chatroomId),
			parseInt(req.body.userId),req.file?.size);
			// console.log(message)
			this.io.to(`userChatroom ${parseInt(req.body.chatroomId)}`).emit("getMessage",message[0]);
			res.status(200).json({success:true});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	searchPerson = async(req:Request,res:Response) => {
		try{
			let username = await this.chatroomService.searchPerson(req.body.name,req.body.alreadyExistName,req.user?.id);
			res.status(200).json({success:true,data:username});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	searchNewPerson = async(req:Request,res:Response) => {
		try{
			let users = await this.chatroomService.searchNewPerson(req.body.name,req.body.alreadyExistName,
			req.body.alreadyInGroupName);
			res.status(200).json({success:true,data:users});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	getChatroomUser = async(req:Request,res:Response) => {
		try{
			let users = await this.chatroomService.getChatroomUser(parseInt(req.params.id));
			res.status(200).json({success:true,data:users});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	createChatroom = async(req:Request,res:Response) => {
		try{
			if(req.file){
				// req.body.icon = req.file.filename;
				req.body.icon = (req.file as any).key;
			}else{
				req.body.icon = "unknown.png";
			}
			// console.log(req.body)
			let admin = req.user?.id;
			// console.log(req.user?.id)
			await this.chatroomService.createChatroom(req.body.name,req.body.icon,JSON.parse(req.body.users),admin);
			res.status(200).json({success:true});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}

	addUserToChatroom = async(req:Request,res:Response) => {
		try{
			await this.chatroomService.addUserToChatroom(req.body.users,parseInt(req.params.id));
			res.status(200).json({success:true});
		}catch(e){
			logger.error(`method:${req.method} path:${req.path}`);
			logger.error(e);
			res.status(500).json({success:false,message:e.toString()});
		}
	}
}