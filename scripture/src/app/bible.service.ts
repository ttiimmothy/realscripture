import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from '@angular/core';
import {Observable, of} from "rxjs";
import {catchError} from "rxjs/operators";
import {environment} from "src/environments/environment";

@Injectable({
    providedIn: 'root',
})
export class BibleService {
    constructor(private http:HttpClient){}

	token = localStorage.getItem("token");

	loadScripture():Observable<any>{
		return this.http.get(`${environment.BACKEND_PATH}/bible/scripture`,
		{headers:new HttpHeaders({authorization:`Bearer ${this.token}`})}).pipe(
			catchError(this.handleError("load scripture"))
		)
	}

	loadScriptureDetails():Observable<any>{
		return this.http.get(`${environment.BACKEND_PATH}/bible/scriptureDetail`,
		{headers:new HttpHeaders({authorization:`Bearer ${this.token}`})}).pipe(
			catchError(this.handleError("load scripture detail"))
		)
	}

	private handleError<T>(operation = "operation",result?:T){
		return(error:any):Observable<T> => {
			console.error(`${operation}:${error}`);
			return of(result as T);
		}
	}
}