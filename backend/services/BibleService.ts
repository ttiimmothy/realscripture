import {Knex} from "knex";

export class BibleService{
	constructor(private knex:Knex){}

	async getBibleScripts(date:number){
		return this.knex.select("*").from("golden_sentence").where("bible_id",date);
	}

	async newScripture(date:number,content:string){
		return await this.knex.insert({bible_id:date,content}).into("golden_sentence").returning("*");
	}

	async loadScriptureDetailId(){
		return await this.knex.select("bible_id").from("golden_sentence").orderBy("bible_id");
	}

	async insertNewScripture(id:number,content:string){
		await this.knex.insert({bible_id:id,content}).into("golden_sentence");
	}

	async loadScriptureDetail(){
		return await this.knex.select("*").from("golden_sentence").orderBy("bible_id");
	}
}