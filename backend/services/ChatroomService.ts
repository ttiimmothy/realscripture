import {Knex} from "knex";
import {logger} from "../logger";

export class ChatroomService{
	constructor(private knex:Knex){}

	async loadChatroomList(userId:number|undefined){
		if(userId){
			return await this.knex.select("chatroom.*","user_chatroom.*").from("chatroom").innerJoin("user_chatroom",
			"chatroom_id","chatroom.id").innerJoin("users","user_id","users.id").orderBy("chatroom.id")
			.where("user_id",userId);
		}
		return;
	}

	async findChatroomInformation(id:number){
		return await this.knex.select("*").from("chatroom").where("id",id);
	}

	async loadChatMessage(roomId:number){
		return await this.knex.select("group_message.*","username","icon").from("group_message").innerJoin("chatroom",
		"chatroom.id","chatroom_id").innerJoin("users","users.id","send_user_id").where("chatroom_id",roomId)
		.orderBy("group_message.id");
	}

	async sendMessage(message:string,roomId:number,userId:number){
		const trx = await this.knex.transaction();
		try{
			await trx.insert({send_user_id:userId,chatroom_id:roomId,content:message}).into("group_message");
			let messages = await trx.select("group_message.*","username","icon").from("group_message")
			.innerJoin("chatroom",
			"chatroom.id","chatroom_id").innerJoin("users","users.id","send_user_id").where("content",message);
			await trx.commit();
			return messages;
		}catch(e){
			logger.error("knex transaction rollback");
			trx.rollback();
			throw e;
		}
	}

	async sendPhotoMessage(image:string,roomId:number,userId:number,size:number|undefined){
		const trx = await this.knex.transaction();
		// console.log(image);
		try{
			await trx.insert({send_user_id:userId,chatroom_id:roomId,content:image,type:"image"}).into("group_message");
			if(size){
				await trx.insert({name:image,size,type:"image"}).into("file_detail");
			}
			let messages = await trx.select("group_message.*","username","icon").from("group_message")
			.innerJoin("chatroom","chatroom.id","chatroom_id").innerJoin("users","users.id","send_user_id")
			.where("content",image);
			await trx.commit();
			return messages;
		}catch(e){
			logger.error("knex transaction rollback");
			trx.rollback();
			throw e;
		}
	}

	async sendVoiceMessage(message:string,roomId:number,userId:number,size:number|undefined){
		const trx = await this.knex.transaction();
		try{
			await trx.insert({send_user_id:userId,chatroom_id:roomId,content:message,type:"sound"}).into("group_message");
			if(size){
				await trx.insert({name:message,size,type:"sound"}).into("file_detail");
			}
			let messages = await trx.select("group_message.*","username","icon").from("group_message")
			.innerJoin("chatroom","chatroom.id","chatroom_id").innerJoin("users","users.id","send_user_id")
			.where("content",message);
			await trx.commit();
			return messages;
		}catch(e){
			logger.error("knex transaction rollback");
			trx.rollback();
			throw e;
		}
	}

	async searchPerson(username:string,existUser:string[],currentUserId:number|undefined){
		if(currentUserId){
			return await this.knex.select("id","username","icon").from("users").where("username","like",`%${username}%`)
			.whereNotIn("username",existUser).andWhereNot("id",currentUserId).limit(3);
		}
		return;
	}

	async searchNewPerson(username:string,existUser:string[],alreadyInGroupUser:string[]){
		for(let user of alreadyInGroupUser){
			existUser.push(user);
		}
		return await this.knex.select("id","username","icon").from("users").where("username","like",`%${username}%`)
		.whereNotIn("username",existUser).limit(10);
	}

	async createChatroom(name:string,icon:string,usernames:string[],adminId:number|undefined){
		const trx = await this.knex.transaction();
		try{
			let roomId = (await trx.insert({room_name:name,room_icon:icon}).into("chatroom").returning("id"))[0];
			for(let user of usernames){
				let userId = await trx.select("id").from("users").where("username",user);
				await trx.insert({user_id:userId[0].id,chatroom_id:roomId}).into("user_chatroom");
			}
			if(adminId){
				await trx.insert({user_id:adminId,chatroom_id:roomId,is_admin:true}).into("user_chatroom");
			}
			await trx.commit();
			// console.log(adminId);
			// console.log(roomId + " room");
		}catch(e){
			logger.error("knex transaction rollback");
			trx.rollback();
			throw e;
		}
	}

	async addUserToChatroom(users:string[],roomId:number){
		const trx = await this.knex.transaction();
		try{
			for(let user of users){
				let userId = await trx.select("id").from("users").where("username",user);
				await trx.insert({user_id:userId[0].id,chatroom_id:roomId}).into("user_chatroom");
			}
			await trx.commit();
		}catch(e){
			logger.error("knex transaction rollback");
			trx.rollback();
			throw e;
		}
	}

	async getChatroomUser(chatroomId:number){
		return await this.knex.select("users.id","username","icon","user_chatroom.is_admin").from("users")
		.innerJoin("user_chatroom","users.id","user_chatroom.user_id").innerJoin("chatroom","chatroom.id",
		"chatroom_id").where("chatroom_id",chatroomId);
	}
}