import {Component,OnInit} from '@angular/core';
import {BibleService} from "../bible.service";
import {UserService} from "../user.service";
import {Sentence} from "./sentence";

@Component({
	selector: 'app-notice',
	templateUrl: './notice.component.html',
	styleUrls: ['./notice.component.scss']
})
export class NoticeComponent implements OnInit{
	constructor(private bibleService:BibleService,private userService:UserService){}
	ngOnInit():void{
		this.userService.checkCurrentUsers().subscribe((result) => {
			if(result.success && result.data.is_admin){
				this.isAdmin = true;
			}
		})
		this.bibleService.loadScripture().subscribe((result) => {
			if(result.success){
				this.goldenSentence = result.data;
			}
		})
	}
	isAdmin = false;
	notices = ["hello world","finish homework"];
	goldenSentence:Sentence = {
		id:0,
		bible_id:0,
		content:""
	}
	passwordInput = false;
	resetPasswordInput = false;
	confirmAgain = false;
	password = "";
	passwordAgain = "";
	defaultPassword = "";
	confirmText = "";
	showPasswordInput(){
		this.passwordInput = true;
		this.resetPasswordInput = false;
	}
	showResetPasswordInput(){
		this.resetPasswordInput = true;
		this.passwordInput = false;
	}

	changePassword(password:string,passwordAgain:string){
		if(password && password === passwordAgain){
			this.userService.changePassword(password).subscribe((result) => {
				// console.log(result);
				if(result.success){
					this.password = "";
					this.passwordAgain = "";
				}
			})
		}
	}
	typeConfirmAgain(){
		this.confirmAgain = true;
	}

	changeDefaultPassword(confirmText:string,password:string){
		if(password && confirmText === "Confirm"){
			this.userService.changeDefaultPassword(password).subscribe((result) => {
				// console.log(result);
				if(result.success){
					this.confirmText = "";
					this.confirmAgain = false;
					this.resetPasswordInput = false;
				}
			})
		}
	}
}
